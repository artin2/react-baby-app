
import React from 'react';
import { storiesOf } from '@storybook/react';
import BranchPreviewChoice from '../index';

const stories = storiesOf('BranchPreviewChoice', module);

stories.add('Light', () => (
  <BranchPreviewChoice label="Partner Support" />
));
