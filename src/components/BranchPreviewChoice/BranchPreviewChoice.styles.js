const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.5rem',
    padding: '1.6rem 0',
  },
});

export { styles };
