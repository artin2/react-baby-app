import BranchPreviewChoice from './BranchPreviewChoice';
import enhance from './BranchPreviewChoice.enhancer';

export default enhance(BranchPreviewChoice);
