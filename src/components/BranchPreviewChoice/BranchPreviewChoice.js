import React from 'react';

type Props = {
  classes: any,
  label: String,
}

const BranchPreviewChoice = (props: Props) => {
  const { classes, label } = props;
  return (
    <div className={classes.root}>{label}</div>
  );
}

export default BranchPreviewChoice;

