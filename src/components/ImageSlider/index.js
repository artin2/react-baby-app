import ImageSlider from './ImageSlider';
import enhance from './ImageSlider.enhancer';

export default enhance(ImageSlider);
