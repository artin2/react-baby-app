import React from 'react';
import { storiesOf } from '@storybook/react';
import ImageSlider from '../index';
// import imageSliderPlaceholder from '../../../assets/images/image-slider-placeholder.png';

const stories = storiesOf('ImageSlider', module);

const images = [
  {
    original: 'http://lorempixel.com/1000/600/nature/1/',
  },
  {
    original: 'http://lorempixel.com/1000/600/nature/2/',
  },
  {
    original: 'http://lorempixel.com/1000/600/nature/3/',
  },
];
stories.add('Normal', () => (
  <ImageSlider items={images} />
));
