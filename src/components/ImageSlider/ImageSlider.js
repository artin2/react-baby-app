import React from 'react';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';

type Props = {
  classes: any,
  items:[any],
}
const ImageSlider = (props: Props) => {
  const { classes, items } = props;
  return (
    <div className={classes.root}>
      <ImageGallery
        autoPlay
        lazyLoad
        showBullets
        items={items}
        showFullscreenButton={false}
        showThumbnails={false}
        showNav={false}
        showPlayButton={false}
        slideInterval={2000}
        slideDuration={1500}
        infinite
      />
    </div>
  );
};

export default ImageSlider;
