import SelectionListItem from './SelectionListItem';
import enhance from './SelectionListItem.enhancer';

export default enhance(SelectionListItem);
