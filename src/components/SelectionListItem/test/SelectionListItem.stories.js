import React from 'react';
import { storiesOf } from '@storybook/react';
import SelectionListItem from '../index';

const stories = storiesOf('SelectionListItem', module);

stories.add('SelectionListItem', () => (
  <div style={{ width: '50%', margin: 'auto' }}>
    <SelectionListItem label="Um, how the heck do I get my socks on?" />
  </div>

));
