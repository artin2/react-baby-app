import React from 'react';
import Chevron from '../Chevron';

type Props = {
  classes: any,
  item: any,
  onClick: () => {},
}

const SelectionListItem = (props: Props) => {
  const {
    onClick,
    item,
    classes,
  } = props;

  const { value } = item;

  const handleClick = (event) => {
    console.log(event, item);
    onClick(event, item);
  };

  return (
    <div
      className={classes.root}
      onClick={handleClick}
      onKeyPress={handleClick}
      role="button"
      tabIndex={0}
    >
      <div className={classes.label}>{value}</div>
      <Chevron direction="right" variant="light" classes={{ root: classes.chevron }} />
    </div>
  );
};

export default SelectionListItem;

