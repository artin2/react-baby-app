const mainFontSize = 1.5;
const styles = theme => ({
  root: {
    '&:first-child': {
      borderTop: `1px solid ${theme.palette.borderGray}`,
    },
    borderBottom: `1px solid ${theme.palette.borderGray}`,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    padding: '1.8rem 1.5rem',
    fontSize: `${mainFontSize * 0.8}rem`,
    fontFamily: theme.typography.fontFamily.primary.main,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${mainFontSize}rem`,
    },
  },
  chevron: {
    paddingRight: '5px',
  },
});

export { styles };
