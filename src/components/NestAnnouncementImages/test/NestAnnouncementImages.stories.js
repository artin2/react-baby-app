import React from 'react';
import { storiesOf } from '@storybook/react';
import NestAnnouncementImages from '../index';
import profileImg from '../../../assets/images/profile-pic2.jpg';
import nestImg from '../../../assets/images/announcement-img@2x.png';

const stories = storiesOf('NestAnnouncementImages', module);

stories.add('NestAnnouncementImages', () => (
  <NestAnnouncementImages
    nestImg={nestImg}
    profileImg={profileImg}
  />
));
