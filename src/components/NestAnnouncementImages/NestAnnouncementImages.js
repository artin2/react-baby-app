import React from 'react';

type Props = {
  classes: any,
  nestImg: any,
  profileImg: any,
  // message: any,
}

const NestAnnouncementImages = (props: Props) => {
  const {
    classes,
    nestImg,
    profileImg,
  } = props;
  return (
    <div className={classes.root}>
      {/* <div className={classes.messageImg}> */}
      <img className={classes.messageImg} src={nestImg} alt="Nest Message" />
      {/* </div> */}
      {/* <div className={classes.profileImg}> */}
      <img className={classes.profileImg} src={profileImg} alt="Nest Profile" />
      {/* </div> */}
    </div>
  );
};

export default NestAnnouncementImages;
