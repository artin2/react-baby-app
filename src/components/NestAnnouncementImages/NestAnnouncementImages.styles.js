const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '300px', // will probably need to change
    justifyContent: 'center',
    alignItems: 'center',
  },
  messageImg: {
    height: '400px',
    backgroundPosition: 0,
    // width: '100%',
    borderRadius: '4px',
  },
  profileImg: {
    borderRadius: '50%',
    height: '80px',
    width: '80px',
    border: `4px solid ${theme.palette.white}`,
    marginTop: '-40px',
  },
});

export { styles };

