import NestAnnouncementImages from './NestAnnouncementImages';
import enhance from './NestAnnouncementImages.enhancer';

export default enhance(NestAnnouncementImages);
