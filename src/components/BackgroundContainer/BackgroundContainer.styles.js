import { section, container } from '../../assets/styles/global.styles';

const styles = theme => ({
  root: {

  },
  section,
  container,
  pink: {
    background: theme.palette.background.pink,
  },
  yellow: {
    background: theme.palette.background.yellow,
  },
  gray: {
    background: theme.palette.background.gray,
  },
});

export { styles };
