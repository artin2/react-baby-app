import React from 'react';
import { storiesOf } from '@storybook/react';
import BackgroundContainer from '..';

const stories = storiesOf('BackgroundContainer', module);

stories.add('Light', () => (
  <BackgroundContainer />
));
