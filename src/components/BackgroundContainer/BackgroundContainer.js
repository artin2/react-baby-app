import React from 'react';
import cn from 'classnames';
import { BackgroundVariant } from '../../config/prop-types';

type Props = {
  classes: any,
  children: any,
  variant: BackgroundVariant,
}
const BackgroundContainer = (props: Props) => {
  const { classes, children, variant } = props;
  return (
    <div className={cn(classes.root, classes[variant])}>
      <div className={classes.section}>
        <div className={classes.container}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default BackgroundContainer;

