import BackgroundContainer from './BackgroundContainer';
import enhance from './BackgroundContainer.enhancer';

export default enhance(BackgroundContainer);
