import HelpIcon from './HelpIcon';
import enhance from './HelpIcon.enhancer';

export default enhance(HelpIcon);
