import React from 'react';

type Props = {
  classes: any,
}

const HelpIcon = (props: Props) => {
  const { classes } = props;
  return (
    <div className={classes.infoIcon}>
      <div className={classes.infoIconValue}>?</div>
    </div>
  );
};

export default HelpIcon;

