// import { globalStyleVars as g } from '../../assets/styles/global-variables';

const styles = theme => ({
  infoIcon: {
    width: '1.6rem',
    height: '1.6rem',
    background: '#FFFFFF',
    borderRadius: '50%',
    position: 'relative',
    margin: '1.3rem',
  },
  infoIconValue: {
    fontWeight: 600,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily.primary.main,
    textAlign: 'center',
    fontSize: '1.2rem',
  },
});

export { styles };
