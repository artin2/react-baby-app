import React from 'react';
import { storiesOf } from '@storybook/react';
import RoundImageUpload from '../index';
import placeholderImage from '../../../assets/images/upload-image.png';

const stories = storiesOf('RoundImageUpload', module);

stories.add('Normal', () => (
  <RoundImageUpload
    image={placeholderImage}
  />
));
