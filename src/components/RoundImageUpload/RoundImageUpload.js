import React from 'react';
import RoundImage from '../RoundImage';
import placeholderImage from '../../assets/images/upload-image.png';

type Props = {
  classes: any,
  image: any,
  onUpload: () => {},
}
const RoundImageUpload = (props: Props) => {
  const { classes, image, onUpload } = props;

  const handleFileChange = async (e) => {
    const file = e.target.files[0];
    onUpload(file);
  };

  // TODO: not sure if I need this
  const handleUpload = () => {
    console.log('uploading');
  };

  return (
    <button type="submit" className={classes.root} onClick={handleUpload}>
      <label htmlFor="add-img-btn" className={classes.label}>
        <RoundImage
          classes={{
            root: classes.roundImageRoot,
          }}
          image={image || placeholderImage}
        />
        <input
          id="add-img-btn"
          onChange={handleFileChange}
          type="file"
          style={{ display: 'none' }}
        />
      </label>
    </button>
  );
};

export default RoundImageUpload;

