import RoundImageUpload from './RoundImageUpload';
import enhance from './RoundImageUpload.enhancer';

export default enhance(RoundImageUpload);
