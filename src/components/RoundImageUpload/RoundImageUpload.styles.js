const styles = theme => ({
  root: {
    borderRadius: '50%',
    border: 'none',
    background: 'transparent',
    padding: 0,
    '&:focus': {
      outline: 0,
      boxShadow: `0 0 0 2px ${theme.palette.primary.main}`,
    },
  },
  roundImageRoot: {
    border: `1px solid ${theme.palette.borderGray}`,
  },
});

export { styles };
