import AboutUs from './AboutUs';
import enhance from './AboutUs.enhancer';

export default enhance(AboutUs);
