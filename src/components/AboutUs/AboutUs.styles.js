import { section, container } from '../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    background: theme.palette.base.main,
    width: '100vw',
  },
  section,
  container,
  header: {
    color: theme.palette.text.white,
    fontSize: '1.3rem',
    fontFamily: theme.typography.fontFamily.primary.bold,
    textTransform: 'uppercase',
  },
  content: {
    color: theme.palette.text.white,
    fontFamily: theme.typography.fontFamily.primary.medium,
    '& > p': {
      fontSize: '1.1rem',
      lineHeight: '16px',
      paddingTop: '16px',
    },
  },
  socialContainer: {
    marginTop: '40px',
  },
  iconContainer: {
    display: 'flex',
    paddingTop: '10px',
    '& >img': {
      padding: '0 10px',
      '&:first-child': {
        paddingLeft: 0,
      },
    },
  },
});

export { styles };
