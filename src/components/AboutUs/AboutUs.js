import React from 'react';
import instagramLogo from '../../assets/images/instagram-logo.svg';
import pinterestLogo from '../../assets/images/pinterest-logo.svg';
import youtubeLogo from '../../assets/images/youtube-logo.svg';

type Props = {
  classes: any
}
const AboutUs = (props: Props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <div className={classes.section}>
        <div className={classes.container}>
          <div className={classes.header}>About Us</div>
          <div className={classes.content}>
            <p>We’re a husband and wife team living in Madison, WI.
              We have two silly, smart, cuddly boys that we’re absolutely in love with.
            </p>
            <p>We started Nestling because we welcomed both of our little guys into the world
              far away from our friends and family. We wanted to give people a meaningful way
              to be a part of our journey.
            </p>
            <p>Our hope is that Nestling bring friends and families all over the world a little
              closer during their important transitions in life.
            </p>
          </div>
          <div className={classes.socialContainer}>
            <div className={classes.header}>Follow Us</div>
            <div className={classes.iconContainer}>
              <img src={instagramLogo} alt="instagram" />
              <img src={pinterestLogo} alt="pinterest" />
              <img src={youtubeLogo} alt="youtube" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;

