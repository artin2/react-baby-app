import ResponseHeader from './ResponseHeader';
import enhance from './ResponseHeader.enhancer';

export default enhance(ResponseHeader);
