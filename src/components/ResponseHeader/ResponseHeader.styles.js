const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.secondary,
    color: '#FFFFFF',
    fontSize: '3.2rem',
    fontWeight: 700,
    textAlign: 'center',
    margin: 'auto',
  },
});

export { styles };
