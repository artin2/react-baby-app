import React from 'react';

type Props = {
  classes: any,
  label: string
}
const ResponseHeader = (props: Props) => {
  const { classes, label } = props;
  return (
    <div className={classes.root}>{label}</div>
  );
};

export default ResponseHeader;

