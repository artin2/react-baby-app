import { btnStyles } from '../../assets/styles/button.styles';

const styles = theme => ({
  root: {
    background: theme.palette.background.gray,
    minHeight: '300px',
    display: 'flex',
    flexDirection: 'column',
    justifyItems: 'center',
    alignItems: 'center',
  },
  placeholderContainer: {
    background: theme.palette.background.white,
    display: 'flex',
    margin: '30px auto 18px auto',
  },
  rectangular: {
    borderRadius: '4px',
    height: '200px',
    width: '180px',
  },
  round: {
    borderRadius: '50%',
    height: '216px',
    width: '216px',
  },
  btn: btnStyles,
  uploadBtn: {
    background: 'none',
  },
  placeholderImage: {
    margin: 'auto',
    display: 'flex',
    justifyItems: 'center',
    alignItems: 'center',
  },
  uploadText: {
    color: theme.palette.pink,
    fontSize: '1.4rem',
    fontFamily: theme.typography.fontFamily.primary.bold,
    textTransform: 'uppercase',
  },
});

export { styles };
