import React from 'react';
import { storiesOf } from '@storybook/react';
import UploadImageCard from '../index';

const stories = storiesOf('UploadImageCard', module);

stories.add('Primary', () => (
  <UploadImageCard uploadTypeText="Announcement" />
));
