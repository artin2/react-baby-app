/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useEffect } from 'react';
import cn from 'classnames';
import { ImagePlaceholderShape } from '../../config/prop-types';
import placeholderImage from '../../assets/images/upload-image.png';

type Props = {
  classes: any,
  uploadTypeText: String,
  image: any,
  onUpload: () => {},
  placeholderShape?: ImagePlaceholderShape,
}

const UploadImageCard = (props: Props) => {
  const {
    classes, image, uploadTypeText, onUpload, placeholderShape,
  } = props;
  const uploadText = `+ Add ${uploadTypeText} Photo`;
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadLabelText, setUploadLabelText] = useState(uploadText);
  useEffect(() => {
    if (image) {
      setUploadLabelText('Change Announcement Photo');
    }
  }, [image]);
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
    onUpload(file);
  };

  const handleUpload = () => {
    console.log(selectedFile);
  };
  return (
    <div className={classes.root}>
      <div className={cn(classes.placeholderContainer, classes[placeholderShape])}>
        {!image ? (
          <img src={placeholderImage} className={classes.placeholderImage} alt="upload" />
        ) : <img style={{ width: '100%', height: '200px', objectFit: 'cover' }} src={image} alt="any" />}
      </div>
      <button type="submit" onClick={handleUpload} className={cn(classes.btn, classes.uploadBtn)}>
        <label htmlFor="add-img-btn" className={classes.uploadText}>
          {uploadLabelText}
          <input
            id="add-img-btn"
            onChange={handleFileChange}
            type="file"
            style={{ display: 'none' }}
          />
        </label>
      </button>
    </div>
  );
};

UploadImageCard.defaultProps = {
  placeholderShape: 'rectangular',
};

export default UploadImageCard;

