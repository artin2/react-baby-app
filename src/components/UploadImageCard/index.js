import UploadImageCard from './UploadImageCard';
import enhance from './UploadImageCard.enhancer';

export default enhance(UploadImageCard);
