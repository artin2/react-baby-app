import ButtonGroup from './ButtonGroup';
import enhance from './ButtonGroup.enhancer';

export default enhance(ButtonGroup);
