import React, {
  type Node,
  Children,
  isValidElement,
  cloneElement,
} from 'react';

type Props = {
  children: Node,
  classes: any,
  size: any,
}

const ButtonGroup = (props: Props) => {
  const {
    children: childrenProp,
    classes,
    size,
  } = props;

  const children = Children.map(childrenProp, (child) => {
    if (!isValidElement(child)) {
      return null;
    }
    return cloneElement(child, {
      size,
    });
  });
  return (
    <div className={classes.root}>
      {children}
    </div>
  );
};

export default ButtonGroup;
