import React from 'react';
import cn from 'classnames';
import headerLogoLight from '../../../assets/images/header-logo-light@2x.png';
import headerLogoDark from '../../../assets/images/header-logo-dark@2x.png';
import { HeaderVariant } from '../../../config/prop-types';
import { HOME_PATH } from '../../../constants/paths';

type Props = {
  classes: any,
  variant: HeaderVariant,
}

const CenteredLogoHeader = (props: Props) => {
  const { classes, variant } = props;
  const img = (variant === 'dark' || variant === 'transparent')
    ? headerLogoDark : headerLogoLight;
  return (
    <a href={HOME_PATH} className={cn(classes.root, classes[variant])}>
      <img className={classes.logo} src={img} alt="Nestling Logo" />
    </a>
  );
};
export default CenteredLogoHeader;

