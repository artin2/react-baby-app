import React from 'react';
import { storiesOf } from '@storybook/react';
import CenteredLogoHeader from '../index';

const stories = storiesOf('CenteredLogoHeader', module);

stories.add('Light', () => (
  <CenteredLogoHeader variant="light"/>
));

stories.add('Dark', () => (
  <div style={{background: 'black', width: '100%'}}>
    <CenteredLogoHeader variant="dark"/>
  </div>

));