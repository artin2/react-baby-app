const styles = {
  root: {
    height: '70px',
    // margin-bottom: ${noMargin? 0 : '5em'};
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: ' #fff',
    zIndex: 999,
  },
  logo: {
    height: '30px',
    objectFit: 'contain',
  },
  light: {
    backgroundColor: 'white',
  },
  dark: {
    backgroundColor: 'transparent',
  },
};

export { styles };
