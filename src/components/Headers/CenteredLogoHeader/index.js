import CenteredLogoHeader from './CenteredLogoHeader';
import enhance from './CenteredLogoHeader.enhancer';

export default enhance(CenteredLogoHeader);
