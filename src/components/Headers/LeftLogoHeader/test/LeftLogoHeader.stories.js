import React from 'react';
import { AddBoxOutlined } from '@material-ui/icons';
import { storiesOf } from '@storybook/react';
import LeftLogoHeader from '../index';

const stories = storiesOf('LeftLogoHeader', module);

stories.add('Light', () => (
  <LeftLogoHeader variant="light" />
));

stories.add('Dark', () => (
  <div style={{ background: 'black', width: '50%' }}>
    <LeftLogoHeader variant="dark" />
  </div>
));

stories.add('Icon', () => (
  <LeftLogoHeader variant="light" button={<AddBoxOutlined />} />
));
