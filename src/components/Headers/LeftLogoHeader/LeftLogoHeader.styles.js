const styles = {
  root: {
    display: 'flex',
    height: '70px',
    marginBottom: '5rem',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 2rem',
    zIndex: 999,
    flex: '70px !important',
    '&:not(:last-child)': {
      marginBottom: '1.5rem',
    },
  },
  light: {
    backgroundColor: 'white',
  },
  logoWrapper: {
    display: 'flex',
    flexBasis: 'auto',
    flexGrow: 0,
    flexShrink: 0,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  logo: {
    display: 'inline-block',
    verticalAlign: 'top',
    height: '3rem',
    objectFit: 'contain',
  },
  hideLogo: {
    visibility: 'hidden',
  },
};

export { styles };
