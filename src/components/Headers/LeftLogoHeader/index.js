import LeftLogoHeader from './LeftLogoHeader';
import enhance from './LeftLogoHeader.enhancer';

export default enhance(LeftLogoHeader);
