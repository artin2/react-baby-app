import React, { type ComponentType } from 'react';
import cn from 'classnames';
import Button from '../../Button';
import headerLogoLight from '../../../assets/images/header-logo-light@2x.png';
import headerLogoDark from '../../../assets/images/header-logo-dark@2x.png';
import { LeftLogoHeaderVariant } from '../../../config/prop-types';
import { HOME_PATH } from '../../../constants/paths';

type Props = {
  button?: ComponentType<*>,
  buttonOnly: boolean,
  classes: any,
  isSignedIn: boolean,
  onSignOut: () => {},
  onSignIn: () => {},
  variant: LeftLogoHeaderVariant,
}

const LeftLogoHeader = (props: Props) => {
  const {
    button,
    buttonOnly,
    classes,
    isSignedIn,
    onSignIn,
    onSignOut,
    variant,
  } = props;
  const btnLabel = isSignedIn ? 'sign-out' : 'sign-in';
  const onClick = isSignedIn ? onSignOut : onSignIn;
  const logo = variant === 'light' ? headerLogoLight : headerLogoDark;
  return (
    <div className={cn(classes.root, classes[variant])}>
      <div className={cn(classes.logoWrapper, { [classes.hideLogo]: buttonOnly })}>
        <a href={HOME_PATH}><img className={classes.logo} src={logo} alt="Nestling" /></a>
      </div>
      <div className={classes.btnContainer}>
        {button || (
          <Button
            label={btnLabel}
            onClick={onClick}
            variant="tertiary"
          />
        )}
      </div>
    </div>
  );
};

LeftLogoHeader.defaultProps = {
  button: null,
};

export default LeftLogoHeader;

