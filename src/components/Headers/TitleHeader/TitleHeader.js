import React from 'react';
import { Grid } from '@material-ui/core';
import { HeaderVariant } from '../../../config/prop-types';
import headerLogoNoText from '../../../assets/images/logos/nestling-logo-no-text-light.png';
import headerLogoLight from '../../../assets/images/header-logo-light@2x.png';
import headerLogoDark from '../../../assets/images/header-logo-dark@2x.png';
import { HOME_PATH } from '../../../constants/paths';

type Props = {
  classes: any,
  logoOnly: boolean,
  titleText: String,
  variant: HeaderVariant,
}
const TitleHeader = (props: Props) => {
  const {
    classes, logoOnly, titleText, variant,
  } = props;
  const getImage = () => {
    if (logoOnly) {
      return (variant === 'dark' || variant === 'transparent')
        ? headerLogoDark : headerLogoLight;
    }
    return headerLogoNoText;
  };

  return (
    <div className={classes.root}>
      <Grid container alignItems="center">
        <Grid item xs={3}>
          <a href={HOME_PATH}>
            <img className={classes.logo} src={getImage()} alt="Nestling Logo" />
          </a>
        </Grid>
        <Grid item xs={9}>
          <div className={classes.titleText}>{titleText}</div>
        </Grid>
      </Grid>
    </div>
  );
};

export default TitleHeader;
