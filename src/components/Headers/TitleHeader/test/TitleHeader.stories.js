import React from 'react';
import { storiesOf } from '@storybook/react';
import TitleHeader from '../index';

const stories = storiesOf('TitleHeader', module);

stories.add('Light', () => (
  <TitleHeader titleText="Create Your Announcement" />
));
