const styles = theme => ({
  root: {
    height: '70px',
    background: theme.palette.background.white,
    display: 'flex',
  },
  logo: {
    margin: '0 16px',
    maxHeight: '28px',
  },
  titleText: {
    fontSize: '1.6rem',
    fontFamily: theme.typography.fontFamily.primary.main,
    color: theme.palette.base.main,
  },
});

export { styles };
