import TitleHeader from './TitleHeader';
import enhance from './TitleHeader.enhancer';

export default enhance(TitleHeader);
