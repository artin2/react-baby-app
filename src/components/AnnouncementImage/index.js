import AnnouncementImage from './AnnouncementImage';
import enhance from './AnnouncementImage.enhancer';

export default enhance(AnnouncementImage);
