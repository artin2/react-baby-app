import { btnStyles } from '../../assets/styles/button.styles';

const mainFontSize = 1.4;
const smFontSize = 1;

const styles = theme => ({
  root: btnStyles,
  primary: {
    background: theme.palette.primary.main,
    color: theme.palette.white,
    border: `2px solid ${theme.palette.primary.main}`,
    fontSize: `${mainFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${mainFontSize}rem`,
    },
  },
  primaryOutline: {
    background: theme.palette.primary.main,
    color: theme.palette.white,
    border: '1.5px solid #F5F4F2',
    borderRadius: '2px',
  },
  secondary: {
    backgroundColor: theme.palette.white,
    color: theme.palette.primary.main,
    border: 'none',
    paddingTop: '1.2em',
    paddingBottom: '1.2em',
    boxShadow: 'none',
    borderRadius: '2px',
    fontSize: `${mainFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${mainFontSize}rem`,
    },
  },
  secondaryOutline: {
    color: theme.palette.primary.main,
    border: `2px solid ${theme.palette.primary.main}`,
    backgroundColor: '#fff',
    fontSize: `${mainFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${mainFontSize}rem`,
    },
  },
  tertiary: {
    backgroundColor: 'transparent',
    color: theme.palette.white,
    border: `2px solid ${theme.palette.primary.main}`,
    padding: '9px 12px',
    fontSize: `${smFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${smFontSize}rem`,
    },
    boxShadow: 'none',
  },
  tertiaryLight: {
    backgroundColor: 'transparent',
    color: theme.palette.primary.main,
    border: `2px solid ${theme.palette.primary.main}`,
    padding: '9px 12px',
    fontSize: `${smFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${smFontSize}rem`,
    },
    boxShadow: 'none',
  },
  configOption: {
    background: theme.palette.background.white,
    border: '1px solid #848A99',
    borderRadius: 0,
    fontSize: '1.2rem',
    color: theme.palette.text.disabled,
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    lineHeight: '24px',
    padding: '5px 0',
  },
  sm: {

  },
  md: {
    padding: '9px 8%',
    fontSize: '10px',
    width: '100%',
  },
  lg: {
    padding: '2rem 0',
  },
});

export { styles };
