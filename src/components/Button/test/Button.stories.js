import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from '../index';
import ButtonGroup from '../../ButtonGroup';

const stories = storiesOf('Button', module);

stories.add('Primary', () => (
  <Button variant="primary" label="sign-in" />
));

stories.add('Primary-Outline', () => (
  <div style={{ background: '#E7228E', width: '100%', padding: '10px' }}>
    <Button variant="primaryOutline" label="sign-in" />
  </div>
));

stories.add('Secondary', () => (
  <Button variant="secondary" label="sign-in" />
));

stories.add('Secondary-Outline', () => (
  <Button variant="secondaryOutline" label="sign-in" />
));

stories.add('Tertiary', () => (
  <Button variant="tertiary" label="sign-in" />
));

stories.add('Tertiary-Light', () => (
  <Button variant="tertiaryLight" label="sign-in" />
));

stories.add('ConfigOption', () => (
  <Button variant="configOption" label="sign-in" />
));

stories.add('Button Group', () => (
  <ButtonGroup size="md">
    <Button variant="primary" label="one" />
    <Button variant="secondary" label="two" />
  </ButtonGroup>
));
