import React from 'react';
import cn from 'classnames';
import { ButtonSize, ButtonVariant } from '../../config/prop-types';
/**
   * primary: pink bg and white text
   * secondary: white bg, pink text and outline
   * tertiary: transparent bg, pink outline, white text
   */

type Props = {
  classes: any,
  label: string,
  onClick: () => {},
  variant: ButtonVariant,
  sz: ButtonSize,
}

const Button = (props: Props) => {
  const {
    classes,
    label,
    onClick,
    sz,
    variant,
  } = props;
  return (
    <button type="submit" className={cn(classes.root, classes[variant], classes[sz])} onClick={onClick}>{label}</button>
  );
};

export default Button;

