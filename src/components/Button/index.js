import Button from './Button';
import enhance from './Button.enhancer';

export default enhance(Button);
