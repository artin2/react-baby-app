import React from 'react';
import { Avatar } from '@material-ui/core';
import cn from 'classnames';

type Props = {
  altText: string,
  classes: any,
  hasBorder: boolean,
  image: any,
}

const RoundImage = (props: Props) => {
  const {
    altText,
    classes,
    hasBorder,
    image,
  } = props;
  return (
    <Avatar
      alt={altText}
      src={image}
      className={cn(classes.root, { [classes.border]: hasBorder })}
    />
  );
};

export default RoundImage;
