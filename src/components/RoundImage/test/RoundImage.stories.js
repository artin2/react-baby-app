import React from 'react';
import { storiesOf } from '@storybook/react';
import RoundImage from '../index';
import profilePic from '../../../assets/images/profile-pic2.jpg';

const stories = storiesOf('RoundImage', module);

stories.add('RoundImage', () => (
  <RoundImage image={profilePic} />
));
