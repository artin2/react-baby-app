import RoundImage from './RoundImage';
import enhance from './RoundImage.enhancer';

export default enhance(RoundImage);

