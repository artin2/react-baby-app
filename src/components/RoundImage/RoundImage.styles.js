const styles = theme => ({
  root: {
    height: '88px',
    width: '88px',
  },
  border: {
    border: `4px solid ${theme.palette.white}`,
    margin: 'auto',
    marginTop: '-44px',
  },
});

export { styles };
