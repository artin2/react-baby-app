import AnnouncementInfo from './AnnouncementInfo';
import enhance from './AnnouncementInfo.enhancer';

export default enhance(AnnouncementInfo);
