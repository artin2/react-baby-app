import React, { SyntheticEvent } from 'react';
import { Grid } from '@material-ui/core';
import TextField from '../InputElements/TextField';
import Select from '../InputElements/Select';
import { months, years, gender } from './dropdown-constants';

type Props = {
  classes: any,
  onChange: (any) => void,
}
const AnnouncementInfo = (props: Props) => {
  const {
    classes,
    onChange,
  } = props;

  const handleOnChange = (event: SyntheticEvent) => {
    const { target: { name } } = event;
    onChange(event, name);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <TextField
            label="Announcement Message"
            id="announcementMessage"
            variant="light"
            onChange={handleOnChange}
            placeholder="Announcement Message"
          />
        </Grid>
      </Grid>
      <Grid container spacing={16}>
        <Grid item xs={6}>
          <Select
            label="Due Date"
            id="dueDateMonth"
            variant="light"
            options={months}
            onChange={handleOnChange}
          />
        </Grid>
        <Grid item xs={6}>
          <Select
            id="dueDateYear"
            variant="light"
            options={years}
          />
        </Grid>
      </Grid>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Select
            id="gender"
            label="Gender"
            variant="light"
            options={gender}
            onChange={handleOnChange}
            placeholder="Gender"
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default AnnouncementInfo;
