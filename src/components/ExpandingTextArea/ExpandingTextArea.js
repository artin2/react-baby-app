/* eslint-disable no-bitwise */
/* eslint-disable no-param-reassign */
import React, { useState } from 'react';

type Props = {
  classes: any,
  label?: string,
  onChange: () => {},
  placeholder?: string,
}

const ExpandingTextArea = (props: Props) => {
  const { classes, label, placeholder } = props;

  const [rows, setRows] = useState(3);
  const [value, setValue] = useState('');
  const minRows = 2;
  const maxRows = 50;

  const handleChange = (event) => {
    const lineHeight = 24;

    const prevRowCount = event.target.rows;
    event.target.rows = minRows;

    const currentRows = ~~(event.target.scrollHeight / lineHeight);
    if (currentRows === prevRowCount) {
      event.target.rows = currentRows;
    }

    if (currentRows >= maxRows) {
      event.target.rows = maxRows;
      event.target.scrollTop = event.target.scrollHeight;
    }

    setValue(event.target.value);
    setRows(currentRows < maxRows ? currentRows : maxRows);
  };

  const onBlur = (event) => {
    const { onChange } = props;
    onChange(event);
  };

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <textarea
          aria-label={label}
          className={classes.textarea}
          onBlur={onBlur}
          onChange={handleChange}
          placeholder={placeholder}
          rows={rows}
          value={value}
        />
      </div>
    </div>
  );
};

ExpandingTextArea.defaultProps = {
  label: 'texarea',
  placeholder: 'Start typing...',
};

export default ExpandingTextArea;

