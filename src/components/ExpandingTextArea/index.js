import ExpandingTextArea from './ExpandingTextArea';
import enhance from './ExpandingTextArea.enhancer';

export default enhance(ExpandingTextArea);
