
const styles = theme => ({
  root: {
    borderRadius: '4px 4px 0 0',
    background: 'white',
    position: 'absolute',
    bottom: '0',
    left: 0,
    width: '100%',
    zIndex: 999,
  },
  container: {
    padding: '40px 24px 11rem 24px',
    width: 'inherit',
    height: '100%',
  },
  textarea: {
    overflow: 'auto',
    height: 'auto',
    width: '100%',
    fontSize: `${1.5 * 0.8}rem`,
    lineHeight: '1.5',
    border: 0,
    '-webkit-appearance': 'none',
    resize: 'none',
    fontFamily: theme.typography.fontFamily.secondary,
    [theme.breakpoints.up('sm')]: {
      fontSize: '1.5rem',
    },
  },
});

export { styles };
