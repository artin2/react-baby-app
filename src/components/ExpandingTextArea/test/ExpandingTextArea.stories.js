import React from 'react';
import { storiesOf } from '@storybook/react';
import ExpandingTextArea from '../index';
import '../../../index.css';
import { globalStyleVars as g } from '../../../assets/styles/global-variables';

const stories = storiesOf('ExpandingTextArea', module);

stories.add('ExpandingTextArea', () => (
  <div style={{ background: g.baseBlue, width: '50vw', height: '100vh' }}>
    <ExpandingTextArea placeholder="Start typing...." />
  </div>
));
