import React from 'react';
import { storiesOf } from '@storybook/react';
import AnnouncementPreview from '../index';

const stories = storiesOf('AnnouncementPreview', module);

const msg = `Guess what!!! Baby #2 is on the way!!! We can’t wait to meet our newest bundle of “joy”, 
and we couldn't think of a better way to celebrate than with you.`;
const dueDate = 'May 2019';
const gender = 'Hoping for a girl';
stories.add('AnnouncementPreview', () => (
  <AnnouncementPreview announcementMessage={msg} dueDate={dueDate} gender={gender} />
));
