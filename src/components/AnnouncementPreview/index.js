import AnnouncementPreview from './AnnouncementPreview';
import enhance from './AnnoucementPreview.enhancer';

export default enhance(AnnouncementPreview);
