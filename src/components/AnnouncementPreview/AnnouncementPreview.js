import React from 'react';
import { Grid } from '@material-ui/core';

type Props = {
  classes: any,
  announcementMessage: string,
  dueDate: String,
  gender: String, // Will have a type
}
const AnnouncementPreview = (props: Props) => {
  const {
    classes,
    announcementMessage,
    dueDate,
    gender,
  } = props;
  return (
    <div className={classes.root}>
      <div className={classes.section}>
        <div className={classes.announcementMessageContainer}>
          {announcementMessage}
        </div>
        <hr className={classes.divider} />
        <Grid container className={classes.infoSection} spacing={40}>
          <Grid item={5}>
            <div className={classes.infoContainer}>
              <div className={classes.label}>Due Date</div>
              <div className={classes.info}>{dueDate}</div>
            </div>
          </Grid>
          <Grid item={7}>
            <div className={classes.infoContainer}>
              <div className={classes.label}>Gender</div>
              <div className={classes.info}>{gender}</div>
            </div>
          </Grid>
        </Grid>
      </div>

    </div>
  );
};

export default AnnouncementPreview;

