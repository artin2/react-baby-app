
const styles = theme => ({
  root: {
    fontSize: '1.6rem',
  },
  announcementMessageContainer: {
    fontSize: '1.6rem',
    fontFamily: theme.typography.fontFamily.primary.main,
    lineHeight: 1.375,
  },
  infoSection: {
  },
  divider: {
    color: theme.palette.gray,
    margin: '24px 0',
  },
  label: {
    fontSize: '1.6rem',
    fontFamily: theme.typography.fontFamily.primary.bold,
    paddingBottom: '10px',
  },
  info: {
    fontColor: theme.palette.text.label,
    fontSize: '1.6rem',
    fontFamily: theme.typography.fontFamily.primary.main,
  },
});

export { styles };
