import React from 'react';

type Props = {
  classes: any,
  label: String,
}

const ShareSubText = (props: Props) => {
  const { classes, label } = props;
  return (
    <p className={classes.root}>{label}</p>
  );
};

export default ShareSubText;

