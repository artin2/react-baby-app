const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: '1.3rem',
    lineHeight: '22px',
  },
});

export { styles };
