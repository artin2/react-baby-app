import ShareSubtext from './ShareSubtext';
import enhance from './ShareSubtext.enhancer';

export default enhance(ShareSubtext);
