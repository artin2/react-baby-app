import React from 'react';
import { storiesOf } from '@storybook/react';
import ShareSubtext from '../index';

const stories = storiesOf('ShareSubtext', module);

stories.add('Normal', () => (
  <ShareSubtext label="This is my subtext" />
));
