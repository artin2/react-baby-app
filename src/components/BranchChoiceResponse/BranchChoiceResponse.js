import React from 'react';

type Props = {
  classes: any,
  content: string,
}
const BranchChoiceResponse = (props: Props) => {
  const { classes, content } = props;
  return (
    <div className={classes.root}>{content}</div>
  );
};

export default BranchChoiceResponse;

