const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.medium,
    fontSize: '1.5rem',
    lineHeight: '24px',
  },
});

export { styles };
