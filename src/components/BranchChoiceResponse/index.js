import BranchChoiceResponse from './BranchChoiceResponse';
import enhance from './BranchChoiceResponse.enhancer';

export default enhance(BranchChoiceResponse);
