import React from 'react';
import { storiesOf } from '@storybook/react';
import ShareSubHeader from '../index';

const stories = storiesOf('ShareSubHeader', module);

stories.add('Normal', () => (
  <ShareSubHeader label="Sub header" />
));
