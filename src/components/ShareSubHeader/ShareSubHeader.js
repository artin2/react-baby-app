import React from 'react';

type Props = {
  classes: any,
  label: string
}
const ShareSubHeader = (props: Props) => {
  const { classes, label } = props;
  return (
    <div className={classes.root}>{label}</div>
  );
};
export default ShareSubHeader;

