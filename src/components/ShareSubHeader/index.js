import ShareSubHeader from './ShareSubHeader';
import enhance from './ShareSubHeader.enhancer';

export default enhance(ShareSubHeader);
