const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.3rem',
  },
});

export { styles };
