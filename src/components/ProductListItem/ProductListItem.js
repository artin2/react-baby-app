/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Grid } from '@material-ui/core';
import ProgressiveImage from 'react-progressive-image';
import Chevron from '../Chevron';

type Props = {
  classes: any,
  id: any,
  image: any,
  onClick: () => {},
  price: string,
  productName: String,
}

const ProductListItem = (props: Props) => {
  const handleClick = () => {
    const { id, onClick } = props;
    onClick(id);
  };

  const {
    classes,
    image,
    price,
    productName,
  } = props;

  return (
    <Grid container className={classes.root}>
      <Grid item xs={4}>
        <ProgressiveImage src={image.large} placeholder={image.small} delay={2000}>
          {src => <img className={classes.img} src={src} alt={productName} />}
        </ProgressiveImage>
      </Grid>
      <Grid item xs={8}>
        <div className={classes.description}>{productName}</div>
        <div className={classes.description}>${price}</div>
        <Grid container alignItems="center" spacing={0} onClick={handleClick}>
          <Grid item xs={9}>
            <div className={classes.recommend}>Recommend This</div>
          </Grid>
          <Grid item xs={1}>
            <Chevron variant="light" />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ProductListItem;

