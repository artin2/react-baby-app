import ProductListItem from './ProductListItem';
import enhance from './ProductListItem.enhancer';

export default enhance(ProductListItem);
