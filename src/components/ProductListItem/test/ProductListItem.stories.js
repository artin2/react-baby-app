import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductListItem from '../index';
import product from '../../../assets/images/p2@2x.png';

const stories = storiesOf('ProductListItem', module);

const description = 'Boba Baby Wrap Carrier, Grey $39.95 - Free Shipping';

stories.add('ProductListItem', () => (
  <ProductListItem description={description} img={product} />
));
