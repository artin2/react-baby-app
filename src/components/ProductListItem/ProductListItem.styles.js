const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main.demiBold,
    padding: '8px 0',
  },
  description: {
    fontSize: '1.2rem',
    fontWeight: 600,
    color: theme.palette.black,
    // width: '80%',
    lineHeight: '18px',
  },
  img: {
    height: '9rem',
    borderRadius: '4px',
    // width: '9rem',
    objectContain: 'cover',
  },
  recommend: {
    fontFamily: theme.typography.fontFamily.tertiary,
    fontSize: '1.2rem',
    fontWeight: 600,
    color: theme.palette.primary.main,
    padding: '2rem 0',
    display: 'flex',
  },
  chevron: { // TODO: create a component for this?
    '&:before': {
      borderColor: theme.palette.primary.main,
      borderStyle: 'solid',
      borderWidth: '0.15em 0.15em 0 0',
      content: '""',
      display: 'inline-block',
      height: '0.80em',
      left: '35%',
      position: 'relative',
      top: '0.5em',
      transform: 'rotate(45deg)',
      verticalAlign: 'top',
      width: '0.80em',
    },
  },
});

export { styles };
