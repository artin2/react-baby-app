import React from 'react';
import { storiesOf } from '@storybook/react';
import ImageBackgroundContainer from '../index';
import BranchHeader from '../../BranchHeader';

const stories = storiesOf('ImageBackgroundContainer', module);

const requiredProps = () => ({
  image: 'https://picsum.photos/id/104/400',
})

stories.add('Normal', () => (
  <ImageBackgroundContainer {...requiredProps()}>
    <BranchHeader header="Recommendations" variant="dark" />
    <div styles={{ fontFamily: 'Avenir-Next-Bold', fontSize: '2rem', color: 'white' }}>I swear by the Boba! We wear it everywhere.I even wear it at home!</div>
  </ImageBackgroundContainer>
));
