import ImageBackgroundContainer from './ImageBackgroundContainer';
import enhance from './ImageBackgroundContainer.enhancer';

export default enhance(ImageBackgroundContainer);
