import React from 'react';
// import ProgressiveImage from 'react-progressive-image';

type Props = {
  classes: any,
  image: String,
  children: any,
}

const ImageBackgroundContainer = (props: Props) => {
  const { children, classes, image } = props;
  console.log(image);
  return (
    <div className={classes.root}>
      {/* <ProgressiveImage src={image.large} placeholder={image.small} delay={1000}>
        {src => <img src={src} alt="Nest" className={classes.image} />}
      </ProgressiveImage> */}
      <img className={classes.image} src={image} alt="dream catcher" />
      <div className={classes.overlay}>
        <div className={classes.section}>
          <div className={classes.container}>
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageBackgroundContainer;

