import { section, container } from '../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    maxHeight: '500px',
    height: '100%',
    width: '100%',
    position: 'relative',
  },
  overlay: {
    background: theme.palette.base.main,
    bottom: 0,
    left: 0,
    position: 'absolute',
    zIndex: 9,
    opacity: 0.85,
    width: '100%',
    height: '100%',
  },
  section,
  container,
  image: {
    width: '100%',
    height: 'auto',
  },
});

export { styles };
