const headerFontSize = 1.9;
const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    borderRadius: '8px',
    background: theme.palette.primary.light,
    width: '100%',
    height: '88px',
    color: theme.palette.white,
    boxShadow: '0 8px 8px rgba(0,0,0,0.16)',
  },
  content: {
    padding: '2rem',
  },
  header: {
    fontSize: `${headerFontSize * 0.8}rem`,
    fontFamily: theme.typography.fontFamily.primary.medium,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${headerFontSize}rem`,
    },
    marginBottom: '1rem',
  },
  link: {
    fontSize: '1.1rem',
  },
  chevron: {
    float: 'right',
  },
});

export { styles };
