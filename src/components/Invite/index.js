import Invite from './Invite';
import enhance from './Invite.enhancer';

export default enhance(Invite);
