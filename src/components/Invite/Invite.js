import React from 'react';
import { Grid } from '@material-ui/core';
import Chevron from '../Chevron';

type Props = {
  classes: any,
  header: String,
}

const Invite = (props: Props) => {
  const { classes, header } = props;
  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <div className={classes.header}>{header}</div>
        <Grid container item xs={12} alignItems="center" justify="flex-end">
          <Grid item xs={10}>
            <div className={classes.link}>Send a nestling invite</div>
          </Grid>
          <Grid item xs={2}>
            <Chevron classes={{ root: classes.chevron }} direction="right" />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default Invite;

