import React from 'react';
import { storiesOf } from '@storybook/react';
import Invite from '../index';

const stories = storiesOf('Invite', module);

const requiredProps = () => ({
  header: 'Get your friends in the mix',
});

stories.add('Test', () => (
  <Invite {...requiredProps()} />
));
