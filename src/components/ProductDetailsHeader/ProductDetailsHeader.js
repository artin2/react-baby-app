/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import cn from 'classnames';

type Props = {
  classes: any,
  price: String,
  productName: String,
  shippingCost: String,
  source: any,
}
const ProductDetailsHeader = (props: Props) => {
  const {
    classes,
    productName,
    price,
    shippingCost,
    source,
  } = props;

  const mapShippingCost = () => {
    const value = shippingCost === '0' ? 'Free Shipping' : shippingCost;
    return `${value} -`;
  };

  return (
    <div className={classes.root}>
      <div className={classes.productName}>{productName}</div>
      <div className={classes.priceDetails}>
        <div className={classes.details}>${price}&nbsp;-&nbsp;</div>
        <div className={classes.details}>{mapShippingCost()}&nbsp;</div>
        <a className={cn(classes.details, classes.link)} href={source.value}>{source.label}</a>
      </div>
    </div>
  );
};

export default ProductDetailsHeader;
