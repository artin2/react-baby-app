import ProductDetailsHeader from './ProductDetailsHeader';
import enhance from './ProductDetailsHeader.enhancer';

export default enhance(ProductDetailsHeader);
