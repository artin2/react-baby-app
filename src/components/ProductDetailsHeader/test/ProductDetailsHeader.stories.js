import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductDetailsHeader from '../index';

const stories = storiesOf('ProductDetailsHeader', module);

const requiredProps = () => ({
  productName: 'Totokan Portable Baby Monitor',
  price: '249',
  shippingCost: '0',
  source: {
    label: 'Amazon',
    value: 'amazon.com',
  },
});
stories.add('Normal', () => (
  <div style={{ background: '#091633', padding: '10px 0' }}>
    <ProductDetailsHeader {...requiredProps()} />
  </div>
));
