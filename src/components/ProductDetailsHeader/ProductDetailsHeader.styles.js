const headerFontSize = 2;
const detailFontSize = 1.4;
const styles = theme => ({
  root: {
    color: theme.palette.white,
  },
  productName: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: `${headerFontSize * 0.8}rem`,
    zIndex: 999,
    textAlign: 'center',
    margin: '2rem 0 1rem 0',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${headerFontSize}rem`,
    },
  },
  priceDetails: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
  },
  details: {
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: `${detailFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${detailFontSize}rem`,
    },
  },
  link: {
    color: theme.palette.white,
  },
});

export { styles };
