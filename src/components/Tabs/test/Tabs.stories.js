import React, { PureComponent } from 'react';
import { storiesOf } from '@storybook/react';
import Tabs from '../index';
import Tab from '../Tab/index';

const stories = storiesOf('Tabs', module);

const tabLabels = [
  {
    id: 0,
    label: 'Nests',
  },
  {
    id: 1,
    label: 'Branches',
  },
];

class Content extends PureComponent {
  state = {
    value: 0,
  }

  handleClick = (e, value) => {
    e.preventDefault();
    this.setState({ value });
  }

  render() {
    const { value } = this.state;
    return (
      <div>
        <Tabs value={value} onClick={this.handleClick}>
          <Tab label="Nest" />
          <Tab label="Branch" />
        </Tabs>
        {value === 0 ? <div>Content 1</div> : <div>Content 2</div>}
      </div>
    )
  }
}

stories.add('One Tab', () => (
  <Tab label="content" />
));

stories.add('One Tab Selected', () => (
  <div style={{ width: '20%', display: 'flex' }}>
    <Tab label="content" selected />
  </div>

));

stories.add('Tabs', () => (
  <Content tabLabels={tabLabels} />
));
