import React, {
  Children,
  isValidElement,
  cloneElement,
} from 'react';

type Props = {
  children: any,
  classes: any,
  onClick: any,
  // selected: any,
  value: any,
}

const Tabs = (props: Props) => {
  const {
    children: childrenProp,
    classes,
    onClick,
    value,
  } = props;
  let childIndex = 0;
  const children = Children.map(childrenProp, (child) => {
    if (!isValidElement(child)) {
      return null;
    }
    const childValue = child.props.value === undefined ? childIndex : child.props.value;
    childIndex += 1;
    return cloneElement(child, {
      selected: childValue === value,
      onClick,
      value: childValue,
    });
  });

  return (
    <div className={classes.root}>
      {children}
    </div>
  );
};

export default Tabs;

