import { globalStyleVars as g } from '../../assets/styles/global-variables';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    display: 'flex',
    background: g.white,
  },
});

export { styles };
