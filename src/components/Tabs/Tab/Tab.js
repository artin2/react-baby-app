import React from 'react';
import cn from 'classnames';

type Props = {
  classes: any,
  onClick: any,
  selected: any,
  label: string,
  value: any,
}

const Tab = (props: Props) => {
  const {
    onClick,
    classes,
    selected,
    label,
    value,
  } = props;
  const handleClick = (e) => {
    onClick(e, value);
  };

  return (
    <div
      className={cn(classes.label, { [classes.selected]: selected })}
      onClick={handleClick}
      onKeyPress={handleClick}
      role="button"
      tabIndex={0}
      value={value}
    >
      {label}
    </div>
  );
};

export default Tab;

