import Tab from './Tab';
import enhance from './Tab.enhancer';

export default enhance(Tab);
