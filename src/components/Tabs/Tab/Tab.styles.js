const styles = theme => ({
  label: {
    color: theme.palette.primary.main,
    fontSize: '13px',
    padding: '14px 12px',
    fontFamily: theme.typography.fontFamily.primary.bold,
    '&:hover': {
      cursor: 'pointer',
      '&:before, &:after': {
        width: '40%',
        opacity: 1,
      },
    },
  },
  selected: {
    position: 'relative',
    // transition: 'all 0.2s ease-in-out',
    '&:before, &:after': {
      content: '""',
      position: 'absolute',
      bottom: '6px',
      width: '0px',
      height: '5px',
      margin: '5px 0 0',
      // transition: 'all 0.2s ease-in-out',
      animation: '$underline 0.2s ease-in forwards',
      transitionDuration: '.75s',
      opacity: 0,
    },
    '&:before': {
      left: 'calc(50%)',
      background: theme.palette.primary.main,
    },
    '&:after': {
      right: 'calc(50%)',
      background: theme.palette.primary.main,
    },
  },
  '@keyframes underline': {
    from: {
      width: 0,
      opacity: 0,
    },
    to: {
      width: '40%',
      opacity: 1,
    },
  },
});

export { styles };
