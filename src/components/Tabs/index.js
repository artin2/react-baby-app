import Tabs from './Tabs';
import enhance from './Tabs.enhancer';

export default enhance(Tabs);
