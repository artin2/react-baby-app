import SignIn from './SignIn';
import enhance from './SignIn.enhancer';

export default enhance(SignIn);
