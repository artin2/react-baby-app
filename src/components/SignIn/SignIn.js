import React from 'react';
import FirebaseAuth from 'react-firebaseui/FirebaseAuth';

type Props = {
  classes: any,
  firebase: any,
  uiConfig: any,
}
const SignIn = (props: Props) => {
  const {
    classes,
    firebase,
    uiConfig,
  } = props;

  return (
    <div className={classes.root}>
      <div className={classes.header}>{'Let\'s Create Your Account'}</div>
      <FirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
    </div>
  );
};

export default SignIn;

