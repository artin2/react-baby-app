import React from 'react';
import { storiesOf } from '@storybook/react';
import SignIn from '../index';
import firebase from '../../../config/firebase';

const stories = storiesOf('SignIn', module);

const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: 'popup',
  // Redirect to /signedIn after sign in is successful.
  // Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: '/save-nest',
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ],
  callbacks: {
    signInSuccessWithAuthResult: (authResult) => {
      console.log('authResult', authResult.user.uid);
      return true;
    },
  },
};
stories.add('Normal', () => (
  <SignIn firebase={firebase} uiConfig={uiConfig} />
));
