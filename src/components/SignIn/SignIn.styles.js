const styles = theme => ({
  root: {
    background: theme.palette.white,
    borderRadius: '25px 25px 0 0',
    paddingTop: '3.2rem',
    position: 'absolute',
    width: '100%',
    bottom: 0,
    right: 0,
  },
  header: {
    fontSize: '1.6rem',
    fontFamily: theme.typography.fontFamily.primary.bold,
    textAlign: 'center',
    paddingBottom: '2.7rem',
  },
});

export { styles };
