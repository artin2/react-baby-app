const styles = theme => ({
  root: {
    background: theme.palette.background.white,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  link: {
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1rem',
    padding: '20px',
    textTransform: 'uppercase',
    textDecoration: 'underline',
  },
});

export { styles };
