import React from 'react';

type Props = {
  classes: any,
}

const Footer = (props: Props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <div className={classes.link}>contact us</div>
      <div className={classes.link}>help & faq</div>
      <div className={classes.link}>our policies</div>
    </div>
  );
};

export default Footer;
