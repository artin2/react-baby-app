import Footer from './Footer';
import enhance from './Footer.enhancer';

export default enhance(Footer);
