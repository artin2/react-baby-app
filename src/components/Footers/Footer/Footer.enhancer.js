import { withStyles } from '@material-ui/core/styles';
import { memo } from 'react';
import { compose } from 'redux';
import { styles } from './Footer.styles';

export default compose(
  memo,
  withStyles(styles),
);
