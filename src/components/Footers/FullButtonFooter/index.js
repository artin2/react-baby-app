import FullButtonFooter from './FullButtonFooter';
import enhance from './FullButtonFooter.enhancer';

export default enhance(FullButtonFooter);
