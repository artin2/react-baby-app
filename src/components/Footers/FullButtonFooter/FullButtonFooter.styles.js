const styles = theme => ({
  root: {
    top: 'auto',
    position: 'fixed',
    bottom: 0,
    right: 0,
    cursor: 'pointer',
    textTransform: 'uppercase',
    fontStyle: 'normal',
    fontWeight: 'bold',
    textAlign: 'center',
    textDecoration: 'none',
    userSelect: 'none',
    fontFamily: theme.typography.fontFamily.primary.bold,
    boxShadow: '1px 3px 15px 0px rgba(123, 123, 123, 0.4)',
    zIndex: 999,
    // [theme.breakpoints.down('sm')]: {
    //   padding: 0,
    // },
  },
  label: {
    textAlign: 'center',
    margin: 'auto',
    fontSize: '1.4rem',
  },
  secondary: {
    background: '#FFFFFF',
    color: theme.palette.primary.main,
  },
  disabled: {
    '& $label': {
      color: '#DCDBD9',
    },
  },
});

export { styles };
