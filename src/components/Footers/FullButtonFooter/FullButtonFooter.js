import React from 'react';
import cn from 'classnames';
import { AppBar, Toolbar } from '@material-ui/core';
import { FullButtonFooterVariant } from '../../../config/prop-types';

type Props = {
  classes: any,
  disabled: Boolean,
  label: string,
  onClick: () => {},
  variant: FullButtonFooterVariant,
}

const FullButtonFooter = (props: Props) => {
  const {
    classes,
    disabled,
    label,
    onClick,
    variant,
  } = props;

  const disableFunc = () => {};
  const canClick = disabled ? disableFunc : onClick;

  return (
    <AppBar
      position="fixed"
      color={variant || 'primary'}
      className={cn(classes.root, { [classes.disabled]: disabled })}
      onClick={canClick}
      classes={{ colorSecondary: classes.secondary }}
    >
      <Toolbar className={classes.toolbar}>
        <div className={classes.label}>{label}</div>
      </Toolbar>
    </AppBar>
  );
};

export default FullButtonFooter;
