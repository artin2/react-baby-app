import NavigationFooter from './NavigationFooter';
import enhance from './NavigationFooter.enhancer';

export default enhance(NavigationFooter);
