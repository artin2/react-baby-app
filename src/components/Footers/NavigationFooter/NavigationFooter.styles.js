const styles = theme => ({
  root: {
    top: 'auto',
    position: 'fixed',
    bottom: 0,
    right: 0,
    cursor: 'pointer',
    textTransform: 'uppercase',
    fontStyle: 'normal',
    fontWeight: 'bold',
    textAlign: 'middle',
    textDecoration: 'none',
    userSelect: 'none',
    fontFamily: theme.typography.fontFamily.primary.bold,
    boxShadow: '1px 3px 15px 0px rgba(123, 123, 123, 0.4)',
    zIndex: 999,

  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    verticalAlign: 'center',
  },
  nav: {
    color: theme.palette.white,
    textDecoration: 'none !important',
    float: 'left',
    fontSize: `${1.4 * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: '1.4rem',
    },
    // justifySelf: 'flex-start',
  },
  navRight: {
    // justifySelf: 'flex-end',
    float: 'right',
  },
  gutter: {
    paddingRight: '1.4rem',
    paddingLeft: '1rem',
  },
});

export { styles };
