import React from 'react';
import { storiesOf } from '@storybook/react';
import NavigationFooter from '../index';

const stories = storiesOf('NavigationFooter', module);

stories.add('NavigationFooter', () => (
  <NavigationFooter backLabel="skip" nextLabel="next"/>
));
