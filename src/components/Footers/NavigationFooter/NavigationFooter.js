import React from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import cn from 'classnames';
import Chevron from '../../Chevron';

type Props = {
  backLabel?: string,
  classes: any,
  backIcon?: boolean,
  nextLabel: string,
  onNextClick: () => {},
  onBackClick: () => {},
}

const NavigationFooter = (props: Props) => {
  const {
    backLabel,
    backIcon,
    classes,
    nextLabel,
    onBackClick,
    onNextClick,
  } = props;
  return (
    <AppBar position="fixed" color="primary" className={classes.root}>
      <Toolbar className={classes.toolbar} classes={{ gutters: classes.gutter }}>
        <div
          id="back"
          className={classes.nav}
          onClick={onBackClick}
          onKeyPress={onBackClick}
          role="button"
          tabIndex={0}
        >
          {backIcon ? <Chevron direction="left" variant="dark" /> : backLabel}
        </div>
        <div
          id="forward"
          className={cn(classes.nav, classes.navRight)}
          onClick={onNextClick}
          onKeyPress={onNextClick}
          role="button"
          tabIndex={0}
        >
          {nextLabel}
        </div>
      </Toolbar>
    </AppBar>
  );
};

NavigationFooter.defaultProps = {
  backLabel: '',
  backIcon: true,
};

export default NavigationFooter;

