import ButtonFooter from './ButtonFooter';
import enhance from './ButtonFooter.enhancer';

export default enhance(ButtonFooter);
