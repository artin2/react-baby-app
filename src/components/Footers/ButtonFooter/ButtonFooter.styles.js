const styles = theme => ({
  footer: {
    top: 'auto',
    zIndex: '99',
    // height: '56px',
    width: '100%',
    background: theme.palette.primary.main,
    position: 'fixed',
    bottom: 0,
    right: 0,
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    justifyContent: 'space-between',
    padding: 0,
  },
});

export { styles };
