import React from 'react';
import { storiesOf } from '@storybook/react';
import ButtonFooter from '../index';

const stories = storiesOf('ButtonFooter', module);

stories.add('Primary', () => (
  <ButtonFooter leftLabel="Find a Friend" rightLabel="Start a Book" />
));
