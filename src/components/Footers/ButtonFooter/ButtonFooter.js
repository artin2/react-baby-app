import React from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Button from '../../Button';

type Props = {
  classes: any,
  leftLabel: string,
  onClick: any,
  rightLabel: string,
}

const ButtonFooter = (props: Props) => {
  const {
    classes, leftLabel, onClick, rightLabel,
  } = props;

  return (
    <AppBar position="fixed" color="primary" className={classes.footer}>
      <Toolbar className={classes.toolbar}>
        <div style={{ width: '100%', margin: '5px 5px 5px 2.5px' }}>
          <Button label={leftLabel} variant="secondary" />
        </div>
        <div style={{ width: '100%', margin: '5px 5px 5px 2.5px' }}>
          <Button label={rightLabel} variant="primary" onClick={onClick} />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default ButtonFooter;

