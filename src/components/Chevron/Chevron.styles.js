const styles = theme => ({
  root: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
    },
  },
  light: {
    color: theme.palette.primary.main,
  },
  dark: {
    color: '#FFFFFF',
  },

});

export { styles };
