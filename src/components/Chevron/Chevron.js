/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import {
  ChevronRight,
  ChevronLeft,
  ExpandLess,
  ExpandMore,
} from '@material-ui/icons';
import { Direction, IconVariant } from '../../config/prop-types';

type Props = {
  classes: any,
  direction: Direction,
  variant: IconVariant,
}
const Chevron = (props: Props) => {
  const { classes } = props;
  const renderIcon = () => {
    const { direction, variant } = props;
    switch (direction) {
      case 'up': {
        return <ExpandLess className={classes[variant]} />;
      }
      case 'down':
        return <ExpandMore classes={{ root: classes.root }} className={classes[variant]} />;
      case 'left':
        return <ChevronLeft classes={{ root: classes.root }} className={classes[variant]} />;
      case 'right':
        return <ChevronRight classes={{ root: classes.root }} className={classes[variant]} />;
      default:
        return <ChevronRight classes={{ root: classes.root }} className={classes[variant]} />;
    }
  };
  return (
    <div>
      {renderIcon()}
    </div>
  );
};

export default Chevron;

