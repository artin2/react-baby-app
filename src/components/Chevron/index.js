import Chevron from './Chevron';
import enhance from './Chevron.enhancer';

export default enhance(Chevron);
