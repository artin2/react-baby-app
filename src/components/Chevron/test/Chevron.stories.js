import React from 'react';
import { storiesOf } from '@storybook/react';
import Chevron from '../index';

const stories = storiesOf('Chevron', module);

stories.add('Left', () => (
  <Chevron direction="left" variant="light" />
));

stories.add('Up', () => (
  <Chevron direction="up" />
));
stories.add('Down', () => (
  <Chevron direction="down" />
));

stories.add('Right', () => (
  <Chevron direction="right" />
));
