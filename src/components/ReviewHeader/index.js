import ReviewHeader from './ReviewHeader';
import enhance from './ReviewHeader.enhancer';

export default enhance(ReviewHeader);
