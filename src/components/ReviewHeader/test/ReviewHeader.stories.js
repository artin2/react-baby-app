import React from 'react';
import { storiesOf } from '@storybook/react';
import ReviewHeader from '../index';

const stories = storiesOf('ReviewHeader', module);

stories.add('Normal', () => (
  <ReviewHeader label="Review Your Branch" />
));

stories.add('Disabled', () => (
  <ReviewHeader label="Review Your Branch" disabled />
));
