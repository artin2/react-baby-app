const fontSize = 1.5;
const styles = theme => ({
  root: {
    background: '#FFFFFF',
    fontFamily: theme.typography.fontFamily.primary.bold,
    padding: '28px',
    textAlign: 'center',
  },
  label: {
    fontSize: `${fontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${fontSize}rem`,
    },
  },
  disabled: {
    color: theme.palette.text.disabled,
  },
});

export { styles };
