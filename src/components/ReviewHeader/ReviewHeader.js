import React from 'react';
import cn from 'classnames';

type Props = {
  classes: any,
  disabled: boolean,
  label: string,
}
const ReviewHeader = (props: Props) => {
  const { classes, disabled, label } = props;
  return (
    <div className={classes.root}>
      <div className={cn(classes.label, { [classes.disabled]: disabled })}>{label}</div>
    </div>
  );
};

export default ReviewHeader;
