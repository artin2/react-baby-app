import ShareHeader from './ShareHeader';
import enhance from './ShareHeader.enhancer';

export default enhance(ShareHeader);
