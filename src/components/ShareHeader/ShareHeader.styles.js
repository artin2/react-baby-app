const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '2rem',
  },
});

export { styles };
