import React from 'react';

type Props = {
  classes: any,
  label: String,
}
const ShareHeader = (props: Props) => {
  const { classes, label } = props;
  return (
    <h3 className={classes.root}>{label}</h3>
  );
};

export default ShareHeader;
