import React from 'react';
import { storiesOf } from '@storybook/react';
import ShareHeader from '../index';

const stories = storiesOf('ShareHeader', module);

stories.add('Normal', () => (
  <ShareHeader label="header" />
));
