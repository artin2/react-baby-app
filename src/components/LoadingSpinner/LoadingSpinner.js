import React from 'react';
import cn from 'classnames'
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
  classes: any,
  size: any,
  show: boolean,
}
const LoadingSpinner = ({ size, classes, show }: Props) => {
  const controlledShow = show ? cn(classes.root, classes.show) : classes.hide;
  const spinnerClass = (show === undefined) ? classes.root : controlledShow;
  console.log('loading spinner', show);
  return (
    <div className={spinnerClass}>
      <div className={classes.progress}>
        <CircularProgress mode="indeterminate" size={size || 80} />
      </div>
    </div>
  );
};

export default LoadingSpinner;

