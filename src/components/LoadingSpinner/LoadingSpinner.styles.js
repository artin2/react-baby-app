const styles = {
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: '7rem',
    height: '100%',
  },
  show: {
    display: 'flex',
  },
  hide: {
    display: 'none',
  },
  progress: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '50%',
  },
};

export { styles };
