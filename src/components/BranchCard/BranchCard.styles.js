const styles = theme => ({
  root: {
    margin: '15px 0',
  },
  cardContainer: {
    padding: '1.2rem',
  },
  cardHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  link: {
    color: theme.palette.primary.main,
    textTransform: 'uppercase',
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.2rem',
    marginBottom: '5px',
  },
  cardImage: {
    paddingBottom: '20px',
    '& >img': {
      maxHeight: '320px',
      width: '100%',
      borderRadius: '4px',
    },
  },
  branchMessage: {
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    fontSize: '1.4rem',
    lineHeight: '20px',
    textAlign: 'center',
  },
});

export { styles };
