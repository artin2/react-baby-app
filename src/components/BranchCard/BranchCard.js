import React from 'react';
import { Paper } from '@material-ui/core';
import BranchProfile from '../BranchProfile';

type Props = {
  coverImage: string,
  coverCaption: string,
  classes: any,
  name: string,
  dateAdded: string,
  profileImage: string,
}
const BranchCard = (props: Props) => {
  const {
    coverImage,
    coverCaption,
    classes,
    name,
    dateAdded,
    profileImage,
  } = props;

  return (
    <div className={classes.root}>
      <Paper className={classes.cardContainer}>
        <div className={classes.cardHeader}>
          <BranchProfile
            orientation="horizontal"
            name={name}
            date={dateAdded}
            img={profileImage}
          />
          <div className={classes.link}>open</div>
        </div>
        <div className={classes.cardImage}>
          <img src={coverImage} alt="" />
        </div>
        <div className={classes.branchMessage}>
          {coverCaption}
        </div>
      </Paper>
    </div>
  );
};

export default BranchCard;

