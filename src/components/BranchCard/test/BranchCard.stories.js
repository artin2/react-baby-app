import React from 'react';
import { storiesOf } from '@storybook/react';
import BranchCard from '../index';
import profile from '../../../assets/images/branchSamples/branch-profile.jpg';
import branchProfile from '../../../assets/images/branchSamples/branch-image-1.jpg';

const stories = storiesOf('BranchCard', module);

const requiredProps = () => (
  {
    date: 'Aug 9, 2019',
    profileImage: profile,
    name: 'Aminah',
    branchImage: branchProfile,
    branchMessage: 'I’m so happy for you! Here’s the best advice I wish someone gave me when we had Winnie.',
  }
);

stories.add('Normal', () => (
  <div style={{ width: '50%' }}>
    <BranchCard {...requiredProps()} />
  </div>
));
