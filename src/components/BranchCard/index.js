import BranchCard from './BranchCard';
import enhance from './BranchCard.enhancer';

export default enhance(BranchCard);
