import BookColorChoice from './BookColorChoice';
import enhance from './BookColorChoice.enhancer';

export default enhance(BookColorChoice);
