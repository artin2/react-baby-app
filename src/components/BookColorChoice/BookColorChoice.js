import React from 'react';
import cn from 'classnames';
import { BookCoverColor } from '../../config/prop-types';

type Props = {
  classes: any,
  color: BookCoverColor,
}
const BookColorChoice = (props: Props) => {
  const { classes, color } = props;
  return (
    <div className={cn(classes.root, classes[color])} />
  );
};

export default BookColorChoice;
