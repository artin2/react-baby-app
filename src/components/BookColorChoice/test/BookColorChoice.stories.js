import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, radios } from '@storybook/addon-knobs';
import BookColorChoice from '../index';

const stories = storiesOf('BookColorChoice', module);

stories.addDecorator(withKnobs);

const label = 'Colors';

const options = {
  Navy: 'navy',
  Yellow: 'yellow',
  Red: 'red',
  Gray: 'gray',
  Pink: 'pink',
  Seafoam: 'seafoam',
  Teal: 'teal',
};

const defaultValue = 'navy';

const color = radios(label, options, defaultValue);
stories.add('Normal', () => (
  <BookColorChoice
    color={color}
  />
));
