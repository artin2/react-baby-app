const squareDimension = 68;
const styles = theme => ({
  root: {
    height: `${squareDimension}px`,
    width: `${squareDimension}px`,
    margin: '8px 8px 0 0',
    [theme.breakpoints.down]: {
      height: `${squareDimension * 0.8}px`,
      width: `${squareDimension * 0.8}px`,
    },
  },
  navy: {
    background: theme.palette.bookCover.navy,
  },
  yellow: {
    background: theme.palette.bookCover.yellow,
  },
  red: {
    background: theme.palette.bookCover.red,
  },
  gray: {
    background: theme.palette.bookCover.gray,
  },
  pink: {
    background: theme.palette.bookCover.pink,
  },
  seafoam: {
    background: theme.palette.bookCover.seafoam,
  },
  teal: {
    background: theme.palette.bookCover.teal,
  },
});

export { styles };
