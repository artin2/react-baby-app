
const nestIdFontSize = 2;
const numBranchFontSize = 1.6;

const styles = theme => ({
  root: {
    color: '#FFFFFF',
    fontFamily: theme.typography.fontFamily.primary.main,
    width: '100%',
    paddingBottom: '5%',
    '&:not(:first-child)': {
      paddingTop: '5%',
      borderTop: `0.25px solid ${theme.palette.white}`,
    },
    // '&:not(:first-child)': {
    // },
  },
  img: {
    height: 'auto',
    width: '100px',
    borderRadius: '8px',
  },
  date: {
    fontSize: '1rem',
    padding: '5px 0',
  },
  nestId: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: `${nestIdFontSize * 0.8}rem`,
    padding: '5px 0',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${nestIdFontSize}rem`,
    },
  },
  numBranches: {
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    fontSize: `${numBranchFontSize * 0.8}rem`,
    padding: '5px 0',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${numBranchFontSize}rem`,
    },
  },
  btnGroup: {
    display: 'flex',
  },
});

export { styles };
