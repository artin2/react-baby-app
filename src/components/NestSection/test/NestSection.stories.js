import React from 'react';
import { storiesOf } from '@storybook/react';
import NestSection from '../index';
import profile1 from '../../../assets/images/nest-profile-img.jpg';
import { globalStyleVars as g } from '../../../assets/styles/global-variables';

const stories = storiesOf('NestSection', module);

stories.add('NestSection', () => (
  <div style={{ width: '100%', background: g.primaryColor, padding: '10px' }}>
    <NestSection
      image={profile1}
      date="Feb 2019"
      nestId="EzraJefferson2019"
      numBranches={0}
    />
  </div>
));
