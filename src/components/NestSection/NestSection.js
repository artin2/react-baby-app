import React from 'react';
import { Grid } from '@material-ui/core';
import Button from '../Button';

type Props = {
  classes: any,
  image: any,
  nestId: string,
  date: string,
  numBranches: number,
}

const NestSection = (props: Props) => {
  const {
    classes,
    image,
    nestId,
    date,
    numBranches,
  } = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={8}>
        <Grid item xs={4}>
          <img className={classes.img} src={image} alt="nest" />
        </Grid>
        <Grid item xs={8}>
          <div className={classes.date}>
            {`Created ${date}`}
          </div>
          <div className={classes.nestId}>{nestId}</div>
          <div className={classes.numBranches}>
            {`${numBranches} Branches`}
          </div>
          <div className={classes.btnGroup}>
            <Button label="Print Nest" variant="primaryOutline" sz="md" />
            <Button label="Save Nest" variant="primaryOutline" sz="md" />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default NestSection;

