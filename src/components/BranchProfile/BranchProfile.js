/* eslint-disable react/jsx-one-expression-per-line */
import React, { Fragment } from 'react';
import cn from 'classnames';

type Orientation = 'horizontal' | 'vertical';

type Props = {
  classes: any,
  date: string,
  img: any,
  name: string,
  orientation: Orientation,
}

const BranchProfile = (props: Props) => {
  const renderDetail = () => {
    const {
      classes,
      orientation,
      name,
      date,
    } = props;
    const details = orientation === 'vertical'
      ? (
        <Fragment>
          <div className={classes.name}>{name}</div>
          <div className={classes.date}>
            {`on ${date}`}
          </div>
        </Fragment>
      )
      : (
        <div className={classes.detailContainer}>
          <div className={classes.name}>{name}</div>
          <div className={classes.date}>
            {`on ${date}`}
          </div>
        </div>
      );
    return details;
  }

  const {
    classes,
    img,
    name,
    orientation,
  } = props;
  return (
    <div className={cn(classes.root, classes[orientation])}>
      <img className={classes.img} src={img} alt={name} />
      {renderDetail()}
    </div>
  );
};

export default BranchProfile;

