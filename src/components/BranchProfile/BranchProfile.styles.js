
const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  horizontal: {
    flexDirection: 'row',
    '& $img': {
      height: '40px',
    },
  },
  vertical: {
    flexDirection: 'column',
  },
  img: {
    height: '60px',
    marginBottom: '15px',
    borderRadius: '50%',
  },
  name: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.6rem',
  },
  date: {
    fontSize: '1.2rem',
    color: theme.palette.text.disabled,
  },
  detailContainer: {
    marginLeft: '10px',
    marginTop: '-12px',
  },
});

export { styles };
