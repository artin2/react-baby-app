import React from 'react';
import { storiesOf } from '@storybook/react';
import BranchProfile from '../index';
import profile from '../../../assets/images/branchSamples/branch-profile.jpg';

const stories = storiesOf('BranchProfile', module);

const requiredProps = () => (
  {
    date: 'Aug 9, 2019',
    img: profile,
    name: 'Aminah',
  }
);

stories.add('Vertical', () => (
  <BranchProfile
    {...requiredProps()}
    orientation="vertical"
  />
));

stories.add('Horizontal', () => (
  <BranchProfile
    {...requiredProps()}
    orientation="horizontal"
  />
));
