import BranchProfile from './BranchProfile';
import enhance from './BranchProfile.enhancer';

export default enhance(BranchProfile);
