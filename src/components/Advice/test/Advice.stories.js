import React from 'react';
import { storiesOf } from '@storybook/react';
import Advice from '../index';

const stories = storiesOf('Advice', module);

const tip = 'Avoid eye contact at all times. Kids are masters at sensing weakness.';

stories.add('Advice', () => (
  <Advice advice={tip} />
));
