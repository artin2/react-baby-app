import React from 'react';
import BranchHeader from '../BranchHeader';
import adviceImg from '../../assets/images/advice-placeholder.png';

type Props = {
  advice: string,
  classes: any,
}

const Advice = (props: Props) => {
  const { classes, advice } = props;
  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <BranchHeader
          variant="light"
          header="Advice"
        />
        <div className={classes.content}>
          <img className={classes.img} src={adviceImg} alt="Advice" />
          <div className={classes.adviceHeader}>Nestling Pro Tip</div>
          <div className={classes.advice}>{advice}</div>
        </div>
      </div>
    </div>
  );
};

export default Advice;

