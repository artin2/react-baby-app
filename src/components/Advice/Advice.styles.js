import { globalStyleVars as g } from '../../assets/styles/global-variables';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    backgroundColor: theme.palette.white,
    fontSize: '1.4rem',
  },
  container: {
    padding: '19px 28px',
    lineHeight: '20px',
  },
  adviceHeader: {
    fontFamily: theme.typography.fontFamily.primary.bold,
  },
  advice: {
    fontWeight: 600,
    textAlign: 'center',
  },
  content: {
    background: '#FFFFFF',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '28px',
    marginTop: '1rem',
  },
  img: {
    height: '160px',
    marginBottom: '1rem',
  },

});

export { styles };
