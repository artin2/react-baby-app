import Advice from './Advice';
import enhance from './Advice.enhancer';

export default enhance(Advice);
