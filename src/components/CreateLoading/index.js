import CreateLoading from './CreateLoading';
import enhance from './CreateLoading.enhancer';

export default enhance(CreateLoading);
