import React from 'react';
import cn from 'classnames';
import yellowLogo from '../../assets/images/yellow-logo.svg';
import pinkLogo from '../../assets/images/pink-logo.svg';
import blueLogo from '../../assets/images/blue-logo.svg';

type Props = {
  classes: any,
}

const CreateLoading = (props: Props) => {
  const { classes } = props;
  return (
    <div id="page-create-loading">
      <div className={classes.background} />
      <div className={classes.container}>
        <div>
          <img className={cn(classes.icon, classes.blue)} src={blueLogo} alt="Blue Logo" />
          <img className={cn(classes.icon, classes.pink)} src={pinkLogo} alt="Pink Logo" />
          <img className={cn(classes.icon, classes.yellow)} src={yellowLogo} alt="Yellow Logo" />
        </div>
        <div className={classes.message}>Just a Second … We’re Creating Your Nest</div>
      </div>
    </div>
  );
};

export default CreateLoading;

