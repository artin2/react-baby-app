import homeBgImg from '../../assets/images/create-loading-bg.png';

const styles = theme => ({
  background: {
    backgroundImage: `url(${homeBgImg})`,
    backgroundSize: 'cover',
    height: '100vh',
    width: '100vw',
    animation: '$backgroundImg 2s ease-out infinite normal',
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
  },
  icon: {
    width: '176.71px',
    position: 'absolute',
    top: '40%',
    left: '33%',
  },
  message: {
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: '1.6rem',
    color: '#848A99', // g.black,
    position: 'absolute',
    top: '60%',
    left: '10%',
  },
  blue: {
    opacity: 0,
    animation: '$blueIcon 1s ease-in-out infinite alternate',
  },
  yellow: {
    opacity: 0,
    animation: '$yellowIcon 1s ease-in-out infinite alternate',
  },
  pink: {
    opacity: 0,
    animation: '$pinkIcon 1s ease-in-out infinite alternate',
  },
  '@keyframes backgroundImg': {
    '0%': {
      transform: 'rotateZ(0deg)',
    },
    '25%': {
      transform: 'rotateZ(180deg)',
    },
    '50%': {
      transform: 'rotateX(180deg)',
    },
    '75%': {
      transform: 'rotateZ(180deg)',
    },
    '100%': {
      transform: 'rotateY(180deg)',
    },
  },
  '@keyframes blueIcon': {
    '0%': {
      opacity: 1,
    },
    '25%': {
      opacity: 0.75,
    },
    '50%': {
      opacity: 0.5,
    },
    '75%': {
      opacity: 0,
    },
    '100%': {
      opacity: 0,
    },
  },
  '@keyframes pinkIcon': {
    '0%': {
      opacity: 0,
    },
    // '25%': {
    //   opacity: 0.75,
    // },
    '50%': {
      opacity: 1,
    },
    '66%': {
      opacity: 0.75,
    },
    '75%': {
      opacity: 0.5,
    },
    '100%': {
      opacity: 0,
    },
  },
  '@keyframes yellowIcon': {
    '0%': {
      opacity: 0,
    },
    '25%': {
      opacity: 0.75,
    },
    '50%': {
      opacity: 0,
    },
    '66%': {
      opacity: 0.25,
    },
    '75%': {
      opacity: 0.5,
    },
    '100%': {
      opacity: 1,
    },
  },
});

export { styles };
