import React from 'react';

type Props = {
  classes: any,
  onChange: (any) => { },
  placeholder: string,
  rows: string,
}
const TextArea = (props: Props) => {
  const {
    classes,
    onChange,
    placeholder,
    rows,
  } = props;
  return (
    <textarea
      className={classes.root}
      onChange={onChange}
      placeholder={placeholder}
      rows={rows}
    />
  );
};

export default TextArea;

