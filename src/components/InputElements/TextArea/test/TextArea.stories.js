import React from 'react';
import { storiesOf } from '@storybook/react';
import TextArea from '../index';

const stories = storiesOf('TextArea', module);

stories.add('Normal', () => (
  <TextArea placeholder="Start Typing" />
));
