const styles = theme => ({
  root: {
    margin: '0 1.5rem 0 1rem',
    width: '100%',
    height: '10em',
    fontSize: '1.4rem',
    color: theme.palette.black,
    fontFamily: theme.typography.fontFamily.primary.main,
    fontWeight: 500,
    border: 'none',
  },
});

export { styles };
