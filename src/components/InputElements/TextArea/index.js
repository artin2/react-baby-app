import TextArea from './TextArea';
import enhance from './TextArea.enhancer';

export default enhance(TextArea);
