import React, { useState } from 'react';
import {
  InputLabel,
  FormControl,
  Select,
  MenuItem,
} from '@material-ui/core';
// import Chevron from '../Chevron';
// import BaseInput from './InputBase';
import { SelectVariant } from '../../../config/prop-types';

type Props = {
  classes: any,
  id: string,
  label: string,
  onChange: (any) => {},
  options: any,
  placeholder: String,
  variant: SelectVariant,
}

const NestSelect = (props: Props) => {
  const {
    classes,
    id,
    label,
    onChange,
    options,
    placeholder,
    variant,
  } = props;

  const [value, setValue] = useState('');

  const labelStyles = variant === 'light' ? 'lightLabel' : 'darkLabel';
  const underlineStyles = variant === 'light' ? 'lightUnderline' : 'darkUnderline';
  const inputStyles = variant === 'light' ? 'lightInput' : 'darkInput';

  const handleChange = (event) => {
    setValue(event.target.value);
    onChange(event);
  };

  return (
    <FormControl className={classes.root}>
      <InputLabel
        htmlFor={id}
        classes={{
          root: classes[labelStyles],
          focused: classes.cssFocused,
        }}
      >
        {label}
      </InputLabel>
      <Select
        classes={{
          select: classes[inputStyles],
          root: classes[underlineStyles],
          icon: classes[variant],
        }}
        fullWidth
        id={id}
        MenuProps={{
          className: classes.menu,
        }}
        inputProps={{ id, name: id }}
        onChange={handleChange}
        placeholder={placeholder}
        value={value}
      >
        {options.map(option => (
          <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
        ))}
      </Select>
      {/* <InputBase
        classes={{
          // root: classes[underlineStyles],
          input: classes[underlineStyles],
        }}
        inputComponent={BaseInput}
        inputProps={{
          options,
          variant,
        }}
      /> */}
    </FormControl>
  );
};

export default NestSelect;
