import Select from './Select';
import enhance from './Select.enhancer';

export default enhance(Select);
