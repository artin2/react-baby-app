import React from 'react';
import { storiesOf } from '@storybook/react';
import NestSelect from '../index';

const stories = storiesOf('Select', module);

const options = [
  {
    value: 'AK',
    label: 'AK',
  },
  {
    value: 'AL',
    label: 'AL',
  },
];
const requiredProps = () => ({
  id: 'select',
  label: 'State',
  options,
});

stories.add('Light', () => (
  <div style={{ width: '90%', padding: '20px' }}>
    <NestSelect {...requiredProps()} variant="light" />
  </div>

));

stories.add('Dark', () => (
  <div style={{ width: '90%', padding: '20px', background: '#091633' }}>
    <NestSelect {...requiredProps()} variant="dark" />
  </div>
));
