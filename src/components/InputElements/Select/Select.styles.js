const styles = theme => ({
  root: {
    width: '100%',
    // cursor: 'text',
    // margin: '35px 20px',
  },
  lightLabel: {
    fontSize: '1.3rem',
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    fontWeight: 600,
    pointerEvents: 'none',
    color: theme.palette.text.label,
    '&$cssFocused': {
      color: theme.palette.primary.main,
    },
  },
  darkLabel: {
    fontSize: '1.3rem',
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    fontWeight: 600,
    pointerEvents: 'none',
    color: theme.palette.white,
    '&$cssFocused': {
      color: theme.palette.primary.main,
    },
  },
  cssFocused: {
  },
  lightUnderline: {
    '&:before': {
      borderBottomColor: '#CCCCCC',
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
  },
  darkUnderline: {
    '&:before': {
      borderBottomColor: theme.palette.white,
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
  },
  lightInput: {
    fontSize: '1.4rem',
    fontFamily: theme.typography.fontFamily.primary.main,
    '&:focus': {
      backgroundColor: '#FFFFFF',
    },
  },
  darkInput: {
    fontSize: '1.4rem',
    fontFamily: theme.typography.fontFamily.primary.main,
    color: '#FFFFFF',
    '&:focus': {
      backgroundColor: theme.palette.baseBlue,
    },
  },
  // input: {
  //   fontSize: '1.4rem',
  //   fontFamily: theme.typography.fontFamily.primary.main,
  //   '&:focus': {
  //     backgroundColor: '#FFFFFF',
  //   },
  // },
  menu: {
    // background: '#FFFFFF',
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: '1.3rem',
  },
  dark: {
    color: '#CCCCCC',
  },
  // label: {
  //   fontSize: g.labelFontSize,
  //   fontFamily: g.secondaryFont,
  //   textAlign: 'left',
  //   fontWeight: 600,
  //   position: 'absolute',
  //   pointerEvents: 'none',
  //   marginTop: '-30px',
  //   transition: '0.2s ease all',
  // },
  // dark: {
  //   color: g.white,
  // },
  // light: {
  //   color: g.black,
  // },
  // bar: {
  //   position: 'relative',
  //   display: 'block',
  //   width: '100%',
  //   '&::before, &::after': {
  //     content: '""',
  //     height: '2px',
  //     width: 0,
  //     bottom: '1px',
  //     position: 'absolute',
  //     background: g.primaryColor,
  //     transition: '0.2s ease all',
  //   },
  //   '&::before': {
  //     left: '50%',
  //   },
  //   '&::after': {
  //     right: '50%',
  //   },
  // },
  // highlight: {
  //   position: 'absolute',
  //   height: '60%',
  //   width: '100px',
  //   top: '25%',
  //   left: 0,
  //   pointerEvents: 'none',
  //   opacity: 0.5,
  // },
  // select: {
  //   border: 'none',
  //   background: 'none',
  //   outline: 'none',
  //   maxWidth: '100%',
  //   borderRadius: 0,
  //   flex: '1 0 auto',
  //   lineHeight: '1.21em',
  //   padding: '6px 0 7px',
  //   display: 'block',
  //   minWidth: 0,
  //   width: '100%',
  //   boxSizing: 'content-box',
  //   borderBottom: '1px solid #CCCCCC',
  //   '&:focus': {
  //     outline: 'none',
  //     border: 'none',
  //   },
  //   '&:focus~$label, &:valid~$label': {
  //     marginTop: '-50px',
  //     // left: 15px;
  //     fontSize: '12px',
  //     color: g.primaryColor,
  //   },
  //   '&:focus~$bar:before, &:focus~$bar:after': {
  //     width: '50%',
  //   },
  //   '&:focus~$highlight': {
  //     animation: '$inputHighlighter 0.3s ease',
  //   },
  // },
});

export { styles };
