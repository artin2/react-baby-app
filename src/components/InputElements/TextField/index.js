import TextField from './TextField';
import enhance from './TextField.enhancer';

export default enhance(TextField);
