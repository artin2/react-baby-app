import React from 'react';
import { storiesOf } from '@storybook/react';
import TextField from '../index';

const stories = storiesOf('TextField', module);

stories.add('No Icon', () => (
  <TextField label="First Name" variant="light" />
));

stories.add('Icon Dark', () => (
  <div style={{ background: '#091633', height: '200px', paddingTop: '40px' }}>
    <TextField id="first-name" label="First Name" hasIcon variant="dark" />
  </div>

));
