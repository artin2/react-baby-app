import React from 'react';
import {
  FormControl,
  Input,
  InputLabel,
  InputAdornment,
} from '@material-ui/core';
import HelpIcon from '../../HelpIcon';
import { TextFieldVariant } from '../../../config/prop-types';

type Props = {
  classes: any,
  hasIcon: boolean,
  id: string,
  label: string,
  onChange: (any) => {},
  placeholder?: String,
  variant: TextFieldVariant,
}

const TextField = (props: Props) => {
  const {
    classes,
    hasIcon,
    id,
    label,
    onChange,
    placeholder,
    variant,
  } = props;
  const labelStyles = variant === 'light' ? 'lightLabel' : 'darkLabel';
  const underlineStyles = variant === 'light' ? 'lightUnderline' : 'darkUnderline';
  const inputStyles = variant === 'light' ? 'lightInput' : 'darkInput';
  return (
    <FormControl classes={{ root: classes.root }}>
      <InputLabel
        htmlFor={id}
        classes={{
          root: classes[labelStyles],
          focused: classes.cssFocused,
        }}
      >
        {label}
      </InputLabel>
      <Input
        fullWidth
        id={id}
        classes={{
          input: classes[inputStyles],
          underline: classes[underlineStyles],
        }}
        inputProps={{ name: id }}
        endAdornment={
          hasIcon
            ? <InputAdornment classes={{ root: classes.helpIcon }}><HelpIcon /></InputAdornment>
            : null
        }
        onChange={onChange}
        placeholder={placeholder}
      />
    </FormControl>
  );
};

TextField.defaultProps = {
  placeholder: null,
};

export default TextField;

