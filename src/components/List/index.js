import List from './List';
import enhance from './List.enhancer';

export default enhance(List);
