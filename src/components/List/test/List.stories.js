/* eslint-disable react/no-array-index-key */
import React from 'react';
import { storiesOf } from '@storybook/react';
import List from '../index';
import { SelectionListItem } from '../../SelectionListItem/SelectionListItem';
import { ProductListItem } from '../../ProductListItem/ProductListItem';
import productImg1 from '../../../assets/images/p1.jpg';
import productImg2 from '../../../assets/images/p2@2x.png';

const stories = storiesOf('List', module);

const labels = [
  'Um, how the heck do I get my socks on?',
  'How likely am I to poop during labor?',
  'What can I use to fight off stretch marks?',
];
stories.add('SelectionList', () => (
  <List>
    {labels.map((label, index) => (
      <SelectionListItem key={index} label={label} />
      // TODO: get an id for this instead of the index
    ))}
  </List>
));

const products = [
  {
    id: '001',
    description: 'Boba Baby Wrap Carrier, Grey $39.95 - Free Shipping',
    img: productImg1,
  },
  {
    id: '002',
    description: 'Totokan Portable Baby Monitor $249.00 - Free Shipping',
    img: productImg2,
  },
]
stories.add('Product List', () => (
  <List>
    {products.map((product, index) => (
      <ProductListItem
        key={index}
        description={product.description}
        img={product.img}
      /> // TODO: get an id for this instead of the index
    ))}
  </List>
));
