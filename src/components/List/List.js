import React from 'react';

type Props = {
  classes: any,
  children: any,
}

const List = (props: Props) => {
  const { classes, children } = props;
  return (
    <div className={classes.root}>
      {children}
    </div>
  );
};

export default List;
