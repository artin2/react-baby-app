import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CategoryTile from '../index';

const stories = storiesOf('CategoryTile', module);

stories.add('Normal', () => (
  <CategoryTile color="purple" categoryText="Clothing" onClick={action('category clicked')} />
));
