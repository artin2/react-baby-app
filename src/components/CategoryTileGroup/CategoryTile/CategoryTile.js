import React, { SyntheticEvent } from 'react';
import cn from 'classnames';

type Props = {
  classes: any,
  categoryText: String,
  color?: string,
  onClick: (SyntheticEvent) => void,
};
const CategoryTile = ({
  classes, categoryText, color, onClick,
}: Props) => {
  const handleOnClick = (event) => {
    onClick(event, categoryText);
  };

  return (
    <div
      className={cn(classes.root, classes[color])}
      onClick={handleOnClick}
      onKeyPress={handleOnClick}
      tabIndex={0}
      role="button"
    >
      <div className={classes.categoryText}>{categoryText}</div>
    </div>
  );
};

CategoryTile.defaultProps = {
  color: '#D74BF6',
};
export default CategoryTile;
