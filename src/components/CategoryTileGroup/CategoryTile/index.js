import CategoryTile from './CategoryTile';
import enhance from './CategoryTile.enhancer';

export default enhance(CategoryTile);
