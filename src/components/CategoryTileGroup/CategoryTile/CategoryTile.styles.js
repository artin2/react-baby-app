
const styles = theme => ({
  root: {
    borderRadius: '4px',
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    width: '100%',
    color: theme.palette.text.label,
    textAlign: 'center',
    '&:active': {
      background: 'white',
    },
  },
  categoryText: {
    padding: '20px 44px',
    fontSize: '1.4rem',
  },
  purple: {
    backgroundColor: '#D74BF6',
    '&:active': {
      background: '#FF7EFF',
    },
  },
  red: {
    backgroundColor: '#EF4E81',
    '&:active': {
      background: '#FF9BCE',
    },
  },
  orange: {
    backgroundColor: '#F76748',
    '&:active': {
      background: '#FFB495',
    },
  },
  yellow: {
    backgroundColor: '#F4C203',
    '&:active': {
      background: '#FFFF50',
    },
  },
  green: {
    backgroundColor: '#80BD36',
    '&:active': {
      background: '#CDFF83',
    },
  },
  blue: {
    backgroundColor: '#309EBD',
    '&:active': {
      background: '#7DEBFF',
    },
  },
});

export { styles };
