import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CategoryTileGroup from '../index';

const stories = storiesOf('CategoryTileGroup', module);

const categories = [
  {
    label: 'Equipment',
    color: 'purple',
  },
  {
    label: 'Clothing',
    color: 'red',
  },
  {
    label: 'Food',
    color: 'orange',
  },
  {
    label: 'Furniture',
    color: 'yellow',
  },
  {
    label: 'Learning',
    color: 'green',
  },
  {
    label: 'Safety',
    color: 'blue',
  },
];
stories.add('Normal', () => (
  <CategoryTileGroup categories={categories} onClick={action('button click')} />
));
