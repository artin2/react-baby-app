import CategoryTileGroup from './CategoryTileGroup';
import enhance from './CategoryTileGroup.enhancer';

export default enhance(CategoryTileGroup);
