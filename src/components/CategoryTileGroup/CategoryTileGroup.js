import React, { SyntheticEvent } from 'react';
import { Grid } from '@material-ui/core';
import CategoryTile from './CategoryTile';

type Props = {
  classes: any,
  categories: any[],
  onClick: (SyntheticEvent) => void,
};

const CategoryTileGroup = ({ classes, categories, onClick }: Props) => {
  const renderGrid = () => {
    const gridArray = [];
    while (categories.length) gridArray.push(categories.splice(0, 2));
    return gridArray;
  };

  return (
    <div className={classes.root}>
      <Grid container>
        {renderGrid().map((row) => {
          return (
            <Grid container spacing={16} className={classes.categoryRow}>
              <Grid item xs={6}>
                <CategoryTile color={row[0].color} categoryText={row[0].label} onClick={onClick} />
              </Grid>
              <Grid item xs={6}>
                <CategoryTile color={row[1].color} categoryText={row[1].label} onClick={onClick} />
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};

export default CategoryTileGroup;
