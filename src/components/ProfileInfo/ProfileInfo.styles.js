const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    fontSize: '2rem',
    fontWeight: 600,
    paddingTop: '10px',
  },
  location: {
    fontSize: '13px',
    fontWeight: 500,
    padding: '3px',
  },
});

export { styles };
