import React from 'react';
import RoundImage from '../RoundImage';

type Props = {
  classes: any,
  image: any,
  name: string,
  location: string,
}

const ProfileInfo = (props: Props) => {
  const {
    classes,
    image,
    name,
    location,
  } = props;
  return (
    <div className={classes.root}>
      <RoundImage image={image} />
      <div className={classes.name}>{name}</div>
      <div className={classes.location}>{location}</div>
    </div>
  );
};

export default ProfileInfo;

