import React from 'react';
import { storiesOf } from '@storybook/react';
import ProfileInfo from '../index';
import profilePic from '../../../assets/images/profile-pic2.jpg';

const stories = storiesOf('ProfileInfo', module);

const requiredProps = () => ({
  name: 'Sarah Garnett',
  location: 'Madison, WI',
});

stories.add('ProfileInfo', () => (
  <ProfileInfo image={profilePic} {...requiredProps()} />
));
