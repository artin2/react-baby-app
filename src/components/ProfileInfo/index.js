import ProfileInfo from './ProfileInfo';
import enhance from './ProfileInfo.enhancer';

export default enhance(ProfileInfo);
