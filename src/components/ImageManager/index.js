import ImageManager from './ImageManager';
import enhance from './ImageManager.enhancer';

export default enhance(ImageManager);
