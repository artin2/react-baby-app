import React from 'react';
import { Grid } from '@material-ui/core';
// import ProgressiveImage from 'react-progressive-image';
import sampleImg from '../../assets/images/sample-upload-image-lg.png';
// import tinySampleImg from '../../assets/images/small/sample-upload-image-lg_1_10.png';

type Props = {
  classes: any,
  img: string,
}

const ImageManager = (props: Props) => {
  const { classes, img } = props;
  const nestImg = img || sampleImg;
  return (
    <Grid container className={classes.root}>
      <Grid item xs={4}>
        <img src={nestImg} alt="Nest" className={classes.img} />
        {/* use this when you can actually resize image */}
        {/* <ProgressiveImage src={nestImg} placeholder={tinySampleImg} delay={1000}>
          {src => <img src={src} alt="Nest" className={classes.img} />}
        </ProgressiveImage> */}
      </Grid>
      <Grid item xs={8}>
        <div className={classes.optionsContainer}>
          <p className={classes.options}>Replace</p>
          <p className={classes.options}>Delete</p>
        </div>
      </Grid>
    </Grid>
  );
};

export default ImageManager;

