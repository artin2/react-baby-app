const styles = theme => ({
  root: {
    background: theme.palette.background.gray,
    marginTop: '1rem',
    fontFamily: theme.typography.fontFamily.primary.main,
    padding: '2rem 0',
  },
  img: {
    margin: '1rem',
    height: '13rem',
    objectFit: 'contain',
    borderRadius: '4px',
  },
  optionsContainer: {
    display: 'flex',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    paddingBottom: '1rem',
  },
  options: {
    color: theme.palette.primary.main,
    fontSize: '1rem',
    fontWeight: 700,
    padding: '4px',
    textTransform: 'uppercase',
  },
});

export { styles };
