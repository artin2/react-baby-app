import BranchHeader from './BranchHeader';
import enhance from './BranchHeader.enhancer';

export default enhance(BranchHeader);
