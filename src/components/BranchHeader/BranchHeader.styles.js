const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.demiBold,
    fontSize: '1.4rem',
  },
  light: {
    color: theme.palette.text.branchHeader,
  },
  dark: {
    color: theme.palette.white,
  },
});

export { styles };
