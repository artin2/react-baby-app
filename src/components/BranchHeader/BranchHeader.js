import React from 'react';
import cn from 'classnames';

type Variant = 'light' | 'dark';
type Props = {
  classes: any,
  header: string,
  variant: Variant,
}

const BranchHeader = (props: Props) => {
  const { classes, header, variant } = props;
  return (
    <div className={cn(classes.root, classes[variant])}>{header}</div>
  );
};
export default BranchHeader;
