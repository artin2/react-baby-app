import React from 'react';
import { storiesOf } from '@storybook/react';
import BranchHeader from '../index';

const stories = storiesOf('BranchHeader', module);

stories.add('Light', () => (
  <BranchHeader header="Answers" variant="light" />
));

stories.add('Dark', () => (
  <BranchHeader header="Recommendations" variant="dark" />
));
