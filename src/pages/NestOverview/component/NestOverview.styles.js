import merge from 'lodash/merge';
import { section, container } from '../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  icon: {
    color: theme.palette.primary.main,
  },
  header: {
    marginBottom: '0 !important',
  },
  section: merge({}, section, {
    backgroundColor: theme.palette.white,
  }),
  container,
  title: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.6rem',
  },
  subtitle: {
    fontFamily: theme.typography.fontFamily.primary.medium,
    fontSize: '1.2rem',
  },
  branchContainer: {
    marginTop: '20px',
  },
});

export { styles };
