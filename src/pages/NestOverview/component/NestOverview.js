import React, { useEffect } from 'react';
import { AddBoxOutlined } from '@material-ui/icons';
import LeftLogoHeader from '../../../components/Headers/LeftLogoHeader';
import BranchCard from '../../../components/BranchCard';

type Props = {
  branch: any,
  classes: any,
  getBranchesForNest: any,
  nest: any,
  user: any,
};

const NestOverview = (props: Props) => {
  const {
    branch: { nestId },
    classes,
    getBranchesForNest,
    nest,
    user,
  } = props;

  useEffect(() => {
    const get = async () => {
      const { uid } = user;
      const res = await getBranchesForNest(uid, nestId);
      console.log(res);
    };
    get();
    // NOTE: sort of hack for clean-up function
    return () => true;
  }, []);

  console.log(nest);
  return (
    <div className={classes.root}>
      <LeftLogoHeader
        classes={{ root: classes.header }}
        variant="light"
        button={<AddBoxOutlined className={classes.icon} />}
      />
      <div className={classes.section}>
        <div className={classes.container}>
          <div className={classes.title}>{`Welcome ${user.name}'s Nest`}</div>
          <div className={classes.subtitle}>
            Browse the Branches her friends and family have added
          </div>
          <div className={classes.branchContainer}>
            {nest.branches && nest.branches.map(branch => (
              <BranchCard
                key={branch.id}
                {...branch.branch}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NestOverview;
