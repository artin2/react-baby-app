import { Loadable } from '../../util/components';
import { NEST_OVERVIEW_PATH as path } from '../../constants/paths';

const NestOverviewRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { NestOverviewRoute };
