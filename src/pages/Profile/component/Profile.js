import React, { useState, useEffect } from 'react';
import AddBoxOutlined from '@material-ui/icons/AddBoxOutlined';
import LeftLogoHeader from '../../../components/Headers/LeftLogoHeader';
import ProfileInfo from '../../../components/ProfileInfo';
import Tabs from '../../../components/Tabs';
import Tab from '../../../components/Tabs/Tab/index';
import NestSection from '../../../components/NestSection';
import Invite from '../../../components/Invite';

type Props = {
  classes: any,
  getUser: any,
  queryParams: any,
  user: any,
};

const Profile = (props: Props) => {
  const [value, setValue] = useState(0);

  const {
    classes, getUser, queryParams, user,
  } = props;
  const { nests } = user;

  useEffect(() => {
    console.log('using effect', queryParams);
    const { uid } = queryParams;
    const get = async () => {
      await getUser(uid);
    };
    get();

    return () => true;
  }, []);

  const handleTabClick = (e, val) => {
    e.preventDefault();
    setValue(val);
  };
  console.log(nests);

  return (
    <div className={classes.root}>
      <LeftLogoHeader variant="light" button={<AddBoxOutlined className={classes.icon} />} />
      <div className={classes.profileContent}>
        <ProfileInfo image={user.profileImage} name="Sara" location="Madison, WI" />
      </div>
      <Tabs value={0} onClick={handleTabClick}>
        <Tab label="Nests" />
        <Tab label="Branches" />
      </Tabs>
      {value === 0 && (
        <div className={classes.content}>
          <div className={classes.section}>
            {nests && nests.length > 0
              ? nests.map(nest => (
                <NestSection
                  key={nest.id}
                  id={nest.id}
                  nestId={nest.id}
                  image={nest.nest.imageUrl}
                  // numBranches={nest.branches.length}
                />
              ))
              : <div>No Nests created</div>
            }
            <Invite header="Get Your Friends in the Mix" />
          </div>
        </div>
      )}
    </div>
  );
};

export default Profile;

