import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './Profile.styles';
import { PageHelper, mapDispatchToProps, mapStateToProps } from '../../../containers/PageHelper/PageHelper';

export default compose(
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
