import merge from 'lodash/merge';
import { section, container } from '../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    display: 'flex',
    flexDirection: 'column',
    '& >div:last-child': {
      flex: 1,
    },
    minHeight: '100vh',
    height: '100%',
  },
  section: merge({}, section, {
    height: '100%',
  }),
  container: merge({}, container, {
    height: '100%',
  }),
  profileContent: {
    display: 'flex',
    justifyContent: 'center',
    paddingBottom: '2rem',
  },
  icon: {
    color: theme.palette.primary.main,
  },
  content: {
    background: theme.palette.primary.main,
    height: '100%',
  },
});

export { styles };
