import { Loadable } from '../../util/components';
import { PROFILE_PATH as path } from '../../constants/paths';

const ProfileRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Profile' */ './component'),
  }),
};

export { ProfileRoute };
