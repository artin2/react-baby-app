import { Loadable } from '../../../util/components';
import { FIND_FRIEND_PATH as path } from '../../../constants/paths';

const FindFriendRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { FindFriendRoute };
