import FindFriend from './FindFriend';
import enhance from './FindFriend.enhancer';

export default enhance(FindFriend);
