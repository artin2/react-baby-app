import React from 'react';
import cn from 'classnames';
import LeftLogoHeader from '../../../../components/Headers/LeftLogoHeader';
import TextField from '../../../../components/InputElements/TextField';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import findFriendImg from '../../../../assets/images/find-a-friend@2x.png';

type Props = {
  classes: any,
}
const FindFriend = (props: Props) => {
  const { classes } = props;

  return (
    <section id="page-save-nest" className={classes.root}>
      <LeftLogoHeader />
      <div className={classes.section}>
        <div className={cn(classes.container, classes.content)}>
          <img className={classes.img} src={findFriendImg} alt="Save Nest" />
          <div className={classes.header}>Now, Save Your Nest</div>
          <form className={classes.form}>
            <TextField
              id="nest-id"
              label="Nest ID"
              variant="dark"
              hasIcon
            />
            <TextField
              id="city"
              label="City"
              variant="dark"
            />
            <TextField
              id="state"
              label="State"
              variant="dark"
            />
          </form>
        </div>
        <NavigationFooter nextLabel="Save" backIcon />
      </div>
    </section>
  );
};
export default FindFriend;

