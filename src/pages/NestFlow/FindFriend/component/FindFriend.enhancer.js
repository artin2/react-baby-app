import { compose } from 'redux';
import { connect } from 'react-redux';
// import { memo } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './FindFriend.styles';
// import { withFirebase } from '../../../../firebase/withFirebase';
import { mapDispatchToProps, mapStateToProps } from '../../../../containers/BranchHelper/BranchHelper'; // TODO: create nest helper
import { PageHelper } from '../../../../containers/PageHelper/PageHelper';

export default compose(
  // memo,  TODO: add when switch class to function
  // withFirebase,
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
