import { section, container } from '../../../../assets/styles/global.styles';
// TODO: this is the same as Save nest find way to reuse
const styles = theme => ({
  root: {
    background: theme.palette.base.main,
    height: '100%',
    minHeight: '100vh',
  },
  section,
  container,
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '40px', // TODO: move to section
  },
  img: {
    minHeight: '180px',
    maxHeight: '300px',
    height: '16em',
    objectFit: 'contain',
  },
  header: {
    color: theme.palette.white,
    fontFamily: theme.typography.fontFamily.primary.main,
    fontWeight: 600,
    textAlign: 'center',
    padding: '30px 0 0 ',
    fontSize: '2rem',
  },
  form: {
    width: '100%',
  },
});

export { styles };

