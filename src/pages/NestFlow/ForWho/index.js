import { Loadable } from '../../../util/components';
import { FOR_WHO_PATH as path } from '../../../constants/paths';

const ForWhoRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { ForWhoRoute };
