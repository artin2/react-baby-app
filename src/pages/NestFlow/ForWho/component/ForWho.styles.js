import { section, container } from '../../../../assets/styles/global.styles';

const imgHeight = 25;
const styles = theme => ({
  root: {
    background: theme.palette.base.main,
    height: '100%',
    minHeight: '100vh',
  },
  section,
  container,
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  header: {
    textAlign: 'center',
    paddingBottom: '2.5rem',
    color: theme.palette.white,
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: '2rem',
    fontWeight: 600,
    [theme.breakpoints.up('sm')]: {
      paddingBottom: '4rem',
    },
  },
  imgBtn: {
    height: `${imgHeight * 0.75}rem`,
    objectFit: 'contain',
    padding: '1.5rem 0',
    [theme.breakpoints.up('md')]: {
      height: `${imgHeight}rem`,
    },
  },
});

export { styles };
