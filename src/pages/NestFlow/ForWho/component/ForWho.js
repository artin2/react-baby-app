/* eslint-disable jsx-a11y/no-noninteractive-element-to-interactive-role */
import React, { useState } from 'react';
import firebase from 'firebase'; // TODO: import from props
import LeftLogoHeader from '../../../../components/Headers/LeftLogoHeader';
import SignIn from '../../../../components/SignIn';
import forWhoImg from '../../../../assets/images/for-you@2x.png';
import someoneElseImg from '../../../../assets/images/someone-else@2x.png';
import { SAVE_NEST_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  navigate: any,
  pageCache: any,
  signOut: any,
  updateUser: any,
  updatePageCache: any,
}

const ForWho = (props: Props) => {
  const [showLogin, setShowLogin] = useState(false);
  const {
    classes,
    pageCache,
    signOut,
    navigate,
    updateUser,
    updatePageCache,
  } = props;

  const { auth } = pageCache;
  // Configure FirebaseUI.
  const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // Redirect to /signedIn after sign in is successful.
    // Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: SAVE_NEST_PATH,
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      signInSuccessWithAuthResult: (authResult) => {
        const { user: { uid } } = authResult;
        // TODO: move db creation to save page here just push to store
        console.log('authResult', authResult.user.uid);
        updateUser('uid', uid);
        updatePageCache('auth', authResult);
        // createNewUser(uid);
        return true;
      },
    },
  };

  const handleClick = () => {
    if (auth) {
      navigate(SAVE_NEST_PATH);
      // TODO: need to set an indicator in the store for who branch is for
    } else {
      setShowLogin(true);
    }
  };
  return (
    <section id="page-for-who" className={classes.root}>
      <LeftLogoHeader
        isSignedIn={auth}
        onSignOut={signOut}
      />
      <div className={classes.section}>
        <div className={classes.container}>
          <div className={classes.content}>
            <div className={classes.header}>Who’s Your Nestling Book For?</div>
            <img
              id="for-you-btn"
              role="button"
              className={classes.imgBtn}
              onClick={handleClick}
              onKeyPress={handleClick}
              src={forWhoImg}
              alt="For Me"
            />
            <img
              alt="For Friend"
              className={classes.imgBtn}
              id="someone-else-btn"
              role="button"
              src={someoneElseImg}
            />
          </div>
        </div>
      </div>
      {showLogin && <SignIn uiConfig={uiConfig} firebase={firebase} />}
    </section>
  );
};

export default ForWho;
