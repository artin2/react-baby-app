import ForWho from './ForWho';
import enhance from './ForWho.enhancer';

export default enhance(ForWho);
