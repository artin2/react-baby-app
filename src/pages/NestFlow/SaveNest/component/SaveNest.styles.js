// import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    background: theme.palette.base.main,
    height: '100%',
    minHeight: '100vh',
  },
  section,
  container,
  // section: merge({}, section, {
  //   padding: '2.5rem 2rem',
  // }),
  // container: {
  //   margin: '0 auto',
  //   position: 'relative',
  // },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '8rem',
  },
  img: {
    minHeight: '180px',
    maxHeight: '300px',
    height: '30rem',
    objectFit: 'contain',
  },
  header: {
    color: theme.palette.white,
    fontFamily: theme.typography.fontFamily.primary.main,
    fontWeight: 600,
    textAlign: 'center',
    padding: '3rem 0 0 ',
    fontSize: '2rem',
  },
  form: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
});

export { styles };
