import React from 'react';
import cn from 'classnames';
import LeftLogoHeader from '../../../../components/Headers/LeftLogoHeader';
import saveNestImg from '../../../../assets/images/save-your-nest@2x.png';
import TextField from '../../../../components/InputElements/TextField';
import Select from '../../../../components/InputElements/Select';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { CREATE_ANNOUNCEMENT_PATH, FOR_WHO_PATH } from '../../../../constants/paths';
import { states } from '../../../../constants/dropdowns';

type Props = {
  classes: any,
  createNewDbUser: any,
  navigate: any,
  updateUser: any,
  updateNest: any,
  pageCache: any,
  signOut: any,
  user: any,
}

const SaveNest = (props: Props) => {
  const {
    classes,
    createNewDbUser,
    navigate,
    updateUser,
    updateNest,
    pageCache: { auth },
    signOut,
    user,
  } = props;

  const handleNextClick = async () => {
    try {
      // TODO: probably want to do a verification on the nestId here
      await createNewDbUser(user);
      navigate(CREATE_ANNOUNCEMENT_PATH);
    } catch (e) {
      console.log(e);
    }
  };

  const handleBackClick = () => navigate(FOR_WHO_PATH);

  const handleValueChange = (event) => {
    const { target: { id, value } } = event;
    console.log(id);
    if (id === 'nestId') {
      updateNest(id, value);
    } else if (id === undefined) {
      updateUser('state', value);
    } else {
      updateUser(id, value);
    }
  };

  return (
    <section id="page-save-nest" className={classes.root}>
      <LeftLogoHeader
        isSignedIn={auth}
        onSignOut={signOut}
      />
      <div className={classes.section}>
        <div className={cn(classes.container, classes.content)}>
          <img className={classes.img} src={saveNestImg} alt="Save Nest" />
          <div className={classes.header}>Now, Save Your Nest</div>
          {/* create a form component? */}
          <form className={classes.form}>
            <TextField
              id="nestId"
              label="Nest ID"
              onChange={handleValueChange}
              variant="dark"
              hasIcon
            />
            <TextField
              id="city"
              label="City"
              onChange={handleValueChange}
              variant="dark"
            />
            <Select
              id="state"
              label="State"
              options={states}
              onChange={handleValueChange}
              variant="dark"
            />
            {/* <TextField
              id="state"
              label="State"
              onChange={handleValueChange}
              variant="dark"
            /> */}
          </form>
        </div>
        <NavigationFooter
          nextLabel="Save"
          backIcon
          onNextClick={handleNextClick}
          onBackClick={handleBackClick}
        />
      </div>
    </section>
  );
};

export default SaveNest;
