import { Loadable } from '../../../util/components';
import { SAVE_NEST_PATH as path } from '../../../constants/paths';

const SaveNestRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { SaveNestRoute };
