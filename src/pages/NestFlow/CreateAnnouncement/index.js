import { Loadable } from '../../../util/components';
import { CREATE_ANNOUNCEMENT_PATH as path } from '../../../constants/paths';

const CreateAnnouncementRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'CreateAnnouncement' */ './component'),
  }),
};

export { CreateAnnouncementRoute };

