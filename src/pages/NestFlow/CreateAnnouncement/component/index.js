import CreateAnnouncement from './CreateAnnouncement';
import enhance from './CreateAnnouncement.enhancer';

export default enhance(CreateAnnouncement);
