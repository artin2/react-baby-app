import { section, container } from '../../../../assets/styles/global.styles';

const styles = {
  root: {
  },
  section,
  container,
};

export { styles };
