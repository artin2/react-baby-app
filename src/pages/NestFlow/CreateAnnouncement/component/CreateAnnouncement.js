import React, { useState, Fragment } from 'react';
// import cn from 'classnames';
import UploadImageCard from '../../../../components/UploadImageCard';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { CONFIRM_ANNOUNCEMENT_PATH } from '../../../../constants/paths';
import LoadingSpinner from '../../../../components/LoadingSpinner';
import { setUpFileForUpload } from '../../../../util/util';
import { USER_STORAGE_PATH } from '../../../../constants/firebase';
import AnnouncementInfo from '../../../../components/AnnouncementInfo';
import TitleHeader from '../../../../components/Headers/TitleHeader';

type Props = {
  classes: any,
  navigate: any,
  pageCache: any,
  uploadPhoto: any,
  updateNest: any,
};

const CreateAnnoucement = (props: Props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [uploadedImage, setUploadedImage] = useState();

  const {
    classes,
    navigate,
    pageCache: { auth },
    uploadPhoto,
    updateNest,
  } = props;

  const handleUpload = async (file) => {
    setIsLoading(true);
    const fileData = setUpFileForUpload(file);
    const folderConfig = {
      path: USER_STORAGE_PATH,
      child: auth.uid,
      update: 'nest',
      updatePath: 'imageUrl',
    };
    try {
      const url = await uploadPhoto(fileData, folderConfig);
      setUploadedImage(url);
      setIsLoading(false);
      // navigate(ADD_MESSAGE_PATH);
    } catch (err) {
      console.log(err);
    }
  };
  const onChange = (event: any, field: string) => {
    const { target: { value } } = event;
    updateNest(field, value);
  };

  const handleOnNext = () => navigate(CONFIRM_ANNOUNCEMENT_PATH);
  const handleOnBack = () => console.log('go back');
  // TODO: add announcement info to store
  return (
    <Fragment>
      {!isLoading && (
        <section id="page-upload-image">
          <div className={classes.root}>
            <TitleHeader variant="light" titleText="Create Your Announcement" />
            <UploadImageCard
              onUpload={handleUpload}
              uploadTypeText="Announcement"
              image={uploadedImage}
            />
            <div className={classes.section}>
              <div className={classes.container}>
                <AnnouncementInfo onChange={onChange} />
              </div>
            </div>
          </div>
          <NavigationFooter
            nextLabel="Next"
            onNextClick={handleOnNext}
            onBackClick={handleOnBack}
          />
        </section>
      )}
      <LoadingSpinner show={isLoading} />
    </Fragment>

  );
};

export default CreateAnnoucement;

