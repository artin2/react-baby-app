import { Loadable } from '../../../util/components';
import { CONFIRM_ANNOUNCEMENT_PATH as path } from '../../../constants/paths';

const ConfirmAnnouncementRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { ConfirmAnnouncementRoute };
