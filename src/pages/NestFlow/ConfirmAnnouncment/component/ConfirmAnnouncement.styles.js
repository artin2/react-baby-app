import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';


const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  section: merge({}, section, {
    marginBottom: '2rem',
  }),
  container,
  announcementImageContainer: (props) => {
    console.log(props.nest.imageUrl);
    return {
      background: props.nest.imageUrl,
      width: '100%',
      height: '400px',
    };
  },
});

export { styles };
