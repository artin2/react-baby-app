import ConfirmAnnouncement from './ConfirmAnnouncement';
import enhance from './ConfirmAnnouncement.enhancer';

export default enhance(ConfirmAnnouncement);
