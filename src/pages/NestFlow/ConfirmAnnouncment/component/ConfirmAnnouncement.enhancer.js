import { compose } from 'redux';
import { connect } from 'react-redux';
import { memo } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './ConfirmAnnouncement.styles';
// import { withFirebase } from '../../../../firebase/withFirebase';
import { PageHelper, mapDispatchToProps, mapStateToProps } from '../../../../containers/PageHelper/PageHelper';

export default compose(
  memo,
  // withFirebase,
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
