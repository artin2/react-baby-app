import React from 'react';
import TitleHeader from '../../../../components/Headers/TitleHeader';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import AnnouncementPreview from '../../../../components/AnnouncementPreview';
import { CREATE_ANNOUNCEMENT_PATH, PROFILE_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  // createNest: any,
  navigate: any,
  nest: any,
  pageCache: any,
  share: any,
}

const ConfirmAnnouncement = (props: Props) => {
  const {
    classes,
    // createNest,
    navigate,
    nest,
    pageCache,
    share,
  } = props;

  const {
    nestId,
    announcementMessage,
    dueDateMonth,
    dueDateYear,
    gender,
    imageUrl,
  } = nest;
  console.log(imageUrl);
  const dueDate = `${dueDateMonth} ${dueDateYear}`;
  const shareConfig = uid => ({
    title: 'Nestling',
    text: `Sharing my nest! here is my Nest ID: ${nestId}`,
    url: `https://qursive-7030a.firebaseapp.com/welcome?uid=${uid}`, // TODO: don't use UID try to find something else
  });

  const handleConfirm = async () => {
    const { auth: { uid } } = pageCache;
    // await createNest(nest);
    if (navigator.share) {
      try {
        const response = await share(shareConfig(uid));
        console.log('SHARED', response);
        navigate(`${PROFILE_PATH}?uid=${uid}`);
      } catch (err) {
        console.log(err);
      }
    } else {
      console.log('no share available');
      navigate(`${PROFILE_PATH}?uid=${uid}`);
    }
  };

  const handleBack = () => navigate(CREATE_ANNOUNCEMENT_PATH);

  return (
    <section id="page-share">
      <div className={classes.root}>
        <TitleHeader variant="light" titleText="Confirm Your Announcement" />
        <div className={classes.announcementImageContainer} />
        {/* <img className={classes.announcementImage} alt="announcement" src={imageUrl} />
        </div> */}
        <div className={classes.section}>
          <div className={classes.container}>
            <AnnouncementPreview
              announcementMessage={announcementMessage}
              dueDate={dueDate}
              gender={gender}
            />
          </div>
        </div>
      </div>
      <NavigationFooter
        nextLabel="confirm"
        onNextClick={handleConfirm}
        onBackClick={handleBack}
      />
    </section>
  );
};

export default ConfirmAnnouncement;

