// import merge from 'lodash/merge';
import { section, container } from '../../../assets/styles/global.styles';
import { overlay } from '../../../assets/styles/mixins';
import homeBgImg from '../../../assets/images/home-background-img.png';

const contentFontSize = 1.6;

const titleFontSize = 3.2;
const styles = theme => ({
  root: {
    background: {
      image: `url(${homeBgImg})`,
    },
    backgroundSize: 'cover',
    height: '50vh',
    width: '100vw',
    fontFamily: theme.typography.fontFamily.primary.main,
    // [theme.breakpoints.up('sm')]: {
    //   height: '42vh',
    // },
  },
  overlay: overlay(0, 50),
  section,
  container,
  title: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    color: theme.palette.text.white,
    fontSize: `${titleFontSize * 0.8}rem`,
    textAlign: 'center',
    lineHeight: '40px',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${titleFontSize}rem`,
    },
  },
  buttonGroup: {
    width: '90%',
    margin: 'auto',
    marginTop: '2.4rem',
    [theme.breakpoints.up('sm')]: {
      marginTop: '3rem',
    },
  },
  navButton: {
    margin: '0 5px',
  },
  imageContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '-20px', // TODO: fix to align
    [theme.breakpoints.up('sm')]: {
      marginTop: '-40px',
    },
  },
  imageSlider: {
    zIndex: 999,
  },
  content: {
    textAlign: 'center',
    fontFamily: theme.typography.fontFamily.primary.medium,
    fontSize: `${contentFontSize * 0.8}rem`,
    lineHeight: '24px',
    marginTop: '20px',
    width: '80%',
    margin: 'auto',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${contentFontSize}rem`,
    },
  },
  configContainer: {
    padding: '30px 0',
  },
  configHeader: {
    color: theme.palette.text.disabled,
    fontSize: '1rem',
    textTransform: 'uppercase',
    paddingBottom: '5px',
    fontFamily: theme.typography.fontFamily.primary.bold,
  },
  buttonContainer: {
    margin: ' 30px auto',
    width: '50%',
  },
  coverOptionsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  sizeButton: {
    marginRight: '10px',
  },
});

export { styles };
