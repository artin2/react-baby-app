import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './Home.styles';
import { PageHelper, mapDispatchToProps, mapStateToProps } from '../../../containers/PageHelper/PageHelper';
// import { UserIsAuthenticated } from '../../../../util/router';

export default compose(
  // UserIsAuthenticated,
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
