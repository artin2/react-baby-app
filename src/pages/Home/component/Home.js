/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import LeftLogoHeader from '../../../components/Headers/LeftLogoHeader';
import ButtonGroup from '../../../components/ButtonGroup';
import Button from '../../../components/Button';
import { FOR_WHO_PATH } from '../../../constants/paths';
import imageSliderPlaceholder from '../../../assets/images/image-slider-placeholder.png';
import { bookCovers } from '../../../constants/book';
import BookColorChoice from '../../../components/BookColorChoice';
import ImageSlider from '../../../components/ImageSlider';
import Footer from '../../../components/Footers/Footer';
import AboutUs from '../../../components/AboutUs';

type Props = {
  classes: any,
  navigate: any,
}

const Home = (props: Props) => {
  const {
    classes,
    navigate,
  } = props;

  const handleOnClick = () => {
    navigate(FOR_WHO_PATH);
  };

  const images = [
    {
      original: imageSliderPlaceholder,
    },
    {
      original: imageSliderPlaceholder,
    },
    {
      original: imageSliderPlaceholder,
    },
  ];

  return (
    <section id="page-home">
      <div className={classes.root}>
        <div className={classes.overlay} />
        <LeftLogoHeader variant="dark" />
        <div className={classes.section}>
          <div className={classes.container} style={{ zIndex: 99 }}>
            <div className={classes.title}>Create a custom baby registry book.</div>
            <ButtonGroup
              classes={{
                root: classes.buttonGroup,
              }}
            >
              <Button
                classes={{
                  root: classes.navButton,
                }}
                variant="secondary"
                label="find a friend"
              />
              <Button
                classes={{
                  root: classes.navButton,
                }}
                variant="primary"
                label="start a book"
                onClick={handleOnClick}
              />
            </ButtonGroup>
          </div>
        </div>
      </div>
      {/* TODO: need to implement slider */}
      <div className={classes.section}>
        <div className={classes.container}>
          <div className={classes.imageContainer}>
            <ImageSlider items={images} />
          </div>
          <div className={classes.content}>
            Capture advice and registry recommendations from your
            friends and family in one convenient place.
          </div>
          <div className={classes.configContainer}>
            <div className={classes.configHeader}>Book Sizes</div>
            <ButtonGroup>
              <Button
                classes={{
                  root: classes.sizeButton,
                }}
                label="10 x 10"
                variant="configOption"
              />
              <Button
                label="12 x 12"
                variant="configOption"
              />
            </ButtonGroup>
          </div>
          <div className={classes.configContainer}>
            <div className={classes.configHeader}>Cover Colors</div>
            <div className={classes.coverOptionsContainer}>
              {bookCovers.map(color => (<BookColorChoice key={color} color={color} />))}
            </div>
          </div>
          <div className={classes.buttonContainer}>
            <Button
              label="finish your book"
              variant="primary"
              sz="lg"
            />
          </div>
        </div>
      </div>
      <AboutUs />
      <Footer />
    </section>
  );
};

export default Home;

