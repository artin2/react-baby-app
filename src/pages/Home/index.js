// import { Profile } ./component/Home./component/Profile';
import { Loadable } from '../../util/components';
import { HOME_PATH as path } from '../../constants/paths';

const HomeRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Login' */ './component'),
  }),
};

export { HomeRoute };
