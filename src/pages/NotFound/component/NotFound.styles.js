import { section, container } from '../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    background: '#FCD43A', // TODO: add to theme
  },
  section,
  container,
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '0.75em',
    height: 'calc(100vh-49px)',
  },
  height: {
    height: 'calc(100vh - 100px)',
    minHeight: '100%',
  },
  img: {
    minHeight: '300px',
    height: 'auto',
    maxHeight: '450px',
    objectFit: 'contain',
  },
  heading: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '32px',
    color: theme.palette.primary.main,
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: 600,
  },
  subheading: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '16px',
    color: theme.palette.black,
    textAlign: 'center',
  },
});

export { styles };

