import NotFound from './NotFound';
import enhance from './NotFound.enhancer';

export default enhance(NotFound);
