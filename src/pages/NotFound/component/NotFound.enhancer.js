import { memo } from 'react';
import { compose } from 'redux';
// import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './NotFound.styles';
// import { BranchHelper, mapDispatchToProps,
// mapStateToProps } from '../../../../containers/BranchHelper/BranchHelper';
// import { UserIsAuthenticated } from '../../../../util/router';

export default compose(
  // UserIsAuthenticated,
  // BranchHelper,
  // connect(mapStateToProps, mapDispatchToProps),
  memo,
  withStyles(styles),
);
