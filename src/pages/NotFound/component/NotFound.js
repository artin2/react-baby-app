import React from 'react';
import cn from 'classnames';
import LeftLogoHeader from '../../../components/Headers/LeftLogoHeader';
import FullButtonFooter from '../../../components/Footers/FullButtonFooter';
import fourOFour from '../../../assets/images/404@2x.png';

type Props = {
  classes: any,
}

const NotFound = ({ classes }: Props) => {
  return (
    <div className={classes.root}>
      <LeftLogoHeader variant="light" />
      <div className={cn(classes.section, classes.height)}>
        <div className={cn(classes.container, classes.content)}>
          <img className={classes.img} src={fourOFour} alt="Oops!" />
          <div className={classes.heading}>Oh Phooey!</div>
          <div className={classes.subheading}>You caught us slipping.  This page is broken.</div>
        </div>
      </div>
      <FullButtonFooter label="Back to Nestling" />
    </div>
  );
};

export default NotFound;
