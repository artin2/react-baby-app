import { Loadable } from '../../util/components';
import { NOT_FOUND_PATH as path } from '../../constants/paths';

const NotFoundRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'NotFound' */ './component'),
  }),
};

export { NotFoundRoute };
