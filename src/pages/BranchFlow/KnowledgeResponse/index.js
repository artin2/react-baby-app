import { Loadable } from '../../../util/components';
import { KNOWLEDGE_RESPONSE_PATH as path } from '../../../constants/paths';

const KnowledgeResponseRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'KnowledgeResponse' */ './component'),
  }),
};

export { KnowledgeResponseRoute };
