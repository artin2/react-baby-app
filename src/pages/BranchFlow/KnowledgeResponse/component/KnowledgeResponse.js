import React from 'react';
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import ResponseHeader from '../../../../components/ResponseHeader';
import ExpandingTextArea from '../../../../components/ExpandingTextArea';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { SHARE_EXPERIENCE_PATH, SHARE_KNOWLEDGE_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  branch: any,
  navigate: any,
  updateBranch: any,
};

const KnowledgeResponse = (props: Props) => {
  const {
    branch: { knowledge },
    classes,
    navigate,
    updateBranch,
  } = props;

  const { value } = knowledge;

  const handleValueChange = (event) => {
    const { target } = event;
    updateBranch('knowledge.response', target.value);
  };

  const handleOnNext = () => navigate(SHARE_EXPERIENCE_PATH);


  const handleOnBack = () => navigate(SHARE_KNOWLEDGE_PATH);

  return (
    <div className={classes.root}>
      <div className={classes.background}>
        <div className={classes.overlay}>
          <CenteredLogoHeader variant="transparent" />
          <div className={classes.section}>
            <div className={classes.container}>
              <div className={classes.content}>
                <ResponseHeader label={value} />
              </div>
            </div>
          </div>
        </div>
        <ExpandingTextArea placeholder="Start typing..." onChange={handleValueChange} />
      </div>
      <NavigationFooter
        nextLabel="Next"
        onNextClick={handleOnNext}
        onBackClick={handleOnBack}
      />
    </div>
  );
};

export default KnowledgeResponse;

