import KnowledgeResponse from './KnowledgeResponse';
import enhance from './KnowledgeResponse.enhancer';

export default enhance(KnowledgeResponse);
