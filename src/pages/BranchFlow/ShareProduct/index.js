import { Loadable } from '../../../util/components';
import { SHARE_PRODUCT_PATH as path } from '../../../constants/paths';

const ShareProductRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'ShareProduct' */ './component'),
  }),
};

export { ShareProductRoute };
