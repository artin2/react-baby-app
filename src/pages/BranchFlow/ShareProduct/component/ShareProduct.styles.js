import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  section: merge({}, section, {
    paddingBottom: '5rem',
  }),
  container,
  popularSubText: {
    fontSize: '1.3rem',
    paddingTop: '20px',
  },
  categoriesContainer: {
    padding: '1.25rem 0 2.5rem 0',
  },
});

export { styles };
