import ShareProduct from './ShareProduct';
import enhance from './ShareProduct.enhancer';

export default enhance(ShareProduct);
