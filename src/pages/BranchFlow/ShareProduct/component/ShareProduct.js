import React, { useEffect } from 'react';
import TitleHeader from '../../../../components/Headers/TitleHeader';
import ShareHeader from '../../../../components/ShareHeader';
import ShareSubHeader from '../../../../components/ShareSubHeader';
import List from '../../../../components/List';
import ProductListItem from '../../../../components/ProductListItem';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import CategoryTileGroup from '../../../../components/CategoryTileGroup';
// import CategoryTile from '../../../../components/CategoryTileGroup/CategoryTile';
import { PRODUCTS_COLLECTION } from '../../../../constants/firebase';
import { PRODUCT_RESPONSE_PATH, REVIEW_BRANCH_PATH, EXPERIENCE_RESPONSE_PATH } from '../../../../constants/paths';

type Props = {
  branch: any,
  classes: any,
  getQuery: any,
  navigate: any,
  setProduct: any,
};

const ShareProduct = (props: Props) => {
  const {
    branch: { product },
    classes,
    getQuery,
    navigate,
    setProduct,
  } = props;

  const { productSet } = product;
  const categories = [
    {
      label: 'Equipment',
      color: 'purple',
    },
    {
      label: 'Clothing',
      color: 'red',
    },
    {
      label: 'Food',
      color: 'orange',
    },
    {
      label: 'Furniture',
      color: 'yellow',
    },
    {
      label: 'Learning',
      color: 'green',
    },
    {
      label: 'Safety',
      color: 'blue',
    },
  ];

  useEffect(() => {
    const getProducts = async () => {
      await getQuery(PRODUCTS_COLLECTION, 'product.productSet');
    };
    getProducts();
    // NOTE: sort of hack for clean-up function
    return () => true;
  }, []);

  const handleCategoryClick = (event, category) => {
    console.log(event, category);
  }
  const handleClick = (id) => {
    setProduct(id);
    navigate(PRODUCT_RESPONSE_PATH);
  };

  const handleOnNext = () => navigate(REVIEW_BRANCH_PATH);

  const handleOnBack = () => navigate(EXPERIENCE_RESPONSE_PATH);
  return (
    <div className={classes.root}>
      <TitleHeader logoOnly />
      <div className={classes.section}>
        <div className={classes.container}>
          <ShareHeader label="Share your favorite product" />
          <div className={classes.popularSubText}>Popular Categories</div>
          <div className={classes.categoriesContainer}>
            <CategoryTileGroup categories={categories} onClick={handleCategoryClick} />
          </div>
          <ShareSubHeader label="Most Popular Recommendations" />
          <List>
            {productSet ? productSet.map(p => (
              <ProductListItem
                {...p}
                onClick={handleClick}
                key={p.id}
              />
            )) : null
            }
          </List>
        </div>
      </div>
      <NavigationFooter
        nextLabel="Skip"
        onNextClick={handleOnNext}
        onBackClick={handleOnBack}
      />
    </div>
  );
};

export default ShareProduct;
