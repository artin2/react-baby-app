import NestWelcome from './NestWelcome';
import enhance from './NestWelcome.enhancer';

export default enhance(NestWelcome);
