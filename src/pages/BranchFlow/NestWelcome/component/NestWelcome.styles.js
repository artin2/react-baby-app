import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';
import welcomeBackgroundImg from '../../../../assets/images/welcome-background-img@2x.png';
import { overlay } from '../../../../assets/styles/mixins';

const headerFontSize = 4;
const contentFontSize = 1.6;

const styles = theme => ({
  root: {
    background: {
      image: `url('${welcomeBackgroundImg}')`,
    },
    backgroundSize: 'cover',
    height: '100vh',
    width: '100vw',
    color: theme.palette.white,
  },
  overlay: overlay(),
  section: merge({}, section, {
    padding: '2.5rem',
    height: '80%',
  }),
  container: merge({}, container, {
    height: '80%',
  }),
  contentContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
    justifyContent: 'center',
  },
  header: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: `${headerFontSize * 0.8}rem`,
    textAlign: 'center',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${headerFontSize}rem`,
    },
  },
  content: {
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: `${contentFontSize * 0.8}rem`,
    textAlign: 'center',
    margin: '20px 20px',
    lineHeight: '22px',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${contentFontSize}rem`,
    },
  },
});

export { styles };
