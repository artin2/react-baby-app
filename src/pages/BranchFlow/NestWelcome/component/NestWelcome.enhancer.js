import { compose } from 'redux';
import { connect } from 'react-redux';
import { memo } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './NestWelcome.styles';
import { PageHelper, mapDispatchToProps, mapStateToProps } from '../../../../containers/PageHelper/PageHelper';
// import { UserIsAuthenticated } from '../../../../util/router';

export default compose(
  // UserIsAuthenticated,
  memo,
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
