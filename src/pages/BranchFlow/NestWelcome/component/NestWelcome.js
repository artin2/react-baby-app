import React, { useState, useEffect } from 'react';
import LeftLogoHeader from '../../../../components/Headers/LeftLogoHeader';
import TextField from '../../../../components/InputElements/TextField';
import FullButtonFooter from '../../../../components/Footers/FullButtonFooter';
import { NEST_ANNOUNCEMENT_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  queryParams: any,
  navigate: any,
  updateBranch: any,
  // branch: any,
  getNest: any,
  // pageCache: any,
};

const NestWelcome = (props: Props) => {
  const [nestId, setNestId] = useState('');
  const [disabled, setDisabled] = useState(true);
  const [uid, setUid] = useState('');
  const [isErrorShown, setIsErrorShown] = useState(false);
  const {
    classes,
    queryParams,
    updateBranch,
    navigate,
    getNest,
    // pageCache,
  } = props;

  useEffect(() => {
    const uidFromParam = queryParams ? queryParams.uid : 'lAEVYQARQtOz6SU4wYj01P1CEoL2';
    console.log(uidFromParam);
    updateBranch('uid', uidFromParam);
    setUid(uidFromParam);
  }, []);

  const handleOnChange = (event) => {
    if (isErrorShown) setIsErrorShown(false);
    const { target: { value } } = event;
    setNestId(value);
    if (value) setDisabled(false);
  };

  const handleNext = async () => {
    if (!disabled) {
      // TODO: getting uid from store isn't triggering
      // const { branch } = props;
      // const { uid } = branch;
      try {
        const response = await getNest(uid, nestId);
        if (response && response.exists) {
          navigate(NEST_ANNOUNCEMENT_PATH);
        } else {
          // TODO: make sure it disappears in every case
          setIsErrorShown(true);
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.backgroundImg}>
        <div className={classes.overlay}>
          <LeftLogoHeader />
          <div className={classes.section}>
            <div className={classes.container}>
              <div className={classes.contentContainer}>
                <div className={classes.header}>Welcome!</div>
                <div className={classes.content}>
                  {'We’re so excited to deliver your big news, but we’ll need to make sure you’re in the right place first.'}
                </div>
                <TextField
                  label="Nest ID"
                  hasIcon
                  onChange={handleOnChange}
                />
                {/* TODO: create actual error message */}
                {isErrorShown && <p style={{ color: 'pink', fontSize: '1.4rem' }}>Oops that is not a valid nest id</p>}
              </div>
            </div>
          </div>
        </div>
      </div>
      <FullButtonFooter onClick={handleNext} label="Next" disabled={disabled} />
    </div>
  );
};

export default NestWelcome;

