import { Loadable } from '../../../util/components';
import { WELCOME_PATH as path } from '../../../constants/paths';

const NestWelcomeRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Welcom' */ './component'),
  }),
};

export { NestWelcomeRoute };
