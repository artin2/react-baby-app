import ExperienceResponse from './ExperienceResponse';
import enhance from './ExperienceResponse.enhancer';

export default enhance(ExperienceResponse);
