import React from 'react';
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import ResponseHeader from '../../../../components/ResponseHeader';
import ExpandingTextArea from '../../../../components/ExpandingTextArea';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { SHARE_PRODUCT_PATH, SHARE_EXPERIENCE_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  branch: any,
  navigate: any,
  updateBranch: any,
};

const ExperienceResponse = (props: Props) => {
  const {
    branch: { experience },
    classes,
    navigate,
    updateBranch,
  } = props;

  const { value } = experience;

  const handleValueChange = (event) => {
    const { target } = event;
    updateBranch('experience.response', target.value);
  };

  const hanleOnNext = () => navigate(SHARE_PRODUCT_PATH);

  const handleOnBack = () => navigate(SHARE_EXPERIENCE_PATH);

  return (
    <div className={classes.root}>
      <div className={classes.background}>
        <div className={classes.overlay}>
          <CenteredLogoHeader variant="transparent" />
          <div className={classes.section}>
            <div className={classes.container}>
              <div className={classes.content}>
                <ResponseHeader label={value} />
              </div>
            </div>
          </div>
        </div>
        <ExpandingTextArea placeholder="Start typing..." onChange={handleValueChange} />
      </div>
      <NavigationFooter
        nextLabel="Next"
        onNextClick={hanleOnNext}
        onBackClick={handleOnBack}
      />
    </div>
  );
};

export default ExperienceResponse;

