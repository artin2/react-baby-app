import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';
import { overlay } from '../../../../assets/styles/mixins';
import responseBackgroundImg from '../../../../assets/images/experience-background@2x.png';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    overflow: 'scroll',
  },
  background: {
    background: {
      image: `url(${responseBackgroundImg})`,
    },
    backgroundSize: 'cover',
    height: '100vh',
    width: '100vw',
  },
  section: merge({}, section, {
    height: '80%',
    overflow: 'scroll',
  }),
  container: merge({}, container, {
    height: '80%',
  }),
  content: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
  },
  overlay: overlay(),
});

export { styles };
