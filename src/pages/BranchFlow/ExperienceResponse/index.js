import { Loadable } from '../../../util/components';
// import { ExperienceResponse } from './ExperienceResponse';
import { EXPERIENCE_RESPONSE_PATH as path } from '../../../constants/paths';

const ExperienceResponseRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Login' */ './component'),
  }),
};

export { ExperienceResponseRoute };
