import merge from 'lodash/merge';
import { section, container } from '../../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  section: merge({}, section, {
    marginBottom: '50px',
  }),
  container,
  coverPhotoHeader: {
    fontSize: '1.3rem',
    paddingBottom: '15px',
  },
});

export { styles };
