import CreateBranch from './CreateBranch';
import enhance from './CreateBranch.enhancer';

export default enhance(CreateBranch);
