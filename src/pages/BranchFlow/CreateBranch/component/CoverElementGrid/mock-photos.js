import smilePhoto from '../../../../../assets/images/coverPhotos/smile.png';
import soExcitedPhoto from '../../../../../assets/images/coverPhotos/so-excited.png';
import smileyBalloonPhoto from '../../../../../assets/images/coverPhotos/smiley-balloons.png';
import oopsPhoto from '../../../../../assets/images/coverPhotos/oops.png';
import flowersPhoto from '../../../../../assets/images/coverPhotos/flowers.png';
import celebratePhoto from '../../../../../assets/images/coverPhotos/celebrate.png';
import teddyPhoto from '../../../../../assets/images/coverPhotos/teddies.png';
import balloonsPhoto from '../../../../../assets/images/coverPhotos/balloons.png';
import confettiPhoto from '../../../../../assets/images/coverPhotos/confetti.png';
import soExcited2Photo from '../../../../../assets/images/coverPhotos/so-excited2.png';
import happyBookPhoto from '../../../../../assets/images/coverPhotos/happy-book.png';
import horsePhoto from '../../../../../assets/images/coverPhotos/horse.png';
import toastPhoto from '../../../../../assets/images/coverPhotos/toast.png';
import sweetsPhoto from '../../../../../assets/images/coverPhotos/sweets.png';
import holyCrapPhoto from '../../../../../assets/images/coverPhotos/holy-crap.png';

export const images = [
  {
    image: smilePhoto,
    label: 'smile',
  },
  {
    image: soExcitedPhoto,
    label: 'so-excited',
  },
  {
    image: smileyBalloonPhoto,
    label: 'smiley-balloons',
  },
  {
    image: oopsPhoto,
    label: 'oops',
  },
  {
    image: flowersPhoto,
    label: 'flowers',
  },
  {
    image: celebratePhoto,
    label: 'celebrate',
  },
  {
    image: teddyPhoto,
    label: 'teddy',
  },
  {
    image: balloonsPhoto,
    label: 'balloons',
  },
  {
    image: confettiPhoto,
    label: 'confetti',
  },
  {
    image: soExcited2Photo,
    label: 'so-excited-2',
  },
  {
    image: happyBookPhoto,
    label: 'happy-book',
  },
  {
    image: horsePhoto,
    label: 'horse',
  },
  {
    image: toastPhoto,
    label: 'toast',
  },
  {
    image: sweetsPhoto,
    label: 'sweets',
  },
  {
    image: holyCrapPhoto,
    label: 'holy-crap',
  },
];
