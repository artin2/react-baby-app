import React from 'react';
import { storiesOf } from '@storybook/react';
import CoverElementGrid from '../index';
import smilePhoto from '../../../../../../assets/images/coverPhotos/smile.png';
import soExcitedPhoto from '../../../../../../assets/images/coverPhotos/so-excited.png';
import smileyBalloonPhoto from '../../../../../../assets/images/coverPhotos/smiley-balloons.png';

const stories = storiesOf('CoverElementGrid', module);

const images = [
  {
    image: smilePhoto,
    label: 'smile',
  },
  {
    image: soExcitedPhoto,
    label: 'so-excited',
  },
  {
    image: smileyBalloonPhoto,
    label: 'smiley-balloons',
  },
  {
    image: smileyBalloonPhoto,
    label: 'smiley-balloons',
  },
  {
    image: smilePhoto,
    label: 'smile',
  },
  {
    image: soExcitedPhoto,
    label: 'so-excited',
  },
  // {
  //   image: smileyBalloonPhoto,
  //   label: 'smiley-balloons',
  // },
  // {
  //   image: smilePhoto,
  //   label: 'smile',
  // },
  // {
  //   image: soExcitedPhoto,
  //   label: 'so-excited',
  // },
  // {
  //   image: smileyBalloonPhoto,
  //   label: 'smiley-balloons',
  // },
  // {
  //   image: smilePhoto,
  //   label: 'smile',
  // },
  // {
  //   image: soExcitedPhoto,
  //   label: 'so-excited',
  // },
  // {
  //   image: smileyBalloonPhoto,
  //   label: 'smiley-balloons',
  // },
];
stories.add('Normal', () => (
  <CoverElementGrid images={images} />
));
