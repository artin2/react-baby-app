import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import CoverPhotoElement from '../CoverPhotoElement';

type Props = {
  classes: any,
  images: string[],
}
const CoverElementGrid = ({ classes, images }: Props) => {
  const [selectedCoverPhoto, setSelectedCoverPhoto] = useState('');
  // TODO: create a util function of out this
  const renderGrid = () => {
    const gridArray = [];
    // may need to keep the images length as a multiple of 3
    for (let i = 0; i < images.length; i += 3) {
      gridArray.push(images.slice(i, i + 3));
    }
    return gridArray;
  };

  const handleOnClick = (event) => {
    const { target: { alt } } = event;
    setSelectedCoverPhoto(alt);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={0} justify="center" style={{ marginLeft: '8px' }}>
        {renderGrid().map(element => (
          <Grid container spacing={0}>
            <Grid item xs={4}>
              <CoverPhotoElement
                key={element[0].label}
                image={element[0].image}
                altText={element[0].label}
                isSelected={selectedCoverPhoto === element[0].label}
                onClick={handleOnClick}
              />
            </Grid>
            <Grid item xs={4}>
              <CoverPhotoElement
                key={element[1].label}
                image={element[1].image}
                altText={element[1].label}
                isSelected={selectedCoverPhoto === element[1].label}
                onClick={handleOnClick}
              />
            </Grid>
            <Grid item xs={4}>
              <CoverPhotoElement
                key={element[2].label}
                image={element[2].image}
                altText={element[2].label}
                isSelected={selectedCoverPhoto === element[2].label}
                onClick={handleOnClick}
              />
            </Grid>
          </Grid>
        ))}

      </Grid>
    </div>
  );
};

export default CoverElementGrid;
