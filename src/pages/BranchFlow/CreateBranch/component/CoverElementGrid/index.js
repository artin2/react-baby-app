import CoverElementGrid from './CoverElementGrid';
import enhance from './CoverElementGrid.enhancer';

export default enhance(CoverElementGrid);
