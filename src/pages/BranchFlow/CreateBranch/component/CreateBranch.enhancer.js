import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { memo } from 'react';
import { compose } from 'redux';
import { PageHelper, mapDispatchToProps, mapStateToProps } from '../../../../containers/PageHelper/PageHelper';
import { styles } from './CreateBranch.styles';

export default compose(
  memo,
  PageHelper,
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
);
