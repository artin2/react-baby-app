const styles = theme => ({
  root: {
    position: 'relative',
    paddingBottom: '5px',
    '& >img': {
      height: '10.4rem',
      width: '10.4rem',
    },
  },
  selected: {
    '&> img': {
      boxShadow: '0 8px 16px rgba(0,0,0,0.64)',
    },
  },
  iconHidden: {
    display: 'none',
  },
  iconShow: {
    display: 'inline-block',
    fill: theme.palette.selected,
    position: 'absolute',
    top: '5px',
    left: '78px',
    background: 'white',
    borderRadius: '50%', // TODO: create another div that is slightly smaller to only have the check white
  },
});

export { styles };
