import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CoverPhotoElement from '../index';
import excitedCoverPhoto from '../../../../../../assets/images/coverPhotos/so-excited.png';

const stories = storiesOf('CoverPhotoElement', module);

stories.add('Normal', () => {
  return (
    <CoverPhotoElement
      altText="balloons"
      image={excitedCoverPhoto}
      onClick={action('image selected')}
    />
  );
});

stories.add('Selected', () => {
  return (
    <CoverPhotoElement
      altText="balloons"
      image={excitedCoverPhoto}
      onClick={action('image selected')}
      isSelected
    />
  );
});
