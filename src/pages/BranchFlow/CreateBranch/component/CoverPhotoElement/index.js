import CoverPhotoElement from './CoverPhotoElement';
import enhance from './CoverPhotoElement.enhancer';

export default enhance(CoverPhotoElement);
