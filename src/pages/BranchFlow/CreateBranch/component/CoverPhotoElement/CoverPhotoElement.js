import React, { SyntheticEvent } from 'react';
import cn from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

type Props = {
  altText: string,
  classes: any,
  image: String,
  isSelected: boolean,
  onClick: (SyntheticEvent) => void,
};

const CoverPhotoElement = ({
  classes, image, altText, isSelected, onClick,
}: Props) => {
  return (
    <div
      className={cn(classes.root, { [classes.selected]: isSelected })}
      onClick={onClick}
      role="button"
      tabIndex={0}
      onKeyPress={onClick}
    >
      <img src={image} alt={altText} />
      <CheckCircleIcon
        classes={{
          root: isSelected ? classes.iconShow : classes.iconHidden,
        }}
      />
    </div>
  );
};

export default CoverPhotoElement;
