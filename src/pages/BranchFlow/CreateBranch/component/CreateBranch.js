import React, { useState /* , useEffect */ } from 'react';
// import cn from 'classnames';
import TitleHeader from '../../../../components/Headers/TitleHeader';
import FullButtonFooter from '../../../../components/Footers/FullButtonFooter';
import UploadImageCard from '../../../../components/UploadImageCard';
import TextField from '../../../../components/InputElements/TextField';
import { setUpFileForUpload, generateBranchId } from '../../../../util/util';
import { SHARE_KNOWLEDGE_PATH } from '../../../../constants/paths';
import { BRANCH_STORAGE_PATH } from '../../../../constants/firebase';
import CoverPhotoGrid from './CoverElementGrid';
import { images } from './CoverElementGrid/mock-photos';
// import avatarPlaceholder from '../../../../assets/images/avatar-placeholder.png';

type Props = {
  branch: any,
  classes: any,
  navigate: any,
  updateBranch: any,
  uploadPhoto: any,
}
const CreateBranch = (props: Props) => {
  const {
    branch,
    classes,
    navigate,
    updateBranch,
    uploadPhoto,
  } = props;

  // const [hasProfilePicture, setHasProfilePicture] = useState(false);
  const [profileImage, setProfileImage] = useState();
  // const [coverImage, setCoverImage] = useState(null);

  // useEffect(() => {
  //   setProfileImage(branch.profileImage);
  // }, [branch.profileImage]);

  // useEffect(() => {
  //   setCoverImage(branch.coverImage);
  // }, [branch.coverImage]);

  // const handleValueOnChange = (event) => {
  //   const { target: { id, value } } = event;
  //   updateBranch(id, value);
  //   if (!hasProfilePicture && id !== 'coverCaption') {
  //     const { firstName, lastName } = branch;
  //     const image = `https://ui-avatars.com/api/?name=${firstName}+${lastName}&background=2252BF&color=fff`;
  //     console.log(image);
  //     setProfileImage(image);
  //     updateBranch('profileImage', image);
  //   }
  // };

  const handleUploadProfileImage = async (file) => {
    const fileData = setUpFileForUpload(file);
    const folderConfig = {
      path: BRANCH_STORAGE_PATH,
      child: `${branch.firstName}_profile`,
      update: 'branch',
      updatePath: 'profileImage',
    };
    try {
      const image = await uploadPhoto(fileData, folderConfig);
      setProfileImage(image);
    } catch (err) {
      // TODO: handle error
      console.log(err);
    }
  };

  // const handleProfileUpload = async (file) => {
  //   const { target: { files } } = file;
  //   console.log(files);
  //   if (files && files.length > 0) {
  //     const fileData = setUpFileForUpload(files[0]);
  //     const folderConfig = {
  //       path: BRANCH_STORAGE_PATH,
  //       child: branch.firstName,
  //       update: 'branch',
  //       updatePath: 'profileImage',
  //     };
  //     try {
  //       await uploadPhoto(fileData, folderConfig);
  //       setHasProfilePicture(true);
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
  // };

  const handleNext = () => {
    const { firstName, nestId } = branch;
    const id = generateBranchId(firstName, nestId);
    updateBranch('id', id);
    navigate(SHARE_KNOWLEDGE_PATH);
  };

  return (
    <div className={classes.root}>
      <TitleHeader titleText="Your Profile" />
      <UploadImageCard
        onUpload={handleUploadProfileImage}
        uploadTypeText="Profile"
        image={profileImage}
        placeholderShape="round"
      />
      <div className={classes.section}>
        <div className={classes.container}>
          <TextField label="First Name" variant="light" />
          <TextField label="Last Name" variant="light" />
          <div className={classes.coverPhotoSelectionContainer}>
            <div className={classes.coverPhotoHeader}>Choose a Cover Photo</div>
            <CoverPhotoGrid images={images} />
          </div>
        </div>
      </div>
      <FullButtonFooter
        label="Next"
        onClick={handleNext}
      />
    </div>
  );
};

export default CreateBranch;

