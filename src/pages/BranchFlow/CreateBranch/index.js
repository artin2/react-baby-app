import { Loadable } from '../../../util/components';
// import { ExperienceResponse } from './ExperienceResponse';
import { CREATE_BRANCH_PATH as path } from '../../../constants/paths';

const CreateBranchRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Login' */ './component'),
  }),
};

export { CreateBranchRoute };
