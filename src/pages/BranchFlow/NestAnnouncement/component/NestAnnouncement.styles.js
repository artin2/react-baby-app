import { section, container } from '../../../../assets/styles/global.styles';

const headerFontSize = 2;
const contentFontSize = 1.4;
const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  section,
  container,
  contentBackground: {
    backgroundColor: theme.palette.white,
  },
  header: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    textAlign: 'center',
    margin: '0 2rem',
    fontSize: `${headerFontSize * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: `${headerFontSize}rem`,
    },
    paddingBottom: '2.7rem',
  },
  img: {
    height: 'auto',
    width: '100%',
    margin: 'auto',
    borderRadius: '4px',
  },
  imageContainer: {
    height: '32rem',
    overflow: 'hidden',
    borderRadius: '4px',
    [theme.breakpoints.up('sm')]: {
      height: '40rem',
    },
  },
  nestMessage: {
    fontFamily: theme.typography.fontFamily.primary.medium,
    lineHeight: '20px',
    fontSize: `${contentFontSize * 0.8}rem`,
    padding: '1.1rem 1rem',
    [theme.breakpoints.up('sm')]: {
      fontSize: `${contentFontSize}rem`,
    },
  },
  replyHeader: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: `${1.4 * 0.8}rem`,
    [theme.breakpoints.up('sm')]: {
      fontSize: '1.4rem',
    },
  },
  subtext: {
    fontFamily: theme.typography.fontFamily.primary.medium,
    padding: '5px 0 2.4rem 0',
    fontSize: '1.2rem',
  },
  button: {
    marginRight: '20px',
  },
});

export { styles };
