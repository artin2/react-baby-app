import NestAnnouncement from './NestAnnouncement';
import enhance from './NestAnnouncement.enhancer';

export default enhance(NestAnnouncement);
