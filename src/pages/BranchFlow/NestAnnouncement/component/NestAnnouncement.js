/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect } from 'react';
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import RoundImage from '../../../../components/RoundImage';
import ButtonGroup from '../../../../components/ButtonGroup';
import Button from '../../../../components/Button';
import { CREATE_BRANCH_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  name: string,
  navigate: any,
  nest: any,
  user: any,
  getUser: any,
}
const NestAnnouncement = (props: Props) => {
  const {
    classes,
    name,
    user,
    getUser,
    nest: { imageUrl, nestMessage },
  } = props;

  // TODO: may want to do this initially when the app first loads
  useEffect(() => {
    const { uid } = user;
    const get = async () => {
      await getUser(uid);
    };
    get();

    return () => true;
  }, []);

  const handleReply = () => {
    const { navigate } = props;
    navigate(CREATE_BRANCH_PATH);
  };
  console.log(user);
  return (
    <section className={classes.root}>
      <CenteredLogoHeader variant="light" />
      <div className={classes.contentBackground}>
        <div className={classes.section}>
          <div className={classes.container}>
            <div className={classes.header}>
              {name || 'Sara'} has some super special news to share
            </div>
            <div className={classes.imageContainer}>
              <img className={classes.img} src={imageUrl} alt="Nest" />
            </div>
            <div className={classes.profileImg}>
              <RoundImage hasBorder image={user.profileImage} />
            </div>
            <div className={classes.nestMessage}>{nestMessage}</div>
          </div>
        </div>
      </div>
      <div className={classes.section}>
        <div className={classes.container}>
          <div className={classes.replyHeader}>Would you like to send a reply?</div>
          <div className={classes.subtext}>
            It only takes a few minutes to pass along your best wishes
          </div>
          <ButtonGroup>
            <Button
              classes={{
                root: classes.button,
              }}
              label="No Thanks"
              sz="lg"
              variant="secondaryOutline"
            />
            <Button
              label="Reply"
              sz="lg"
              variant="primary"
              onClick={handleReply}
            />
          </ButtonGroup>
        </div>
      </div>
    </section>
  );
};

// const NestAnnouncement = withStyles(styles)(nestAnnountment);

export default NestAnnouncement;
