import { Loadable } from '../../../util/components';
import { NEST_ANNOUNCEMENT_PATH as path } from '../../../constants/paths';

const NestAnnouncementRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Login' */ './component'),
  }),
};

export { NestAnnouncementRoute };
