import { Loadable } from '../../../util/components';
import { REVIEW_BRANCH_PATH as path } from '../../../constants/paths';

const ReviewBranchRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'Review Branch' */ './component'),
  }),
};

export { ReviewBranchRoute };
