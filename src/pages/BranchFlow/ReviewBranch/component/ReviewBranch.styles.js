const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
    paddingBottom: '6rem',
    [theme.breakpoints.down('sm')]: {
      paddingBottom: '4rem',
    },
  },
  coverImage: {
    width: '100%',
    borderRadius: '8px',
  },
  caption: {
    fontSize: '1.4rem',
    fontFamily: theme.typography.fontFamily.primary.demibold,
    padding: '3.2rem 1rem 1rem 1rem',
    lineHeight: '20px',
    textAlign: 'center',
  },
  productResponse: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '2rem',
    color: theme.palette.white,
    zIndex: 999,
    marginTop: '50%',
  },
  linkContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productLink: {
    zIndex: 999,
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '1.4rem',
    color: theme.palette.white,
    padding: '2.5rem 0',
  },
});

export { styles };
