import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
// import omit from 'lodash/omit';
import ReviewHeader from '../../../../components/ReviewHeader';
import BackgroundContainer from '../../../../components/BackgroundContainer';
import BranchProfile from '../../../../components/BranchProfile';
import BranchHeader from '../../../../components/BranchHeader';
import BranchPreviewChoice from '../../../../components/BranchPreviewChoice';
import BranchChoiceResponse from '../../../../components/BranchChoiceResponse';
import FullButtonFooter from '../../../../components/Footers/FullButtonFooter';
import ImageBackgroundContainer from '../../../../components/ImageBackgroundContainer';
import Chevron from '../../../../components/Chevron';

type Props = {
  branch: any,
  classes: any,
  addBranchToNest: any,
  user: any,
}

const ReviewBranch = (props: Props) => {
  const {
    branch,
    classes,
    addBranchToNest,
    user,
  } = props;

  const [isPublished, setIsPublished] = useState(false);

  const handleClick = async () => {
    const { uid } = user;
    console.log(uid);
    // const { uid, nestId } = branch;
    await addBranchToNest(branch, uid);
    // const newBranch = omit(branch, ['uid', 'nestId']);
    // TODO async create branch
    // await addBranchToNest(uid, nestId, newBranch);
    setIsPublished(true);
  };

  const {
    coverImage,
    coverCaption,
    firstName,
    profileImage,
    knowledge,
    experience,
    product,
  } = branch;

  const headerLabel = isPublished ? 'Your Branch Has Been Published' : 'Review Your Branch';
  const footerVariant = isPublished ? 'secondary' : 'primary';
  const buttonLabel = isPublished ? 'Browse Other Branches' : 'Publish Branch';

  return (
    <div className={classes.root}>
      <ReviewHeader label={headerLabel} disabled={!isPublished} />
      <BackgroundContainer variant="gray">
        <Grid container>
          <Grid item xs={6}>
            <img className={classes.coverImage} src={coverImage} alt={firstName} />
          </Grid>
          <Grid item xs={6}>
            <BranchProfile
              date="Aug 9, 2019"
              img={profileImage}
              name={firstName}
              orientation="vertical"
            />
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={12}
          className={classes.caption}
          justify="center"
        >
          {coverCaption}
        </Grid>
      </BackgroundContainer>
      <BackgroundContainer variant="pink">
        <BranchHeader header="Answers" variant="light" />
        <BranchPreviewChoice label={knowledge.value} />
        <BranchChoiceResponse content={knowledge.response} />
      </BackgroundContainer>
      <BackgroundContainer variant="yellow">
        <BranchHeader header="Advice" variant="light" />
        <BranchPreviewChoice label={experience.value} />
        <BranchChoiceResponse content={experience.response} />
      </BackgroundContainer>
      <ImageBackgroundContainer image={product.image.large}>
        <BranchHeader header="Recommendations" variant="dark" />
        <div className={classes.productResponse}>{product.response}</div>
        <div className={classes.linkContainer}>
          <div className={classes.productLink}>Find it on {product.source.label}</div>
          {/* TODO: need to update the size */}
          <Chevron direction="right" variant="dark" />
        </div>
      </ImageBackgroundContainer>
      <FullButtonFooter label={buttonLabel} onClick={handleClick} variant={footerVariant} />
    </div>
  );
};

export default ReviewBranch;

