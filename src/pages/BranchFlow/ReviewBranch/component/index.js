import ReviewBranch from './ReviewBranch';
import enhance from './ReviewBranch.enhancer';

export default enhance(ReviewBranch);
