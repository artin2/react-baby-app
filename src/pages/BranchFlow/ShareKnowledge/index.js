import { Loadable } from '../../../util/components';
import { SHARE_KNOWLEDGE_PATH as path } from '../../../constants/paths';

const ShareKnowledgeRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'ShareKnowledge' */ './component'),
  }),
};

export { ShareKnowledgeRoute };
