import React, { useEffect } from 'react'; // , useState
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import List from '../../../../components/List';
import SelectionListItem from '../../../../components/SelectionListItem';
import ShareHeader from '../../../../components/ShareHeader';
import ShareSubtext from '../../../../components/ShareSubtext';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { QUESTIONS_COLLECTION } from '../../../../constants/firebase';
import { KNOWLEDGE_RESPONSE_PATH, CREATE_BRANCH_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  getQuery: any,
  branch: any,
  updateBranch: any,
  navigate: any,
}

const ShareKnowledge = (props: Props) => {
  const {
    branch: { knowledge },
    classes,
    getQuery,
    navigate,
    updateBranch,
  } = props;

  const { knowledgeSet } = knowledge;

  useEffect(() => {
    const get = async () => {
      await getQuery(QUESTIONS_COLLECTION, 'knowledge.knowledgeSet');
    };

    get();

    return () => true;
  }, []);

  const handleClick = (event, k) => {
    updateBranch('knowledge.id', k.id);
    updateBranch('knowledge.value', k.value);
    navigate(KNOWLEDGE_RESPONSE_PATH);
  };

  const onNextClick = () => navigate(KNOWLEDGE_RESPONSE_PATH);

  const onBackClick = () => navigate(CREATE_BRANCH_PATH);

  return (
    <div className={classes.root}>
      <CenteredLogoHeader variant="light" />
      <div className={classes.section}>
        <div className={classes.container}>
          <ShareHeader label="Share your knowledge" />
          <ShareSubtext label="Choose one questions" />
        </div>
      </div>
      <List>
        {knowledgeSet ? knowledgeSet.map(el => (
          <SelectionListItem
            item={el}
            key={el.id}
            onClick={handleClick}
          />
        )) : null}
      </List>
      <NavigationFooter
        nextLabel="Skip"
        onNextClick={onNextClick}
        onBackClick={onBackClick}
      />
    </div>
  );
};

export default ShareKnowledge;
