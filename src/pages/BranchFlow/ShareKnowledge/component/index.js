import ShareKnowledge from './ShareKnowledge';
import enhance from './ShareKnowledge.enhancer';

export default enhance(ShareKnowledge);
