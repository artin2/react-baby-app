import { Loadable } from '../../../util/components';
import { SHARE_EXPERIENCE_PATH as path } from '../../../constants/paths';

const ShareExperienceRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'ShareExperience' */ './component'),
  }),
};

export { ShareExperienceRoute };
