import ShareExperience from './ShareExperience';
import enhance from './ShareExperience.enhancer';

export default enhance(ShareExperience);
