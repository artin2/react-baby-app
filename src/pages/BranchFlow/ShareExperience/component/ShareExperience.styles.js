import { section, container } from '../../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.primary.main,
  },
  section,
  container,
});

export { styles };
