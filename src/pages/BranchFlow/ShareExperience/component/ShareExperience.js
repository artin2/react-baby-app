import React, { useEffect } from 'react';
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import List from '../../../../components/List';
import SelectionListItem from '../../../../components/SelectionListItem';
import ShareHeader from '../../../../components/ShareHeader';
import ShareSubtext from '../../../../components/ShareSubtext';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import { EXPERIENCE_TOPICS_COLLECTION } from '../../../../constants/firebase';
import { EXPERIENCE_RESPONSE_PATH, KNOWLEDGE_RESPONSE_PATH } from '../../../../constants/paths';

type Props = {
  classes: any,
  branch: any,
  updateBranch: any,
  navigate: any,
  getQuery: any,
};

const ShareExperience = (props: Props) => {
  const {
    branch: { experience },
    classes,
    getQuery,
    navigate,
    updateBranch,
  } = props;

  const { experienceSet } = experience;

  useEffect(() => {
    const get = async () => {
      await getQuery(EXPERIENCE_TOPICS_COLLECTION, 'experience.experienceSet');
    };
    get();

    return () => true;
  }, []);

  const handleClick = (event, topic) => {
    updateBranch('experience.id', topic.id);
    updateBranch('experience.value', topic.value);
    navigate(EXPERIENCE_RESPONSE_PATH);
  };

  const handleOnNext = () => navigate(EXPERIENCE_RESPONSE_PATH);

  const handleOnBack = () => navigate(KNOWLEDGE_RESPONSE_PATH);

  return (
    <div className={classes.root}>
      <CenteredLogoHeader variant="light" />
      <div className={classes.section}>
        <div className={classes.container}>
          <ShareHeader label="Share your experience" />
          <ShareSubtext label="Choose one questions" />
        </div>
      </div>
      <List>
        {experienceSet ? experienceSet.map(el => (
          <SelectionListItem
            item={el}
            key={el.id}
            onClick={handleClick}
          />
        )) : null}
      </List>
      <NavigationFooter
        nextLabel="Skip"
        onNextClick={handleOnNext}
        onBackClick={handleOnBack}
      />
    </div>
  );
};

export default ShareExperience;

