import { Loadable } from '../../../util/components';
import { PRODUCT_RESPONSE_PATH as path } from '../../../constants/paths';

const ProductResponseRoute = {
  path,
  component: Loadable({
    loader: () => import(/* webpackChunkName: 'ProductResponse' */ './component'),
  }),
};

export { ProductResponseRoute };
