/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import ProgressiveImage from 'react-progressive-image';
import CenteredLogoHeader from '../../../../components/Headers/CenteredLogoHeader';
import ExpandingTextArea from '../../../../components/ExpandingTextArea';
import NavigationFooter from '../../../../components/Footers/NavigationFooter';
import ProductDetailsHeader from '../../../../components/ProductDetailsHeader';
import { REVIEW_BRANCH_PATH } from '../../../../constants/paths';

// import responseBackgroundImg from '../../../assets/images/share-response-bg-img@2x.png';

type Props = {
  classes: any,
  branch: any,
  navigate: any,
  updateBranch: any,
};

const ProductResponse = (props: Props) => {
  const {
    branch: { product },
    classes,
    navigate,
    updateBranch,
  } = props;

  const { image } = product;

  const handleChange = (event) => {
    const { target: { value } } = event;
    updateBranch('product.response', value);
  };

  const handleNext = () => {
    navigate(REVIEW_BRANCH_PATH);
  };

  return (
    <section className={classes.root}>
      <div className={classes.backgroundImg}>
        {/* <img className={classes.backgroundImg}
        src={responseBackgroundImg} alt="background" /> */}
        <div className={classes.overlay} />
        <CenteredLogoHeader variant="transparent" />
        <div className={classes.content}>
          <div className={classes.section}>
            <div className={classes.container}>
              <ProductDetailsHeader {...product} />
            </div>
          </div>
          <ProgressiveImage src={image.large} placeholder={image.small} delay={500}>
            {src => <img className={classes.img} src={src} alt={product.productName} />}
          </ProgressiveImage>
          <ExpandingTextArea
            classes={{ root: classes.expandingTextArea }}
            onChange={handleChange}
          />
        </div>
      </div>
      <NavigationFooter backLabel="Skip" nextLabel="Next" onNextClick={handleNext} />
    </section>
  );
};

export default ProductResponse;
