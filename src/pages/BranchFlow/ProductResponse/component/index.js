import ProductResponse from './ProductResponse';
import enhance from './ProductResponse.enhancer';

export default enhance(ProductResponse);
