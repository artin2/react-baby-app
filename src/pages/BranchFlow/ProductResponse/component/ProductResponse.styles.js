import merge from 'lodash/merge';
import responseBackgroundImg from '../../../../assets/images/share-response-bg-img@2x.png';
import { overlay } from '../../../../assets/styles/mixins';
import { section, container } from '../../../../assets/styles/global.styles';

const styles = theme => ({
  root: {
    fontFamily: theme.typography.fontFamily.secondary,
    color: theme.palette.white,
    display: 'inline',
  },
  backgroundImg: {
    background: {
      image: `url(${responseBackgroundImg})`,
    },
    backgroundSize: 'cover',
    height: '100vh',
    content: '""',
    width: '100vw',
    position: 'relative',
  },
  overlay: overlay(),
  section: merge({}, section, {
    zIndex: 999,
  }),
  container,
  content: {
    display: 'flex',
    flexDirection: 'column',
  },
  productName: {
    fontFamily: theme.typography.fontFamily.primary.bold,
    fontSize: '2rem',
    zIndex: 999,
    textAlign: 'center',
    margin: '2rem 0 1rem 0',
  },
  productDetails: {
    fontFamily: theme.typography.fontFamily.primary.main,
    fontSize: '1.4rem',
    textAlign: 'center',
  },
  img: {
    width: '100%',
    height: 'auto',
    zIndex: 999,
    position: 'absolute',
    bottom: '20%',
  },
  expandingTextArea: {
    borderRadius: 0,
  },
});

export { styles };
