/* eslint-disable import/no-default-export */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'; // , Redirect
import { HomeRoute } from './Home';
import { ProfileRoute } from './Profile';
import { NestOverviewRoute } from './NestOverview';
// CREATE NEST FLOW TODO:
import { ForWhoRoute } from './NestFlow/ForWho';
import { FindFriendRoute } from './NestFlow/FindFriend';
import { SaveNestRoute } from './NestFlow/SaveNest';
import { CreateAnnouncementRoute } from './NestFlow/CreateAnnouncement';
import { ConfirmAnnouncementRoute } from './NestFlow/ConfirmAnnouncment';
// BRANCH FLOW // TODO: create sub routes
import { NestWelcomeRoute } from './BranchFlow/NestWelcome';
import { NestAnnouncementRoute } from './BranchFlow/NestAnnouncement';
import { CreateBranchRoute } from './BranchFlow/CreateBranch';
import { ShareKnowledgeRoute } from './BranchFlow/ShareKnowledge';
import { KnowledgeResponseRoute } from './BranchFlow/KnowledgeResponse';
import { ShareExperienceRoute } from './BranchFlow/ShareExperience';
import { ExperienceResponseRoute } from './BranchFlow/ExperienceResponse';
import { ShareProductRoute } from './BranchFlow/ShareProduct';
import { ProductResponseRoute } from './BranchFlow/ProductResponse';
import { ReviewBranchRoute } from './BranchFlow/ReviewBranch';

// 404/ERROR ROUTE
import { NotFoundRoute } from './NotFound';

// import FirebaseProvider from '../firebase/firebaseProvider';
// import firebase from '../config/firebase';
import { HOME_PATH } from '../constants/paths';
// const rrfProps = {
//   firebase,
//   config: {},
//   // dispatch: store.dispatch,
//   // createFirestoreInstance // <- needed if using firestore
// };
// const renderPage = (props: { store: any }, component) => {
//   console.log(props);
//   const Component = component;
//   const { store } = props;
//   return (
//     <FirebaseProvider {...rrfProps} dispatch={store.dispatch}>
//       <Component {...props} />
//     </FirebaseProvider>
//   )
// }

export default function createRoutes(store) {
  console.log(store.getState());
  return (
    <Switch>
      <Route exact path={HomeRoute.path} component={HomeRoute.component} />
      {/* Build Route components from routeSettings */
        [
          HomeRoute,
          ProfileRoute,
          ForWhoRoute,
          FindFriendRoute,
          SaveNestRoute,
          CreateAnnouncementRoute,
          ConfirmAnnouncementRoute,
          NestWelcomeRoute,
          NestAnnouncementRoute,
          CreateBranchRoute,
          ShareKnowledgeRoute,
          KnowledgeResponseRoute,
          ShareExperienceRoute,
          ExperienceResponseRoute,
          ShareProductRoute,
          ProductResponseRoute,
          ReviewBranchRoute,
          NestOverviewRoute,
          /* Add More Routes Here */
        ].map((settings, index) => (
          <Route key={`Route-${index}`} {...settings} />
        ))}
      <Redirect exact from="/" to={HOME_PATH} />
      <Route path="*" component={NotFoundRoute.component} />
    </Switch>
  );
}
