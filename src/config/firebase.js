import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyDpH5iQifT6cBrEit7DgZzHZstRiAc9Tqs',
  authDomain: 'qursive-7030a.firebaseapp.com',
  databaseURL: 'https://qursive-7030a.firebaseio.com',
  projectId: 'qursive-7030a',
  storageBucket: 'qursive-7030a.appspot.com',
  messagingSenderId: '349060364170',
};

firebase.initializeApp(config);

export default firebase;
