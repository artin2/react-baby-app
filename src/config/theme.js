import { createMuiTheme } from '@material-ui/core/styles';

const blue = '#0743BE';
const yellow = '#F4C203';
const pink = '#E7228E';
// const lightPink = '#EFA2BD';
const black = '#091633';
const gray = '#A5A9B5';
const white = '#F5F4F2';
const baseBlue = '#091633';
const backgroundGray = '#F5F4F2';

const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 361,
      md: 376,
      lg: 560, // TODO: fix lg and xl
      xl: 1169,
    },
  },
  palette: {
    base: {
      main: baseBlue,
    },
    primary: {
      light: '#FF55C1',
      main: pink,
    },
    background: {
      gray: backgroundGray,
      pink: '#DD7BB0',
      yellow: '#F2D083',
      white: '#FFFFFF',
    },
    black,
    blue,
    yellow,
    gray,
    white,
    pink,
    text: {
      label: '#363239',
      disabled: '#848A99',
      branchHeader: '#AD015E',
      white: '#FFFFFF',
    },
    borderGray: '#CCCCCC',
    bookCover: {
      navy: '#293947',
      yellow: '#CF924A',
      red: '#7C2E2E',
      gray: '#CFCFCF',
      pink: '#EDD9D8',
      seafoam: '#9CAFB1',
      teal: '#3B6068',
    },
    selected: '#66E317',
  },
  typography: {
    fontFamily: {
      primary: {
        main: 'Avenir-Next, sans-serif',
        medium: 'Avenir-Next-Medium, sans-serif',
        demiBold: 'Avenir-Next-Demi-Bold, sans-serif',
        bold: 'Avenir-Next-Bold, sans-serif',
      },
      secondary: 'Lato, sans-serif',
      tertiary: 'Open Sans, sans-serif',

    },
    useNextVariants: true,
    htmlFontSize: 10,
  },
  overrides: {
    MuiToolbar: {
      regular: {
        minHeight: '4.48rem',
      },
    },
    MuiInputBase: {
      root: {
        marginBottom: '1.3rem',
        width: '100%',
        '& $light': {
          '&:before': {
            borderBottomColor: '#CCCCCC !important',
          },
        },
        '&:after': {
          borderBottomColor: `${pink} !important`,
        },
      },
      input: {
        fontFamily: 'Avenir-Next, sans-serif',
        fontSize: '1.4rem',
        // color: '#FFFFFF',
      },
    },
    MuiInput: {
      root: {
        fontFamily: 'Avenir-Next, sans-serif',
        '&:before': {
          borderBottomColor: '#CCCCCC !important',
        },
      },
    },
  },
});

export { theme };
