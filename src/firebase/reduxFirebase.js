// NOTE: not sure if want to have firebase in context or not
/* eslint-disable no-underscore-dangle */
import { createFirebaseInstance } from './createFirebaseInstance';

let firebaseInstance;

export default (instance, otherConfig) => next => (
  reducer,
  initialState,
  middleware,
) => {
  const store = next(reducer, initialState, middleware);

  const configs = {
    ...otherConfig,
  };

  firebaseInstance = createFirebaseInstance(
    instance.firebase_ || instance,
    configs,
    store.dispatch,
  );
  // Attach instance (with methods wrapped in dispatch) to store
  store.firebase = firebaseInstance;

  return store;
};

export const getFirebase = () => {
  return firebaseInstance;
};
