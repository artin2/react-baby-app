// NOTE: not sure if want to have firebase in context or not
/* eslint-disable react/prop-types */
import React from 'react';
import { createFirebaseInstance } from './createFirebaseInstance';
import FirebaseContext from './firebaseContext';

type Props = {
  children: any,
  config: any,
  dispatch: any,
  firebase: any,
  initializeAuth: any,
};
function FirebaseProvider(props:Props = {}) {
  const {
    children,
    config,
    dispatch,
    firebase,
    initializeAuth,
  } = props;

  const extendedFirebaseInstance = createFirebaseInstance(
    firebase,
    config,
    dispatch,
  );

  // Initialize auth if not disabled
  if (initializeAuth) {
    extendedFirebaseInstance.initializeAuth();
  }
  return (
    <FirebaseContext.Provider value={extendedFirebaseInstance}>
      {children}
    </FirebaseContext.Provider>
  );
}

FirebaseProvider.defaultProps = {
  initializeAuth: true,
};

export default FirebaseProvider;
