// NOTE: not sure if want to have firebase in context or not
import { createContext } from 'react';

const FirebaseContext = createContext('ReactReduxFirebase');

export default FirebaseContext;
