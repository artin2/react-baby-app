// NOTE: not sure if want to have firebase in context or not
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import ReactReduxFirebaseContext from './firebaseContext';

export const withFirebase = (WrappedComponent) => {
  class WithFirebaseWrapper extends Component {
    render() {
      return (
        <WrappedComponent
          {...this.props}
        />
      );
    }
  }

  const WithFirebase = props => (
    <ReactReduxFirebaseContext.Consumer>
      {firebase => (
        <WithFirebaseWrapper
          firebase={firebase}
          dispatch={firebase.dispatch}
          {...props}
        />
      )}
    </ReactReduxFirebaseContext.Consumer>
  );

  return WithFirebase;
};
