// NOTE: not sure if want to have firebase in context or not
import { storageActions, authActions, queryActions } from '../redux/actions/index';

export const createFirebaseInstance = (firebase, configs = {}, dispatch) => {
  console.log(configs);

  const initializeAuth = () => {
    authActions.init(dispatch, firebase);
  };

  const signOut = () => firebase.auth().signOut();

  const getNest = (uid, nestId) => {
    console.log(uid, nestId);
    const opts = {
      uid,
      nestId,
    }
    queryActions.getNest(dispatch, firebase, opts);
  }

  const uploadFile = (file, fileName, metadata, uid) => {
    const opts = {
      file,
      fileName,
      metadata,
      uid,
    };
    storageActions.uploadFiles(dispatch, firebase, opts);
  };
  const helpers = {
    // ref: path => firebase.database().ref(path),
    getNest,
    signOut,
    initializeAuth,
    uploadFile,
  };
  return Object.assign(firebase, helpers);
};
