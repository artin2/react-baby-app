// NOTE: NOT SURE IF USING
import React, { Component } from 'react';
import { helperActions, queryActions } from '../../redux/actions/index';

type Props = {
  data: any,
  history: any,
  location: any,
  match: {
    params: {
      nestId: string,
    },
  },
  pageName: string,
  children: any,
}

export default (WrappedComponent: any) => {
  class BranchHelper extends Component<Props> {
    navigate = (page: string) => {
      const { history } = this.props;
      history.push(page);
    }

    render() {
      const {
        children: childrenProp,
        data,
        history,
        location,
        ...passThruProps
      } = this.props;

      const pageProps = {
        data,
        history,
        getQuestions: this.getQuestions,
        getNest: this.getNest,
        ...passThruProps,
      }
      return <WrappedComponent {...pageProps} />
    }
  }
  return BranchHelper;
}
export const mapStateToProps = (state) => {
  const {
    branch, nest, user, pageCache,
  } = state;
  // TODO: Nest Announcement page still is referencing other objects
  // need to create a standalone branch state
  return {
    branch, nest, user, pageCache,
  };
};

export const mapDispatchToProps = dispatch => ({
  updateBranch: (path, value) => {
    dispatch(helperActions.updateBranch(path, value));
  },
  updatePageCache: (path, value) => {
    dispatch(helperActions.updatePageCache(path, value));
  },
  // TODO: do I need this??
  // setProduct: (id) => {
  //   dispatch(actions.setProduct(id));
  // },
  getNest: (uid, nestId) => {
    const opts = {
      uid,
      nestId,
    };
    dispatch(queryActions.getNest(opts));
  },
  getQuery: (collectionName, path) => {
    dispatch(queryActions.getQuery(collectionName, path))
      .then(res => console.log('heeeey', res));
  },
});
