// NOTE: NOT SURE IF USING
import { helperActions } from '../../redux/actions/index';

export const mapStateToProps = (state) => {
  const {
    branch, nest, user, pageCache,
  } = state;
  // TODO: Nest Announcement page still is referencing other objects
  // need to create a standalone branch state
  return {
    branch, nest, user, pageCache,
  };
};

export const mapDispatchToProps = dispatch => ({
  updateNest: (path, value) => {
    dispatch(helperActions.updateNest(path, value));
  },
  updatePageCache: (path, value) => {
    dispatch(helperActions.updatePageCache(path, value));
  },
  createNewUser: (uid, nestId) => {
    console.log(uid, nestId);
  },
  addNestToUser: (nest) => {
    console.log(nest);
  },
  uploadPhoto: (file, fileName, metadata) => {
    console.log(file, fileName, metadata);
  },
});
