import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { JssProvider } from 'react-jss';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { theme } from '../../config/theme';
import { jss } from '../../assets/styles/global.styles';
// import ReactReduxFirebaseProvider from '../../firebase/firebaseProvider';
// import firebase from '../../config/firebase';

type Props = {
  routes: any,
  store: any,
};

// const rrfProps = {
//   firebase,
//   config: {},
//   // dispatch: store.dispatch,
//   // createFirestoreInstance // <- needed if using firestore
// };
// TODO: import persist??
const AppRoot = ({ routes, store }: Props) => (
  <MuiThemeProvider theme={theme}>
    <JssProvider jss={jss}>
      <Provider store={store}>
        {/* <ReactReduxFirebaseProvider {...rrfProps} dispatch={store.dispatch}> */}
        <Router>
          {routes}
        </Router>
        {/* </ReactReduxFirebaseProvider> */}
      </Provider>
    </JssProvider>
  </MuiThemeProvider>
);

export { AppRoot };
