/* eslint-disable react/no-unused-state */
/* eslint-disable no-unused-vars */
import React, {
  Component,
  type ComponentType,
} from 'react';
// import { connect } from 'react-redux';
import qs from 'query-string';
import trimStart from 'lodash/trimStart';
import { store } from '../../redux';
import {
  queryActions,
  storageActions,
  shareActions,
  authActions,
  helperActions,
} from '../../redux/actions/index';

type Props = {
  data: any,
  history: any,
  location: any,
  match: {
    params: {
      nestId: string,
    },
  },
}

// TODO: refactor this as a base class
const PageHelper = (WrappedComponent: any) => {
  class Helper extends Component<Props> {
    componentDidMount() {
      // TODO: this may cause an issue with branch flow - leaving here for now
      store.dispatch(authActions.auth());
    }

    navigate = (page: string) => {
      const { history } = this.props;
      history.push(this.sanitizeUrl(page));
    }

    sanitizeUrl = url => trimStart(url, '/')

    render() {
      const { location } = this.props;
      const queryParams = qs.parse(location.search);
      const pageProps = {
        ...this.props,
        navigate: this.navigate,
        queryParams,
      };
      return <WrappedComponent {...pageProps} />;
    }
  }
  return Helper;
};

export const mapStateToProps = (state) => {
  return { ...state };
};

export const mapDispatchToProps = dispatch => ({
  updateNest: (path, value) => {
    dispatch(helperActions.updateNest(path, value));
  },
  updateBranch: (path, value) => {
    dispatch(helperActions.updateBranch(path, value));
  },
  updatePageCache: (path, value) => {
    dispatch(helperActions.updatePageCache(path, value));
  },
  setProduct: (id) => {
    dispatch(helperActions.setProduct(id));
  },
  updateUser: (path, value) => {
    dispatch(helperActions.updateUser(path, value));
  },
  createNewDbUser: (user) => {
    dispatch(queryActions.createNewDbUser(user));
  },
  uploadPhoto: async (file, fileName, metadata, uid) => {
    const response = await dispatch(storageActions.uploadFile(file, fileName, metadata, uid));
    return Promise.resolve(response);
  },
  share: (config) => {
    dispatch(shareActions.share(config));
  },
  createNest: (nest) => {
    dispatch(queryActions.createNest(nest));
  },
  getQuery: (collectionName, path) => {
    dispatch(queryActions.getQuery(collectionName, path));
  },
  addBranchToNest: (branch, uid) => {
    dispatch(queryActions.addBranchToNest(branch, uid));
  },
  getNest: async (uid, nestId) => {
    const response = await dispatch(queryActions.getNest(uid, nestId));
    return Promise.resolve(response);
  },
  getBranchesForNest: async (uid, nestId) => {
    const response = await dispatch(queryActions.getBranchesForNest(uid, nestId));
    return Promise.resolve(response);
  },
  signOut: () => {
    dispatch(authActions.signOut());
  },
  getUser: async (uid) => {
    const response = await dispatch(queryActions.getUser(uid));
    return Promise.resolve(response);
  },
});

export { PageHelper };

