import { globalStyleVars as g } from './global-variables';

const btnStyles = {
  cursor: 'pointer',
  display: 'inline-block',
  minHeight: '1em',
  outline: 'none',
  border: 'none',
  verticalAlign: 'baseline',
  margin: '0em 0.25em 0em 0em',
  padding: '0.78571429em 1.5em 0.78571429em',
  textShadow: 'none',
  textTransform: 'uppercase',
  lineHeight: '1em',
  fontStyle: 'normal',
  fontFamily: g.headerFont,
  fontWeight: 'bold',
  textAlign: 'center',
  borderRadius: '4px',
  textDecoration: 'none',
  transition: 'opacity 0.1s ease, background-color 0.1s ease, color 0.1s ease, box-shadow 0.1s ease, background 0.1s ease',
  userSelect: 'none',
  willChange: '""',
  width: '100%',
};

export { btnStyles };
