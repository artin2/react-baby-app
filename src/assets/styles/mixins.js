import { globalStyleVars as g } from './global-variables';

const createBackgroundImage = (url) => {
  return {
    background: `url('${url}')`,
    backgroundSize: 'cover',
    height: '100vh',
    width: '100vw',
  };
};

const overlay = (offset = 0, height = 100) => {
  return {
    background: g.baseBlue,
    bottom: offset,
    left: offset,
    position: 'absolute',
    zIndex: 9,
    opacity: 0.85,
    right: offset,
    top: offset,
    width: '100vw',
    height: `${height}vh`,
  };
};

export {
  createBackgroundImage,
  overlay,
};
