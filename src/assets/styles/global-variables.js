const blue = '#0743BE';
const yellow = '#F4C203';
const pink = '#E7228E';
// const lightPink = '#EFA2BD';
const black = '#091633';
const gray = '#A5A9B5';
const white = '#F5F4F2';
const baseBlue = '#091633';
const backgroundGray = '#F5F4F2';

const globalStyleVars = {
  primaryColor: pink,
  secondaryColor: blue,
  tertiaryColor: yellow,
  black,
  gray,
  white,
  baseBlue,
  backgroundGray,
  copyFontSize: '15px',
  labelFontSize: '14px',
  buttonFontSize: '14px',
  headerFontSize: '20px',
  subtextFontSize: '16px',
  smallPrintFontSize: '11px',
  sectionPadding: '2rem 1.5rem',
  primaryFont: 'Avenir-Next, sans-serif',
  secondaryFont: 'Lato, sans-serif',
  tertiaryFont: 'Open Sans, sans-serif',
  headerFont: 'Avenir-Next-Bold, sans-serif',
};

export { globalStyleVars };
