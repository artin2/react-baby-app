const fonts = {
  '@global': {
    '@font-face': [{
      fontFamily: 'Avenir-Next',
      src: 'url("src/assets/fonts/Avenir-next/AvenirNextLTPro-Regular.woff") format("woff")',
      fontWeight: 'normal',
      fontStyle: 'normal',
    },
      // {
      //   fontFamily: 'Avenir-Next-Bold',
      //   src: 'url(./fonts/Avenir-next/AvenirNextLTPro-Bold.otf)',
      //   fontWeight: 'normal',
      //   fontStyle: 'normal',
      // }
    ],
  },
};

export { fonts };
