/* eslint-disable no-useless-computed-key */
import { create as createJss } from 'jss';
import preset from 'jss-preset-default';

const jss = createJss(preset());

const section = {
  padding: '2.5rem 2.5rem',
};

const container = {
  margin: '0 auto',
  position: 'relative',
};

const styles = {
  '@global': {
    ['html, body, div, span, object, h1, h2, h3, h4, h5, h6, p, a, img, ol, ul, li, form, label']: {
      margin: 0,
      padding: 0,
      border: 0,
      outline: 0,
      fontWeight: 'inherit',
      fontStyle: 'inherit',
      fontFamily: 'inherit',
      fontSize: '62.5%',
      verticalAlign: 'baseline',
    },
    '*, :after, :before': {
      boxSizing: 'border-box',
    },
    fontFamily: 'Avenir-Next',
    // ...fonts, // TODO: fix fonts aren't loading through jss

  },
};

jss.createStyleSheet(styles).attach();

export { jss, section, container };

