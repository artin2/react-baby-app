export const USERS_COLLECTION = 'Users';
export const QUESTIONS_COLLECTION = 'Questions';
export const EXPERIENCE_TOPICS_COLLECTION = 'ExperienceTopics';
export const PRODUCTS_COLLECTION = 'Products';

export const NESTS_SUBCOLLECTION = 'nests';
export const BRANCHES_SUBCOLLECTION = 'branches';

export const USER_STORAGE_PATH = 'userImages';
export const BRANCH_STORAGE_PATH = 'branchImages';
