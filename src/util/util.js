import omit from 'lodash/omit';

export const prepBranch = branch => omit(branch, ['nestId', 'product.productSet', 'knowledge.knowledgeSet', 'experience.experienceSet']);

export const generateBranchId = (name, nestId) => `${name}_${nestId}_${Date.now()}`;

export const generateFileName = name => `${Date.now()}-${name}`;

export const setUpFileForUpload = (file) => {
  const { type, name } = file;
  const fileName = generateFileName(name);
  const metadata = { contentType: type };
  console.log(fileName, metadata);
  return {
    file,
    fileName,
    metadata,
  };
};
