// import React, { Component } from 'react';
import LoadableComponent from 'react-loadable';
import LoadingSpinner from '../components/LoadingSpinner';
/**
 * Create component which is loaded async, showing a loading spinner
 * in the meantime.
 * @param {Object} opts - Loading options
 * @param {Function} opts.loader - Loader function (should return import promise)
 */
export function Loadable(opts) {
  return LoadableComponent({
    loading: LoadingSpinner,
    ...opts,
  });
}
