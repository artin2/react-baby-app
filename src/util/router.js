import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import { createBrowserHistory } from 'history';
import LoadingSpinner from '../components/LoadingSpinner';
import { HOME_PATH } from '../constants/paths';
import { unauthedRedirect } from '../redux/actions/helper';

const history = createBrowserHistory();

export const UserIsAuthenticated = connectedRouterRedirect({
  wrapperDisplayName: 'UserIsAuthenticated',
  authenticatedSelector: ({ pageCache: { auth } }) => !auth && !auth.isEmpty && !!auth.uid,
  AuthenticatingComponent: LoadingSpinner,
});

export const UserIsNotAuthenicated = connectedRouterRedirect({
  redirectPath: HOME_PATH,
  wrapperDisplayName: 'UserIsNotAuthenicated',
  authenticatedSelector: ({ pageCache: { auth } }) => auth === false,
  AuthenticatingComponent: LoadingSpinner,
  // TODO: create unauthed redirect
  redirectAction: newLoc => (dispatch) => {
    history.push(newLoc);
    dispatch(unauthedRedirect());
  },
});
