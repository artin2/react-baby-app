import React from 'react';
import ReactDOM from 'react-dom';

// import * as serviceWorker from './serviceWorker';
import './index.css';
import { store } from './redux';
import { AppRoot } from './containers/AppRoot/AppRoot';

const routes = require('./pages/index').default(store);

console.log(process.env);
ReactDOM.render(
  <AppRoot store={store} routes={routes} />,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.register();
