import { combineReducers } from 'redux';
// import { firebaseReducer as firebase } from 'react-redux-firebase';
// import merge from 'lodash/merge';
import cloneDeep from 'lodash/cloneDeep';
import set from 'lodash/set';
import { ACTION_TYPES } from './actions/action-types';

export const userReducer = (currentState = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES.UPDATE_USER: {
      const { path, value } = action.payload;
      const user = cloneDeep(currentState);

      set(user, `${path}`, value);
      return {
        // ...currentState,
        ...user,
      };
    }
    case ACTION_TYPES.SET_USER: {
      const { user } = action.payload;
      return {
        ...user,
      };
    }
    default:
      return currentState;
  }
};

export const nestReducer = (currentState = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES.UPDATE_NEST: {
      const { path, value } = action.payload;
      const nest = cloneDeep(currentState);

      set(nest, `${path}`, value);
      return {
        ...nest,
      };
    }
    case ACTION_TYPES.SET_NEST: {
      const { nest } = action.payload;
      return {
        ...nest,
      };
    }
    default:
      return currentState;
  }
};

// TODO: not sure if I need this - may just move auth to user
export const pageReducer = (currentState = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES.SET_CURRENT_USER: {
      const { user } = action.payload;
      return {
        ...currentState,
        auth: user,
      };
    }
    case ACTION_TYPES.UPDATE_PAGE_CACHE: {
      const { path, value } = action.payload;
      const cache = cloneDeep(currentState);

      set(cache, `${path}`, value);
      return {
        ...cache,
      };
    }
    case ACTION_TYPES.ERROR: {
      const errorMessage = action.payload;
      return {
        ...currentState,
        errorMessage,
      };
    }
    default:
      return currentState;
  }
};

export const branchReducer = (currentState = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES.UPDATE_BRANCH: {
      const { path, value } = action.payload;
      const branch = cloneDeep(currentState);

      set(branch, `${path}`, value);
      return {
        ...branch,
      };
    }
    case ACTION_TYPES.SET_PRODUCT: {
      const { id } = action.payload;
      const { product: { productSet } } = currentState;
      const selectedProduct = productSet.find(p => p.id === id);
      return {
        ...currentState,
        product: selectedProduct,
      };
    }
    default:
      return currentState;
  }
};

export function makeRootReducer(asyncReducers) {
  return combineReducers({
    // Add sync reducers here
    // firebase,
    nest: nestReducer,
    pageCache: pageReducer,
    branch: branchReducer,
    user: userReducer,
    ...asyncReducers,
  })
}
