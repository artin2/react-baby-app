// import firebase from '../../config/firebase';
import { updatePageCache } from './helper';

// export const auth = () => {
//   return (dispatch) => {
//     return firebase.auth().onAuthStateChanged((user) => {
//       if (user) {
//         dispatch(setCurrentUser(user));
//       } else { dispatch(error('No User is signed in')); }
//     });
//   };
// };

// export const unAuth = () => {
//   return (dispatch) => {
//     const onSuccess = () => {
//       dispatch(signout());
//     };

//     const onFailure = (e) => {
//       dispatch(error(`Could not sign user out: ${e}`));
//     }
//     try {
//       const response = firebase.auth().signOut();
//       console.log(response);
//       return onSuccess();
//     } catch (e) {
//       return onFailure(e);
//     }
//   };
// };

export const auth = () => {
  return (dispatch, getState, firebase) => {
    // exit if auth does not exist
    if (!firebase.auth) {
      return;
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        dispatch(updatePageCache('auth', user));
      } else {
        dispatch(updatePageCache('auth', false))
      }
    });
  };
};

export const signOut = () => {
  return (dispatch, getState, firebase) => {
    // exit if auth does not exist
    if (!firebase.auth) {
      return;
    }
    console.log('signing out');
    firebase.auth().signOut();
  };
}
