import { shareComplete, error } from './helper';

export const share = (config) => {
  return async (dispatch) => {
    console.log(config);
    try {
      await navigator.share(config);
      dispatch(shareComplete());
    } catch (err) {
      error(err);
    }
  };
};
