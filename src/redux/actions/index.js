import * as storageActions from './storage';
import * as authActions from './auth';
import * as queryActions from './query';
import * as shareActions from './share';
import * as helperActions from './helper';

export {
  storageActions,
  authActions,
  queryActions,
  shareActions,
  helperActions,
};
export default {
  storageActions,
  authActions,
  queryActions,
  shareActions,
  helperActions,
};
