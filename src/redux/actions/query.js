/* eslint-disable prefer-destructuring */
import cloneDeep from 'lodash/cloneDeep';
import { prepBranch } from '../../util/util';
import { USERS_COLLECTION, NESTS_SUBCOLLECTION, BRANCHES_SUBCOLLECTION } from '../../constants/firebase';
import * as helpers from './helper';

export const getNest = (uid, nestId) => {
  return async (dispatch, getState, firebase) => {
    console.log(uid, nestId);
    console.log(getState());
    try {
      const nest = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .collection(NESTS_SUBCOLLECTION)
        .doc(nestId)
        .get();
      const { exists } = nest;
      const value = exists ? nest.data() : 'no nest for id'; // TODO: find a better way to handle no id
      dispatch(helpers.updatePageCache('nestExist', exists));
      dispatch(helpers.setNest(value));
      return {
        exists,
        rawResponse: nest,
      };
    } catch (err) {
      dispatch(helpers.error(`There was an error getting the nest: ${err}`));
      throw err;
    }
  };
};

/**
 * @param {string} collectionName - collection to get items from
 * @param {string} path - where in the branch to store it
 */
export const getQuery = (collectionName, path) => {
  return async (dispatch, getState, firebase) => {
    dispatch(helpers.getCollectionInitiated(collectionName));
    try {
      const collection = await firebase
        .firestore()
        .collection(collectionName)
        .get();
      const returnCollectionArray = [];
      collection.forEach((doc) => {
        const { id } = doc;
        returnCollectionArray.push({
          id,
          ...doc.data(),
        });
      });
      dispatch(helpers.updateBranch(path, returnCollectionArray));
      dispatch(helpers.getCollectionComplete(collectionName));
      return returnCollectionArray;
    } catch (err) {
      const errorMessage = `There was an error getting ${collectionName}: ${err}`;
      dispatch(helpers.error(errorMessage));
      return { err };
    }
  };
};

export const createNewDbUser = (user) => {
  return async (dispatch, getState, firebase) => {
    const updatedUser = cloneDeep(user);
    const state = getState();
    const { pageCache: { auth } } = state;
    const { displayName, uid } = auth;
    const fullName = displayName.split(' ');
    updatedUser.firstName = fullName[0];
    updatedUser.lastName = fullName[1];
    try {
      dispatch(helpers.createNewDbUserInitiated);
      const newUser = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .set(updatedUser);
      dispatch(helpers.createNewDbUserComplete);
      return newUser;
    } catch (err) {
      const errorMessage = `There was an issue creating an new user: ${err}`;
      dispatch(helpers.error(errorMessage));
      throw err;
    }
  };
};

export const createNest = (nest) => {
  return async (dispatch, getState, firebase) => {
    // TODO: do I want to pull this from getState() vs passing it in??
    const { nestId } = nest;
    const state = getState();

    dispatch(helpers.nestCreateInitiated(nestId));

    // TODO: get from auth instead of user
    const { user: { uid } } = state;

    try {
      const newNest = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .collection(NESTS_SUBCOLLECTION)
        .doc(nestId)
        .set(nest);
      dispatch(helpers.nestCreateComplete(nestId));
      return newNest;
    } catch (err) {
      dispatch(helpers.error(err));
      throw err;
    }
  };
};

export const addBranchToNest = (newBranch, uid) => {
  return async (dispatch, getState, firebase) => {
    dispatch(helpers.addBranchToNestInitiated());
    const { nestId, id } = newBranch; // TODO: probably want to store in user
    const filteredBranch = prepBranch(newBranch);
    try {
      const branchResponse = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .collection(NESTS_SUBCOLLECTION)
        .doc(nestId)
        .collection(BRANCHES_SUBCOLLECTION)
        .doc(id)
        .set(filteredBranch)
      dispatch(helpers.addBranchToNestComplete());
      return branchResponse;
    } catch (err) {
      dispatch(helpers.error('There was an error adding the branch', err));
      throw err;
    }
  };
};

export const getBranchesForNest = (uid, nestId) => {
  return async (dispatch, getState, firebase) => {
    dispatch(helpers.getBranchForNestInitiated());
    try {
      const branches = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .collection(NESTS_SUBCOLLECTION)
        .doc(nestId)
        .collection(BRANCHES_SUBCOLLECTION)
        .get();
      const branchData = [];
      branches.forEach((doc) => {
        const formattedDoc = {
          id: doc.id,
          branch: doc.data(),
        };

        branchData.push(formattedDoc);
      });
      dispatch(helpers.updateNest('branches', branchData));
      dispatch(helpers.getBranchForNestComplete());
      return Promise.resolve(branchData);
    } catch (err) {
      dispatch(helpers.error(err));
      return Promise.reject(err);
    }
  };
};

export const getUser = (uid) => {
  return async (dispatch, getState, firebase) => {
    try {
      const user = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .get();
      dispatch(helpers.setUser(user.data()));
      // TODO: move to new action
      const nests = await firebase
        .firestore()
        .collection(USERS_COLLECTION)
        .doc(uid)
        .collection(NESTS_SUBCOLLECTION)
        .get();
      const nestData = [];
      nests.forEach((nest) => {
        const formattedNest = {
          id: nest.id,
          nest: nest.data(),
        };
        nestData.push(formattedNest);
      });
      dispatch(helpers.updateUser('nests', nestData));
      return Promise.resolve();
    } catch (err) {
      dispatch(helpers.error(err));
      return Promise.reject(err);
    }
  }
}

