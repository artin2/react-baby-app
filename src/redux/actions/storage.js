import {
  fileUploadInitiated,
  updateNest,
  updateBranch,
  error,
  fileUploadComplete,
} from './helper';
// import { USER_STORAGE_PATH } from '../../constants/firebase';

type Config = {
  path: string,
  child: string,
  update: string,
  updatePath: string,
};
/**
 * @param {*} file raw file
 * @param {string} fileName
 * @param {*} metadata config type
 * @param {*} folderConfig
 * for user:
 * {
 *  path: USER_STORAGE_PATH}
 *  child: uid,
 *  dispatch: updateNest,
 *  updatePath: 'imageUrl'
 * }
 * for branchUser
 * {
 *  path: BRANCH_STORAGE_PATH
 *  child: branchId => firstName_Date.now(),
 *  dispatchFunc: updateBranch,
 *  updatPath: 'profileImage' || 'coverImage'
 * }
 */
export const uploadFile = ({ file, fileName, metadata }, config: Config) => {
  return async (dispatch, getState, firebase) => {
    dispatch(fileUploadInitiated(fileName));
    const {
      path, child, update, updatePath,
    } = config;
    const userRef = `${path}/${child}`;
    const uploadPromise = () => firebase.storage().ref(userRef).child(fileName).put(file, metadata);
    return uploadPromise()
      .then((snapshot) => {
        return snapshot.ref.getDownloadURL().then((downloadURL) => {
          console.log('File available at', downloadURL);
          if (update === 'branch') dispatch(updateBranch(updatePath, downloadURL));
          if (update === 'nest') dispatch(updateNest(updatePath, downloadURL));
          // dispatch(updateNest('imageUrl', downloadURL));
          dispatch(fileUploadComplete(fileName));
          return Promise.resolve(downloadURL);
        }, (err) => {
          dispatch(error(err));
          // TODO: dont' think this is right
          return Promise.reject(err);
        });
      });
  }
}
