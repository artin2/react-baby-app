import { ACTION_TYPES } from './action-types';

/**
 * AUTH
 */
export const unauthedRedirect = () => ({
  type: ACTION_TYPES.UNAUTHED_REDIRECT,
  payload: { message: 'User is not authenicated' },
});

/**
 * QUERY STATES
 */
export const getNestInitiated = () => ({
  type: ACTION_TYPES.CREATE_NEW_DB_USER_INITIATED,
  payload: 'Initiating new user creation',
});

export const createNewDbUserInitiated = () => ({
  type: ACTION_TYPES.CREATE_NEW_DB_USER_INITIATED,
  payload: 'Initiating new user creation',
});

export const createNewDbUserComplete = () => ({
  type: ACTION_TYPES.CREATE_NEW_DB_USER_COMPLETE,
  payload: 'New user created successfully',
});

export const nestCreateInitiated = nestId => ({
  type: ACTION_TYPES.NEST_CREATE_INITIATED,
  payload: `Nest for ${nestId} initiated`,
});

export const nestCreateComplete = nestId => ({
  type: ACTION_TYPES.NEST_CREATE_COMPLETE,
  payload: `${nestId} was successfully created`,
});

export const getCollectionInitiated = collectionName => ({
  type: ACTION_TYPES.GET_COLLECTION_INITIATED,
  payload: `GET: ${collectionName} initiated`,
});

export const getCollectionComplete = collectionName => ({
  type: ACTION_TYPES.GET_COLLECTION_COMPLETE,
  payload: `GET: ${collectionName} complete`,
});

export const addBranchToNestInitiated = () => ({
  type: ACTION_TYPES.ADD_BRANCH_TO_NEST_INITIATED,
  payload: 'add branch to nest',
});

export const addBranchToNestComplete = () => ({
  type: ACTION_TYPES.ADD_BRANCH_TO_NEST_COMPLETE,
  payload: 'add branch to nest',
});

export const getBranchForNestInitiated = () => ({
  type: ACTION_TYPES.GET_BRANCHES_FOR_NEST_INITIATED,
  payload: 'getting branches for nest',
});

export const getBranchForNestComplete = () => ({
  type: ACTION_TYPES.GET_BRANCHES_FOR_NEST_COMPLETE,
  payload: 'Successfully got branches',
});

export const setUser = user => ({
  type: ACTION_TYPES.SET_USER,
  payload: { user },
});
/**
 * STORAGE
 */
export const fileUploadInitiated = fileName => ({
  type: ACTION_TYPES.FILE_UPLOAD_INITIATED,
  payload: `Uploading ${fileName}`,
});

export const fileUploadComplete = fileName => ({
  type: ACTION_TYPES.FILE_UPLOAD_COMPLETE,
  payload: `File upload complete for ${fileName}`,
});

/**
 * SHARE
 */

export const shareComplete = () => ({
  type: ACTION_TYPES.NEST_SHARE_COMPLETE,
  payload: 'Nest successfully shared',
});

/**
 * STORE UPDATES
 */
export const updateUser = (path, value) => ({
  type: ACTION_TYPES.UPDATE_USER,
  payload: { path, value },
});

export const updateNest = (path, value) => ({
  type: ACTION_TYPES.UPDATE_NEST,
  payload: { path, value },
});

export const setNest = nest => ({
  type: ACTION_TYPES.SET_NEST,
  payload: { nest },
});

export const updatePageCache = (path, value) => ({
  type: ACTION_TYPES.UPDATE_PAGE_CACHE,
  payload: { path, value },
});

export const updateBranch = (path, value) => ({
  type: ACTION_TYPES.UPDATE_BRANCH,
  payload: { path, value },
});

export const error = msg => ({
  type: ACTION_TYPES.ERROR,
  payload: { msg },
});

export const setProduct = id => ({
  type: ACTION_TYPES.SET_PRODUCT,
  payload: { id },
});

