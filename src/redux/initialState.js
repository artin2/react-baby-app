import { State } from './types';
import avatarPlaceholder from '../assets/images/avatar-placeholder.png';
// Fake data to help with testing
export const initialState: State = {
  pageCache: {
    auth: false,
  },
  user: {
    uid: 'lAEVYQARQtOz6SU4wYj01P1CEoL2',
    city: 'Test City',
    state: 'New Test State',
    // profileImage: 'http://i.pravatar.cc/300?img=35',
    nests: [
      {
        id: 'EzraJefferson2019',
        nest: {
          nestId: 'EzraJefferson2019',
          imageUrl: 'https://bit.ly/2YVXfDr',
          announcementMessage: `This is my test message. I should probably write a little more just to test the length.
          So let me copy paste some of this. This is my test message. I should probably write a little more just to test the length.
          So let me copy paste some of this`,
          branches: [],
          dateAdded: '',
        },
      },
      {
        id: 'BabyLevi2016',
        nest: {
          nestId: 'BabyLevi2016',
          imageUrl: 'https://bit.ly/2YVXfDr',
          announcementMessage: `This is my test message. I should probably write a little more just to test the length.
          So let me copy paste some of this. This is my test message. I should probably write a little more just to test the length.
          So let me copy paste some of this`,
          branches: [],
        },
      },
    ],
  },
  nest: {
    nestId: 'testNestId2',
    imageUrl: 'https://bit.ly/2YVXfDr',
    announcementMessage: `This is my test message. I should probably write a little more just to test the length.
    So let me copy paste some of this. This is my test message. I should probably write a little more just to test the length.
    So let me copy paste some of this`,
    gender: 'Hoping for a girl',
    dueDateMonth: 'April',
    dueDateYear: '2019',
  },
  branch: {
    uid: '',
    nestId: 'testNestId',
    // coverImage: 'https://bit.ly/2D9S7Ta',
    coverCaption: 'You’re going to be an AMAZING mom! I can’t wait for to be an AUNTIE!!!!',
    profileImage: avatarPlaceholder,
    firstName: '',
    lastName: '',
    knowledge: {
      id: '',
      value: 'Um, how the heck do I get my socks on?',
      response: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Quisque ut porttitor leo. Class aptent taciti sociosqu ad litora torquent
      per conubia nostra, per inceptos himenaeos. Vestibulum ante ipsum primis 
      in faucibus orci luctus et ultrices posuere cubilia Curae; Class aptent 
      taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
      Pellentesque et diam a odio eleifend commodo at vel ipsum. Ut mollis imperdiet 
      pulvinar. Nunc aliquam justo vehicula lobortis varius. Suspendisse
      volutpat sapien velit, in faucibus quam tristique vel.`,
      knowledgeSet: [],
    },
    experience: {
      id: '6EBHVJUSibbZbyv6LAmH',
      value: 'Fashion',
      response: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Quisque ut porttitor leo. Class aptent taciti sociosqu ad litora torquent
      per conubia nostra, per inceptos himenaeos. Vestibulum ante ipsum primis 
      in faucibus orci luctus et ultrices posuere cubilia Curae; Class aptent 
      taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
      Pellentesque et diam a odio eleifend commodo at vel ipsum. Ut mollis imperdiet 
      pulvinar. Nunc aliquam justo vehicula lobortis varius. Suspendisse
      volutpat sapien velit, in faucibus quam tristique vel.`,
      experienceSet: [],
    },
    product: {
      id: 'fYPZLSRmOES8nvWzdXLD',
      image: {
        large: 'https://firebasestorage.googleapis.com/v0/b/qursive-7030a.appspot.com/o/products%2Fdelta-children-sweet-beginnings.jpg?alt=media&token=aaa80e20-e6a6-40ca-9467-e2faab51be4d',
      },
      price: '3333',
      productName: 'Fake Product Name',
      shippingCost: '0',
      source: {
        label: 'Amazon',
        value: 'amazon.com',
      },
      response: 'I SWEAR by the Boba! We wear it everywhere. I even wear it at home!',
      productSet: [],
    },
  },
};

// Using this for unit testing for now
export const actualInitialState: State = {
  user: {

  },
  nest: {

  },
  branch: {

  },
};

