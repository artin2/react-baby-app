/* eslint-disable no-underscore-dangle */
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import { makeRootReducer } from './reducers';
import { initialState } from './initialState';
import firebase from '../config/firebase';
// import reduxFirebase from '../firebase/reduxFirebase';
// const persistConfig = {
//   key: 'root',
//   storage,
// }
// const combinedReducer = redux.combineReducers({
//   pageCache: pageReducer,
//   nest: nestReducer,
//   branch: branchReducer,
//   user: userReducer,
// });
// const persistedReducer = persistReducer(persistConfig, combinedReducer);

// export default () => {
//   const store = redux.createStore(
//     persistedReducer,
//     initialState,
//     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
//   )
//   const persistor = persistStore(store);
//   return { store, persistor };
// };

const middleware = process.env.NODE_ENV === 'development' ? compose(
  applyMiddleware(thunk.withExtraArgument(firebase)),
  // reduxFirebase(firebase, {}),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
) : applyMiddleware(thunk.withExtraArgument(firebase));

export const store = createStore(
  // persistedReducer,
  makeRootReducer(),
  initialState,
  middleware,
);
// export const persistor = persistStore(store);
