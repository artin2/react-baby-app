// TODO: need to redo these to match the flows
export type State = {
  pageCache: {
    auth: any,
    knowledgeSet: [any],
    experienceSet: [any],
    productSet: [any],
  },
  user: {
    uid: String,
    city: String,
    state: String,
    name: String,
    profileImage: String,
    nests: [any],
  },
  nest: {
    nestId: string,
    imageUrl: string,
    nestMessage: string,
    branches: [any],
  },
  branch: {
    nestId: String,
    uid: String,
    coverPhoto: string,
    coverCaption: string,
    profileImage: string,
    name: String,
    knowledge: {
      id: string,
      value: String,
      response: string,
    },
    experience: {
      id: string,
      value: String,
      response: string,
    },
    product: {
      id: string,
      image: string,
      price: string,
      productName: string,
      shippingCost: string,
      source: {
        label: string,
        value: string,
      },
      response: string,
    },
  }
};
