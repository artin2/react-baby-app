/* eslint-disable no-param-reassign */

import firebaseMock from 'firebase-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
// import cloneDeep from 'lodash/cloneDeep';
import { queryActions } from '../../src/redux/actions';

const mockauth = new firebaseMock.MockFirebase();
let mockfirestore = new firebaseMock.MockFirestore();
const mocksdk = firebaseMock.MockFirebaseSdk(null, () => {
  return mockauth;
}, () => {
  return mockfirestore;
});

const mockapp = mocksdk.initializeApp();
const firebase = store => next => (action) => {
  store.firebase = mockapp.firestore;
  console.log('stttooooreesss', store, action);
  next(action);
};
const middlewares = [thunk, firebase];
const mockStore = configureMockStore(middlewares);

const initialState = {};
describe('Firebase Query Actions', () => {
  let store;
  const uid = '18429JKWKFI9W9JKSDJKFQ01';
  const nestId = 'MaybeBaby123';
  beforeEach(() => {
    store = mockStore(initialState);
    store.firebase = mockapp.firestore;
    store.firebase().collection('users').doc(uid).collection('nests').doc(nestId);
    // mockfirestore = new firebaseMock.MockFirestore();
    // mockfirestore.autoFlush();
  });

  it('test of mock', () => {
    console.log('store', store);
  });

  it('test getNest', () => {

    queryActions.getNest(uid, nestId)(store.dispatch, store.getState, mockapp)
      .then((res) => {
        const action = store.getActions;
        console.log('RESPONSE>>>>>>', action, res);
      })
      .catch(err => console.log(err));
  });
});
