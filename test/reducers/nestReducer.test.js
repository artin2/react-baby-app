import { nestReducer } from '../../src/redux/reducers';
import { ACTION_TYPES } from '../../src/redux/actions/action-types';

const initialState = { };

describe('nest reducer', () => {
  it('returns the initial state', () => {
    expect(nestReducer(undefined, {})).toEqual(initialState);
  });

  it('should update nest id -> test UPDATE_NEST', () => {
    const action = {
      type: ACTION_TYPES.UPDATE_NEST,
      payload: {
        path: 'nestId',
        value: 'MaybeBaby2019',
      },
    };
    expect(nestReducer(initialState, action)).toEqual({
      ...initialState,
      nestId: 'MaybeBaby2019',
    });
  });
});
