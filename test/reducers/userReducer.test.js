import { userReducer } from '../../src/redux/reducers';
import { ACTION_TYPES } from '../../src/redux/actions/action-types';

const initialState = { };

describe('user reducer', () => {
  it('returns the initial state', () => {
    expect(userReducer(undefined, {})).toEqual(initialState);
  });

  it('should update user\'s city', () => {
    const action = {
      type: ACTION_TYPES.UPDATE_USER,
      payload: {
        path: 'city',
        value: 'Madison',
      },
    };
    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      city: 'Madison',
    });
  });
});
