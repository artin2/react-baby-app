import { branchReducer } from '../../src/redux/reducers';
import { ACTION_TYPES } from '../../src/redux/actions/action-types';

const initialState = { };

describe('nest reducer', () => {
  it('returns the initial state', () => {
    expect(branchReducer(undefined, {})).toEqual(initialState);
  });

  it('should update name -> test UPDATE_BRANCH', () => {
    const action = {
      type: ACTION_TYPES.UPDATE_BRANCH,
      payload: {
        path: 'name',
        value: 'Jamie',
      },
    };
    expect(branchReducer(initialState, action)).toEqual({
      ...initialState,
      name: 'Jamie',
    });
  });

  it('should update knowledgeset -> test nested UPDATE_BRANCH', () => {
    const value = [
      {
        id: '1',
        question: 'Question 1',
      },
      {
        id: '2',
        question: 'Question 2',
      },
    ];
    const action = {
      type: ACTION_TYPES.UPDATE_BRANCH,
      payload: {
        path: 'knowledge.knowledgeSet',
        value,
      },
    };
    expect(branchReducer(initialState, action)).toEqual({
      ...initialState,
      knowledge: {
        knowledgeSet: value,
      },
    });
  });
});
