import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withA11y } from '@storybook/addon-a11y';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { JssProvider } from 'react-jss';
import { jss } from '../src/assets/styles/global.styles';
import { theme } from '../src/config/theme';
// automatically import all files ending in *.stories.js
const req = require.context('../src/', true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

const jssDecorator = storyFn => (
  <JssProvider jss={jss}>
    <MuiThemeProvider theme={theme}>
      <div style={{ marginTop: '30px', width: '100wv' }}>
        {storyFn()}
      </div>
    </MuiThemeProvider>
  </JssProvider>
);

addDecorator(jssDecorator);
addDecorator(withA11y);
addDecorator(withKnobs);
configure(loadStories, module);

